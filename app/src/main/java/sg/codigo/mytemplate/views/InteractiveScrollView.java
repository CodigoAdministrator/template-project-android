package sg.codigo.mytemplate.views;

import sg.codigo.mytemplate.utils.Constants;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;


/**
 * Triggers a event when scrolling reaches bottom.
 *
 * Created by martinsandstrom on 2010-05-12.
 * Updated by martinsandstrom on 2014-07-22.
 *
 * Usage:
 *
 *  scrollView.setOnBottomReachedListener(
 *      new InteractiveScrollView.OnBottomReachedListener() {
 *          @Override
 *          public void onBottomReached() {
 *              // do something
 *          }
 *      }
 *  );
 * 
 *
 * Include in layout:
 *  
 *  <se.marteinn.ui.InteractiveScrollView
 *      android:layout_width="match_parent"
 *      android:layout_height="match_parent" />
 *  
 */
public class InteractiveScrollView extends ScrollView {
	OnTopReachedListener mListenerTopReached;
    OnBottomReachedListener mListenerBottomReached;
    OnScrollUpListener mListenerScrollUp;
    OnScrollDownListener mListenerScrollDown;

    public InteractiveScrollView(Context context, AttributeSet attrs,
            int defStyle) {
        super(context, attrs, defStyle);
    }

    public InteractiveScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InteractiveScrollView(Context context) {
        super(context);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        View view = (View) getChildAt(getChildCount()-1);
        int diffTop = (getScrollY() - view.getTop());
        
        int diffBottom = (view.getBottom()-(getHeight()+getScrollY()));
        
        if (diffTop <= Constants.DEFAULT_SCROLL_UP_DIFF && mListenerTopReached != null) {
        	mListenerTopReached.onTopReached();
        }
        
        if(t > oldt && mListenerScrollDown != null){
        	mListenerScrollDown.onScrollDown();
        }
        
        /*
        if(Constants.DEBUG_MODE){
        	Log.d(Constants.TAG, "view.getBottom(): " + diffBottom);
        	Log.d(Constants.TAG, "getHeight(): " + getHeight());
        	Log.d(Constants.TAG, "getScrollY(): " + getScrollY());
        	Log.d(Constants.TAG, "getHeight()+getScrollY(): " + getHeight()+getScrollY());
        	Log.d(Constants.TAG, "diffBottom: " + diffBottom);
        }
        */
        
        if (diffBottom <= Constants.DEFAULT_SCROLL_DOWN_DIFF && mListenerBottomReached != null) {
        	mListenerBottomReached.onBottomReached();
        }
        
        if(t < oldt && mListenerScrollUp != null){
        	mListenerScrollUp.onScrollUp();
        }

        super.onScrollChanged(l, t, oldl, oldt);
    }


    // Getters & Setters
    public OnTopReachedListener getOnTopReachedListener() {
        return mListenerTopReached;
    }

    public void setOnTopReachedListener(OnTopReachedListener onTopReachedListener) {
    	mListenerTopReached = onTopReachedListener;
    }
    
    public OnBottomReachedListener getOnBottomReachedListener() {
        return mListenerBottomReached;
    }

    public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener) {
    	mListenerBottomReached = onBottomReachedListener;
    }
    

    public OnScrollUpListener getOnScrollUpListener() {
        return mListenerScrollUp;
    }

    public void setOnScrollUpListener(OnScrollUpListener onScrollUpListener) {
    	mListenerScrollUp = onScrollUpListener;
    }
    

    public OnScrollDownListener getOnScrollDownListener() {
        return mListenerScrollDown;
    }

    public void setOnScrollDownListener(OnScrollDownListener onScrollDownListener) {
    	mListenerScrollDown = onScrollDownListener;
    }
    
    /**
     * Event listener.
     */
    public interface OnTopReachedListener{
        public void onTopReached();
    }
    
    public interface OnBottomReachedListener{
        public void onBottomReached();
    }
    
    public interface OnScrollUpListener{
        public void onScrollUp();
    }
    
    public interface OnScrollDownListener{
        public void onScrollDown();
    }
    
}
package sg.codigo.mytemplate;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sg.codigo.mytemplate.callbacks.JsonCallback;
import sg.codigo.mytemplate.callbacks.PopupCallback;
import sg.codigo.mytemplate.dialogs.DialogOK;
import sg.codigo.mytemplate.models.FacebookUserInfo;
import sg.codigo.mytemplate.storages.AppPreferences;
import sg.codigo.mytemplate.utils.Constants;
import sg.codigo.mytemplate.utils.HTTPPostAsyncTask;
import sg.codigo.mytemplate.utils.Utils;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.facebook.AppEventsLogger;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionDefaultAudience;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.internal.SessionTracker;
import com.facebook.model.GraphUser;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.DirectionalViewPager;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.content.Context;
import android.content.Intent;

public class LandingActivity extends FragmentActivity implements OnClickListener{
	// data
	//private static Context context;
	//private static Context fbContext;
	
	private static AppPreferences appPrefs;
	private int animationIndex = 1;
	public static int currentIndex = 0;
	/*
	private PopupCallback popupCallback;
	private ArrayList<NameValuePair> nameValuePairs;
	private JsonCallback jsonCallback;
	private DialogOK dialogOK;
	private DialogOK dialogError;
	*/

	// layout - landing
	//private ImageView ivWelcomeText;
	private TextView tvLandingTitle;
	private TextView tvLandingMessage;
	private RelativeLayout layoutBtnSignIn;
	private Button btnSignIn;
	private RelativeLayout layoutBtnRegister;
	private Button btnRegister;
	private Button btnSkip;

	@Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
        
        Utils.updateActionBar(LandingActivity.this, "", false, false, false, null);
		setContentView(R.layout.activity_landing);
		
		
		// facebook
    	//uiHelper = new UiLifecycleHelper(LandingActivity.this, fbCallback);
        //uiHelper.onCreate(savedInstanceState);
		
		init();
		
    }
    
    @Override
    public void onStart() {
    	super.onStart();
    }
    
    @Override
    protected void onResume(){
		super.onResume();
		
		if(appPrefs == null){
    		appPrefs = new AppPreferences(LandingActivity.this);
    	}
		
		if(appPrefs.getEmergencyStatus()){
        	Intent intent = new Intent(this, EmergencyActivity.class);
    		startActivity(intent);
    		this.finish();
        }
		
		//Utils.updateGA(this, getString(R.string.ga_screen_landing));
    	
		// facebook
    	//uiHelper.onResume();
		// Logs 'install' and 'app activate' App Events.
		//AppEventsLogger.activateApp(this);
    }
    
    @Override
    public void onPause() {
        super.onPause();
        
        // facebook
        //uiHelper.onPause();
        // Logs 'app deactivate' App Event.
        //AppEventsLogger.deactivateApp(this);
    }
    
    @Override
    protected void onDestroy(){
    	super.onDestroy();
    	
    	// facebook
    	//uiHelper.onDestroy();
    }
    
    @Override
	protected void onStop() {
    	super.onStop();
    	
	}
    
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        	finish();
        	
        }else if (keyCode == KeyEvent.KEYCODE_MENU) {
        
        }else if (keyCode == KeyEvent.KEYCODE_HOME) {
        
        }

        return false;
        //return super.onKeyDown(keyCode, event);
    }
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode, resultCode, data);
	}
	
    @Override
	public void onLowMemory(){
    	System.gc();
	}
	
    
    @Override
	public void onSaveInstanceState (Bundle outState) {
    	super.onSaveInstanceState(outState);
    	
    	// facebook
	    //uiHelper.onSaveInstanceState(outState);
    }
   
    /*
    @Override
    protected void onRestoreInstanceState (Bundle savedInstanceState) {
    	super.onRestoreInstanceState(savedInstanceState);
    }
    */
    
    private void init(){
    	
    	//hide keybaord
    	getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    	//context = LandingActivity.this;
    	
    	/*
    	jsonCallback = LandingActivity.this;
        nameValuePairs = new ArrayList<NameValuePair>();
        popupCallback = LandingActivity.this;
        */
    	
    	// facebook
    	//fbUserInfo = new FacebookUserInfo();
    	
    	if(appPrefs == null){
    		appPrefs = new AppPreferences(LandingActivity.this);
    	}

		tvLandingTitle = (TextView)findViewById(R.id.tv_landing_title);
		tvLandingTitle.setTypeface(Utils.getFontAvenirLight(LandingActivity.this));

		tvLandingMessage = (TextView)findViewById(R.id.tv_landing_message);
		tvLandingMessage.setTypeface(Utils.getFontAvenirLight(LandingActivity.this));

		layoutBtnSignIn = (RelativeLayout)findViewById(R.id.layout_btn_sign_in);
		btnSignIn = (Button)findViewById(R.id.btn_sign_in);
		btnSignIn.setTypeface(Utils.getFontAvenirBook(LandingActivity.this));
		btnSignIn.setOnClickListener(this);

		layoutBtnRegister = (RelativeLayout)findViewById(R.id.layout_btn_register);
		btnRegister = (Button)findViewById(R.id.btn_register);
		btnRegister.setTypeface(Utils.getFontAvenirBook(LandingActivity.this));
		btnRegister.setOnClickListener(this);

		btnSkip = (Button)findViewById(R.id.btn_skip);
		btnSkip.setTypeface(Utils.getFontAvenirBook(LandingActivity.this));
		btnSkip.setOnClickListener(this);

		YoYo.with(Techniques.FadeIn)
				.duration(Constants.animationTime * (animationIndex))
				.playOn(layoutBtnSignIn);

		YoYo.with(Techniques.FadeIn)
				.duration(Constants.animationTime * (animationIndex))
				.playOn(layoutBtnRegister);

		YoYo.with(Techniques.FadeIn)
				.duration(Constants.animationTime * (animationIndex))
				.playOn(btnSkip);

		YoYo.with(Techniques.FadeIn)
				.duration(Constants.animationTime * (animationIndex * 2))
				.playOn(tvLandingTitle);

		YoYo.with(Techniques.FadeIn)
				.duration(Constants.animationTime * (animationIndex * 4))
				.playOn(tvLandingMessage);

    }

	@Override
	public void onClick(View v) {
		if(v == btnSkip){
			Intent intent = new Intent(LandingActivity.this, MainActivity.class);
			startActivity(intent);
			//this.finish();
		}else if(v == btnRegister){
			Intent intent = new Intent(LandingActivity.this, RegisterActivity.class);
			startActivityForResult(intent, Constants.SCREEN_LANDING);
		}else if(v == btnSignIn){
			Intent intent = new Intent(LandingActivity.this, SignInActivity.class);
			startActivityForResult(intent, Constants.SCREEN_LANDING);
		}
	}

}

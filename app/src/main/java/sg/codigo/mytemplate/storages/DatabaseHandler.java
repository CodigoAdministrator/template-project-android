package sg.codigo.mytemplate.storages;

import java.util.ArrayList;
import java.util.List;

import sg.codigo.mytemplate.models.CustomNotification;
import sg.codigo.mytemplate.utils.Constants;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {
	private Context context;
	private AppPreferences appPrefs;
	//public SQLiteDatabase db;
    
	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DB_NAME = "";
	
	// Database path
	//private static final String DB_PATH = "/data/data/(packagename)/databases/";
	//private String DB_PATH = "/data/data/com.codigo.fastfast/databases/";
    
	
	// Table name
	private static final String TABLE_NOTIFICATION = "tbl_notification";
	

	// Notification
	private static final String KEY_NOTIFICATION_ID = "id";
	private static final String KEY_NOTIFICATION_FROM = "frm";
	private static final String KEY_NOTIFICATION_TIMESTAMP = "timestamp";
	private static final String KEY_NOTIFICATION_TYPE = "type";
	private static final String KEY_NOTIFICATION_MESSAGE = "message";
	private static final String KEY_NOTIFICATION_SOUND = "sound";
	private static final String KEY_NOTIFICATION_COLLAPSE_KEY = "collapse_key";
		
		
    public DatabaseHandler(Context context) { 
    	super(context, DB_NAME, null, DATABASE_VERSION);
        this.context = context;
        this.appPrefs = new AppPreferences(this.context);
        
        //SQLiteDatabase db = this.getWritableDatabase();
        
    }

	private String CREATE_NOTIFICATION_TABLE = 
			"CREATE TABLE IF NOT EXISTS " + TABLE_NOTIFICATION 
			+ "("
			+ KEY_NOTIFICATION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," 
			+ KEY_NOTIFICATION_FROM + " TEXT,"
			+ KEY_NOTIFICATION_TIMESTAMP + " TEXT,"
			+ KEY_NOTIFICATION_TYPE + " TEXT,"
			+ KEY_NOTIFICATION_MESSAGE + " TEXT,"
			+ KEY_NOTIFICATION_SOUND + " TEXT,"
			+ KEY_NOTIFICATION_COLLAPSE_KEY + " TEXT"
			+ ")";
	
	
	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		
		db.execSQL(CREATE_NOTIFICATION_TABLE);
		
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIFICATION);
		
		// Create tables again
		onCreate(db);
		
	}

	
	//
	//All CRUD(Create, Read, Update, Delete) Operations
	//

	// Notification
	/*
 	// Add Notification
	public long addNotification(CustomNotification notification, String customerToken) {
		long rowId = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		
		// Inserting Row
		try{
			boolean isExists = isNotificationExists(notification, customerToken);
			if(isExists){
				//this.deleteAddr(addr);
				this.updateNotification(notification, customerToken);
			}else{
			
				
				ContentValues values = new ContentValues();
				
				values.put(KEY_NOTIFICATION_FROM, notification.getFrom());
				values.put(KEY_NOTIFICATION_TIMESTAMP, notification.getTimestamp());
				values.put(KEY_NOTIFICATION_TYPE, notification.getType());
				values.put(KEY_NOTIFICATION_MESSAGE, notification.getMessage());
				values.put(KEY_NOTIFICATION_SOUND, notification.getSound());
				values.put(KEY_NOTIFICATION_COLLAPSE_KEY, notification.getCollapseKey());
				
				rowId = db.insertOrThrow(TABLE_NOTIFICATION, null, values);
				if(Constants.DEBUG_MODE){
					Log.d(Constants.TAG, TABLE_NOTIFICATION + "-data rowId: " + rowId);
				}
			}
		}catch (SQLException e){
			//e.printStackTrace();
			if(e!=null){
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, "SQLException: " + e.toString());
				}
			}
		}catch (Exception e){
			//e.printStackTrace();
			if(e!=null){
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, "Exception: " + e.toString());
				}
			}
		}
		//if(db!=null && db.isOpen()){
			//db.close(); // Closing database connection
		//}
		return rowId;
	}
	
	// Check notification is exists
	public boolean isNotificationExists(CustomNotification notification, String customerToken) {
		SQLiteDatabase db = this.getWritableDatabase();
		
		boolean isExists = false;
		
		String selectQuery = "SELECT COUNT(*) FROM " + TABLE_NOTIFICATION
				+ " WHERE " + KEY_NOTIFICATION_BOOKING_ID + " = \"" +  notification.getBookingId() + "\""
				+ " AND " +  KEY_NOTIFICATION_CUSTOMER_TOKEN + " = \"" + customerToken + "\""
				;
		
		Cursor cursor = db.rawQuery(selectQuery, null);
		try{
			if (cursor.moveToFirst()) {
				isExists = cursor.getInt(0) > 0;
			}
		}catch(NumberFormatException e){
			if(e!=null){
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, "NumberFormatException: " + e.toString());
				}
			}
		}catch(Exception e){
			if(e!=null){
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, "Exception: " + e.toString());
				}
			}
		}
		finally{
			cursor.close();
		}
		//if(db!=null && db.isOpen()){
			//db.close(); // Closing database connection
		//}
		return isExists;
	}

	// Update Notification
	public long updateNotification(CustomNotification notification, String customerToken) {
		long rowId = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		
		// updating Row
		try{
			ContentValues values = new ContentValues();
			
			values.put(KEY_NOTIFICATION_DRIVER_ID, notification.getDriverId());
			values.put(KEY_NOTIFICATION_FROM, notification.getFrom());
			values.put(KEY_NOTIFICATION_TIMESTAMP, notification.getTimestamp());
			values.put(KEY_NOTIFICATION_TYPE, notification.getType());
			values.put(KEY_NOTIFICATION_MESSAGE, notification.getMessage());
			values.put(KEY_NOTIFICATION_SOUND, notification.getSound());
			values.put(KEY_NOTIFICATION_COLLAPSE_KEY, notification.getCollapseKey());
			
			rowId = db.update(
					TABLE_NOTIFICATION, 
					values, 
					KEY_NOTIFICATION_BOOKING_ID + " = ? AND " + KEY_NOTIFICATION_CUSTOMER_TOKEN + "=?",
					new String[] {String.valueOf(notification.getBookingId()), customerToken}
					);
			if(Constants.DEBUG_MODE){
				Log.d(Constants.TAG, TABLE_NOTIFICATION + "-data rowId: " + rowId);
			}
			
		}catch (SQLException e){
			//e.printStackTrace();
			if(e!=null){
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, "SQLException: " + e.toString());
				}
			}
		}catch (Exception e){
			//e.printStackTrace();
			if(e!=null){
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, "Exception: " + e.toString());
				}
			}
		}
		//if(db!=null && db.isOpen()){
			//db.close(); // Closing database connection
		//}
		return rowId;
	}

	// get all Notification
	public List<CustomNotification> getAllNotification(String customerToken) {
		SQLiteDatabase db = this.getWritableDatabase();
		
		List<CustomNotification> notificationList = new ArrayList<CustomNotification>();
		String selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION
							// + " WHERE " + KEY_NOTIFICATION_CUSTOMER_TOKEN + " =\"" + customerToken + "\""
							;
			
		Cursor cursor = db.rawQuery(selectQuery, null);
		
		if(Constants.DEBUG_MODE){
			Log.d(Constants.TAG, "selectQuery: " + selectQuery);
			Log.d(Constants.TAG, "getAllNotification cursor.getCount(): " + cursor.getCount());
		}
		try{
			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
			
				do {
				
					CustomNotification notification= new CustomNotification();
					notification.setId(cursor.getInt(cursor.getColumnIndex(KEY_NOTIFICATION_ID)));
					notification.setFrom(cursor.getString(cursor.getColumnIndex(KEY_NOTIFICATION_FROM)));
					notification.setTimestamp(cursor.getString(cursor.getColumnIndex(KEY_NOTIFICATION_TIMESTAMP)));
					notification.setType(cursor.getString(cursor.getColumnIndex(KEY_NOTIFICATION_TYPE)));
					notification.setMessage(cursor.getString(cursor.getColumnIndex(KEY_NOTIFICATION_MESSAGE)));
					notification.setSound(cursor.getString(cursor.getColumnIndex(KEY_NOTIFICATION_SOUND)));
					notification.setCollapseKey(cursor.getString(cursor.getColumnIndex(KEY_NOTIFICATION_COLLAPSE_KEY)));
					
					notificationList.add(notification);
					
				
				} while (cursor.moveToNext());
			
			}
		}catch(Exception e){
			if(e!=null){
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, "Exception: " + e.toString());
				}
			}
		}
		finally{
			cursor.close();
		}
		
		//if(db!=null && db.isOpen()){
			//db.close(); // Closing database connection
		//}
		return notificationList;
	}
		
	// get Notification by id 
	public CustomNotification getNotificationByBookingId(String bookingId, String customerToken) {
		SQLiteDatabase db = this.getWritableDatabase();
		
		CustomNotification notification = null;
		String selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION 
							+ " WHERE " + KEY_NOTIFICATION_BOOKING_ID + " = \"" + bookingId + "\"" 
							+ " AND " + KEY_NOTIFICATION_CUSTOMER_TOKEN + " =\"" + customerToken + "\"";
		
		Cursor cursor = db.rawQuery(selectQuery, null);
		
		if(Constants.DEBUG_MODE){
			Log.d(Constants.TAG, "selectQuery: " + selectQuery);
			Log.d(Constants.TAG, "getNotificationByBookingId cursor.getCount(): " + cursor.getCount());
		}
		
		try{
			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				notification = new CustomNotification();
				notification.setId(cursor.getInt(cursor.getColumnIndex(KEY_NOTIFICATION_ID)));
				notification.setBookingId(cursor.getString(cursor.getColumnIndex(KEY_NOTIFICATION_BOOKING_ID)));
				notification.setDriverId(cursor.getString(cursor.getColumnIndex(KEY_NOTIFICATION_DRIVER_ID)));
				notification.setFrom(cursor.getString(cursor.getColumnIndex(KEY_NOTIFICATION_FROM)));
				notification.setTimestamp(cursor.getString(cursor.getColumnIndex(KEY_NOTIFICATION_TIMESTAMP)));
				notification.setType(cursor.getString(cursor.getColumnIndex(KEY_NOTIFICATION_TYPE)));
				notification.setMessage(cursor.getString(cursor.getColumnIndex(KEY_NOTIFICATION_MESSAGE)));
				notification.setSound(cursor.getString(cursor.getColumnIndex(KEY_NOTIFICATION_SOUND)));
				notification.setCollapseKey(cursor.getString(cursor.getColumnIndex(KEY_NOTIFICATION_COLLAPSE_KEY)));
				
				
			}
		}catch(Exception e){
			if(e!=null){
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, "Exception: " + e.toString());
				}
			}
		}finally{
			cursor.close();
		}
		
		//if(db!=null && db.isOpen()){
			//db.close(); // Closing database connection
		//}
		return notification;
	}
	
	
	// Deleting single notification
	public void deleteNotification(int id, String customerToken) {
		SQLiteDatabase db = this.getWritableDatabase();
		
		int rowDeleted = db.delete(
				TABLE_NOTIFICATION, 
				KEY_NOTIFICATION_ID + "=? AND " + KEY_NOTIFICATION_CUSTOMER_TOKEN + "=?",
				new String[] {String.valueOf(id), customerToken}
				);
			if(rowDeleted>0){
				if(Constants.DEBUG_MODE){
					Log.d(Constants.TAG, TABLE_NOTIFICATION + "-data deleted: " + rowDeleted);
				}
			}
		
		//if(db!=null && db.isOpen()){
			//db.close(); // Closing database connection
		//}
	}

	// Deleting all notifications
	public void deleteAllNotifications(String customerToken) {
		SQLiteDatabase db = this.getWritableDatabase();
		
		int rowDeleted = db.delete(
				TABLE_NOTIFICATION, 
				KEY_NOTIFICATION_CUSTOMER_TOKEN + "=?",
				new String[] {customerToken}
					);
			if(rowDeleted>0){
				if(Constants.DEBUG_MODE){
					Log.d(Constants.TAG, TABLE_NOTIFICATION + "-data deleted: " + rowDeleted);
				}
			}
		
		//if(db!=null && db.isOpen()){
			//db.close(); // Closing database connection
		//}
	}
	*/
}

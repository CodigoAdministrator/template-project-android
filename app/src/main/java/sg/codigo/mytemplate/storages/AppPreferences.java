package sg.codigo.mytemplate.storages;

import sg.codigo.mytemplate.models.FacebookUserInfo;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;

public class AppPreferences {
     private static final String APP_SHARED_PREFS = "sg.codigo.mytemplate.app_preferences"; //  Name of the file -.xml
     private SharedPreferences appSharedPrefs;
     private Editor prefsEditor;

     public AppPreferences(Context context)
     {
         this.appSharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS, Activity.MODE_PRIVATE);
         this.prefsEditor = appSharedPrefs.edit();
     }

     public void clearPreferences(){
    	 
     }
     
     
     // Facebook
     public FacebookUserInfo getLastSignInUser() {
    	 FacebookUserInfo fbUserInfo = new FacebookUserInfo();
    	 fbUserInfo.setId(appSharedPrefs.getString("last_fb_user_id", ""));
    	 fbUserInfo.setName(appSharedPrefs.getString("last_fb_user_name", ""));
    	 fbUserInfo.setEmail(appSharedPrefs.getString("last_fb_user_email", ""));
    	 fbUserInfo.setGender(appSharedPrefs.getString("last_fb_user_gender", ""));
    	 fbUserInfo.setDOB(appSharedPrefs.getString("last_fb_user_dob", ""));
    	 
    	 return fbUserInfo;
     }

     public void setLastSignInUser(FacebookUserInfo fbUserInfo){
    	 prefsEditor.putString("last_fb_user_id", fbUserInfo.getId());
    	 prefsEditor.putString("last_fb_user_name", fbUserInfo.getName());
    	 prefsEditor.putString("last_fb_user_email", fbUserInfo.getEmail());
    	 prefsEditor.putString("last_fb_user_gender", fbUserInfo.getGender());
    	 prefsEditor.putString("last_fb_user_dob", fbUserInfo.getDOB());
    	 prefsEditor.commit();
     }
     
     public boolean getFBLoginStatus(){
    	 return appSharedPrefs.getBoolean("fb_log_in_status", false);
     }
     
     public void setFBLoginStatus(boolean loginStatus){
    	 prefsEditor.putBoolean("fb_log_in_status", loginStatus);
    	 prefsEditor.commit();
     }
     
     public String getFBToken() {
    	 String str=appSharedPrefs.getString("fb_token", "");
         return str;
     }

     public void setFBToken(String fbToken) {
         prefsEditor.putString("fb_token", fbToken);
         prefsEditor.commit();
     }
     
     /*
     public String getServerToken() {
    	 String str=appSharedPrefs.getString("server_token", "");
         return str;
     }

     public void setServerToken(String serverToken) {
         prefsEditor.putString("server_token", serverToken);
         prefsEditor.commit();
     }
     */
     
     public FacebookUserInfo getFBUserInfo(){
    	 FacebookUserInfo fbUserInfo = new FacebookUserInfo();
    	 fbUserInfo.setId(appSharedPrefs.getString("fb_user_id", ""));
    	 fbUserInfo.setName(appSharedPrefs.getString("fb_user_name", ""));
    	 fbUserInfo.setEmail(appSharedPrefs.getString("fb_user_email", ""));
    	 fbUserInfo.setGender(appSharedPrefs.getString("fb_user_gender", ""));
    	 fbUserInfo.setDOB(appSharedPrefs.getString("fb_user_dob", ""));
    	 
    	 return fbUserInfo;
     }
     
     public void setFBUserInfo(FacebookUserInfo fbUserInfo){
    	 prefsEditor.putString("fb_user_id", fbUserInfo.getId());
    	 prefsEditor.putString("fb_user_name", fbUserInfo.getName());
    	 prefsEditor.putString("fb_user_email", fbUserInfo.getEmail());
    	 prefsEditor.putString("fb_user_gender", fbUserInfo.getGender());
    	 prefsEditor.putString("fb_user_dob", fbUserInfo.getDOB());
    	 prefsEditor.commit();
     }
     
     public void clearFBUserInfo(){
    	 prefsEditor.putString("fb_user_id", "");
    	 prefsEditor.putString("fb_user_name", "");
    	 prefsEditor.putString("fb_user_email", "");
    	 prefsEditor.putString("fb_user_gender", "");
    	 prefsEditor.putString("fb_user_dob", "");
    	 prefsEditor.commit();
     }
     
     /*
     // Twitter
     public String getString(String key){
    	 return appSharedPrefs.getString(key, "");
     }
     
     public void setString(String key, String value){
    	 prefsEditor.putString(key, value);
    	 prefsEditor.commit();
     }
     
     public void removeString(String key){
    	 prefsEditor.remove(key);
    	 prefsEditor.commit();
     }
     */
     
     // GCM
     public String getGCMRegistrantId() {
         return appSharedPrefs.getString("gcm_registrant_id", "");
     }

     public void setGCMRegistrantId(String gcmRegistrantId) {
         prefsEditor.putString("gcm_registrant_id", gcmRegistrantId);
         prefsEditor.commit();
     }

     public boolean getIsTokenSentToServer(){
         return appSharedPrefs.getBoolean("is_token_sent_to_server", false);
     }

     public void setIsTokenSentToServer(boolean isTokenSentToServer) {
         prefsEditor.putBoolean("is_token_sent_to_server", isTokenSentToServer);
         prefsEditor.commit();
     }



    public boolean getIsAgreedTNC() {
         return appSharedPrefs.getBoolean("agreed_tnc", false);
     }

     public void setIsAgreedTNC(boolean isAgreedTNC) {
         prefsEditor.putBoolean("agreed_tnc", isAgreedTNC);
         prefsEditor.commit();
     }
     
     public boolean getIsFirstLaunch() {
         return appSharedPrefs.getBoolean("first_launch", true);
     }

     public void setIsFirstLaunch(boolean isFirstLaunch) {
         prefsEditor.putBoolean("first_launch", isFirstLaunch);
         prefsEditor.commit();
     }
     
     public boolean getIsLoggedIn() {
         return appSharedPrefs.getBoolean("logged_in", false);
     }

     public void setIsLoggedIn(boolean isLoggedIn) {
         prefsEditor.putBoolean("logged_in", isLoggedIn);
         prefsEditor.commit();
     }
     
     // FromScreen
     public int getFromScreen() {
         return appSharedPrefs.getInt("fromScreen", 0);
     }

     public void setFromScreen(int fromScreen) {
         prefsEditor.putInt("fromScreen", fromScreen);
         prefsEditor.commit();
     }

     public String getLastLoggedInEmail() {
         return appSharedPrefs.getString("last_logged_in_email", "");
     }

     public void setLastLoggedInEmail(String lastLoggedInEmail) {
         prefsEditor.putString("last_logged_in_email", lastLoggedInEmail);
         prefsEditor.commit();
     }

     public String getAppVersionName() {
         return appSharedPrefs.getString("app_version_name", "1.0.0");
     }

     public void setAppVersionName(String appVersionName) {
         prefsEditor.putString("app_version_name", appVersionName);
         prefsEditor.commit();
     }
     
     public boolean getIsForceUpdate() {
         return appSharedPrefs.getBoolean("is_force_update", false);
     }

     public void setIsForceUpdate(boolean isForceUpdate) {
         prefsEditor.putBoolean("is_force_update", isForceUpdate);
         prefsEditor.commit();
     }
     
     public String getAppVersionMessage() {
         return appSharedPrefs.getString("app_version_message", "");
     }

     public void setAppVersionMessage(String appVersionMessage) {
         prefsEditor.putString("app_version_message", appVersionMessage);
         prefsEditor.commit();
     }
     
     public boolean getEmergencyStatus() {
         return appSharedPrefs.getBoolean("emergency_status", false);
     }

     public void setEmergencyStatus(boolean emergencyStatus) {
         prefsEditor.putBoolean("emergency_status", emergencyStatus);
         prefsEditor.commit();
     }
     
     public String getEmergencyMessage() {
         return appSharedPrefs.getString("emergency_message", "");
     }

     public void setEmergencyMessage(String emergencyMessage) {
         prefsEditor.putString("emergency_message", emergencyMessage);
         prefsEditor.commit();
     }
     
     public String getEmergencyRedirectURL() {
         return appSharedPrefs.getString("emergency_redirect_url", "");
     }

     public void setEmergencyRedirectURL(String emergencyRedirectURL) {
         prefsEditor.putString("emergency_redirect_url", emergencyRedirectURL);
         prefsEditor.commit();
     }
     
     public String getOptionalUpdateLastCheckingDate() {
         return appSharedPrefs.getString("optional_update_last_checking_date", "2015-06-01");
     }

     public void setOptionalUpdateLastCheckingDate(String optionalUpdateLastCheckingDate) {
         prefsEditor.putString("optional_update_last_checking_date", optionalUpdateLastCheckingDate);
         prefsEditor.commit();
     }
     
     public String getMasterDataJSON() {
         return appSharedPrefs.getString("master_data_json", "");
     }

     public void setMasterDataJSON(String masterDataJSON) {
         prefsEditor.putString("master_data_json", masterDataJSON);
         prefsEditor.commit();
     }
     
     public boolean getIsLoginThruFB() {
         return appSharedPrefs.getBoolean("is_login_thru_fb", false);
     }

     public void setIsLoginThruFB(boolean isLoginThruFB) {
         prefsEditor.putBoolean("is_login_thru_fb", isLoginThruFB);
         prefsEditor.commit();
     }
     
     public String getActivationCode(){
    	 return appSharedPrefs.getString("activation_code", "");
     }
     
     public void setActivationCode(String activationCode) {
    	 prefsEditor.putString("activation_code", activationCode);
         prefsEditor.commit();
     }
}
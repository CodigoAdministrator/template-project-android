package sg.codigo.mytemplate;

import sg.codigo.mytemplate.callbacks.JsonCallback;
import sg.codigo.mytemplate.callbacks.PopupCallback;
import sg.codigo.mytemplate.dialogs.DialogOK;
import sg.codigo.mytemplate.gcm.QuickstartPreferences;
import sg.codigo.mytemplate.gcm.RegistrationIntentService;
import sg.codigo.mytemplate.storages.AppPreferences;
import sg.codigo.mytemplate.utils.Constants;
import sg.codigo.mytemplate.utils.HTTPGetAsyncTask;
import sg.codigo.mytemplate.utils.Utils;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
//import com.google.android.gcm.GCMRegistrar;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.TextView;
import android.os.Handler;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public class SplashActivity extends Activity implements JsonCallback, PopupCallback{
	// data
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	private BroadcastReceiver mRegistrationBroadcastReceiver;
	//AsyncTask<Void, Void, Void> mRegisterTask;

	private Handler handler;
	private AppPreferences appPrefs;
	private JsonCallback jsonCallback;
	//private ArrayList<NameValuePair> nameValuePairsMasterData;
	private DialogOK dialogError;
	private PopupCallback popupCallback;
	
	// layout
	private TextView tvWelcome;
	
		
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		Fresco.initialize(SplashActivity.this);

		if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) { 
            // Activity was brought to front and not created, 
            // Thus finishing this will get us to the last viewed activity 
            finish(); 
            return; 
        } 
		
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
		
        setContentView(R.layout.activity_splash);
        
        handler = new Handler();
        appPrefs = new AppPreferences(SplashActivity.this);
        popupCallback = SplashActivity.this;


		// new gcm
		mRegistrationBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				boolean sentToken = appPrefs.getIsTokenSentToServer();
				if (sentToken) {
					//mInformationTextView.setText(getString(R.string.gcm_send_message));
					if(Constants.DEBUG_MODE){
						Log.d(Constants.TAG, getString(R.string.gcm_send_message));
					}
				} else {
					//mInformationTextView.setText(getString(R.string.token_error_message));
					if(Constants.DEBUG_MODE){
						Log.d(Constants.TAG, getString(R.string.token_error_message));
					}
				}
			}
		};

		if (checkPlayServices()) {
			// Start IntentService to register this application with GCM.
			Intent intent = new Intent(this, RegistrationIntentService.class);
			startService(intent);

			init();

			Runnable r = new Runnable()
			{
				public void run()
				{
					Intent mainIntent = new Intent(SplashActivity.this, TnCActivity.class);
					mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
					SplashActivity.this.startActivity(mainIntent);
					SplashActivity.this.finish();

				}
			};

			handler.postDelayed(r, Constants.SPLASH_DISPLAY_TIME_MILLISECONDS);

		}else{
			if(Constants.DEBUG_MODE){
				Log.i(Constants.TAG, "This device is not supported.");
			}
		}


		// old gcm
		/*
        // Check device for Play Services APK.
        if (checkPlayServices()) {
        	registerGCM();
        	
        	init();
            
            Runnable r = new Runnable()
            {
                public void run() 
                {
            		Intent mainIntent = new Intent(SplashActivity.this, TnCActivity.class);
            		mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                	SplashActivity.this.startActivity(mainIntent);
                	SplashActivity.this.finish();
                    
                }
            };

            handler.postDelayed(r, Constants.SPLASH_DISPLAY_TIME_MILLISECONDS);
            
        }else{
        	if(Constants.DEBUG_MODE){
            	Log.i(Constants.TAG, "This device is not supported.");
        	}
        }
        */
    }
    
    @Override
    public void onStart() {
    	super.onStart();
    }
    
    @Override
    public void onResume(){
    	super.onResume();
    	//checkPlayServices();
    	//Utils.initGA(SplashActivity.this);
    	Utils.updateGA(SplashActivity.this, getString(R.string.ga_screen_splash));

		LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
				new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
    }

	@Override
	protected void onPause() {
		LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
		super.onPause();
	}

    @Override
    public void onStop() {
    	super.onStop();
    }
    
    @Override
    protected void onDestroy() {
		// old GCM
		/*
        if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }
        try{
        	unregisterReceiver(mHandleMessageReceiver);
        }catch(IllegalArgumentException e){
        	if(Constants.DEBUG_MODE){
        		Log.e(Constants.TAG, "IllegalArgumentException: " + e.toString());
        	}
        }catch(Exception e){
        	if(Constants.DEBUG_MODE){
				Log.e(Constants.TAG, "Exception: " + e.toString());
        	}
        }
        GCMRegistrar.onDestroy(this);
        */

        super.onDestroy();
    }
    
    @Override
	public void onLowMemory(){
    	System.gc();
	}

	// old gcm
    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
	/*
    private boolean checkPlayServices() {
        final int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(SplashActivity.this);
        if(Constants.DEBUG_MODE){
        	Log.i(Constants.TAG, "checkGooglePlayServicesAvailable, resultCode=" + resultCode);
        }
        if (resultCode != ConnectionResult.SUCCESS) {
        	
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
            	Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, SplashActivity.this, PLAY_SERVICES_RESOLUTION_REQUEST);
            	if (dialog != null) {
                    dialog.show();
                    dialog.setOnDismissListener(new OnDismissListener() {
                        public void onDismiss(DialogInterface dialog) {
                            if (ConnectionResult.SERVICE_INVALID == resultCode) finish();
                        }
                    });
                    return false;
                }
            } else {
            	if(Constants.DEBUG_MODE){
                	Log.i(Constants.TAG, "This device is not supported.");
            	}
            	if(dialogError==null || !dialogError.isShowing()){
    				dialogError = new DialogOK(SplashActivity.this, getString(R.string.dialog_title_error), "This device is not supported.", getString(R.string.btn_ok), Constants.POPUP_API_CALL_FAILED, popupCallback);
    				dialogError.show();
    			}
            	
            	//finish();
            }
            return false;
        }
        return true;
    }
    */

	// new gcm
	/**
	 * Check the device to make sure it has the Google Play Services APK. If
	 * it doesn't, display a dialog that allows users to download the APK from
	 * the Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkPlayServices() {
		GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
		int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (apiAvailability.isUserResolvableError(resultCode)) {
				apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				if(Constants.DEBUG_MODE){
					Log.i(Constants.TAG, "This device is not supported.");
				}
				if(dialogError==null || !dialogError.isShowing()){
					dialogError = new DialogOK(SplashActivity.this, getString(R.string.dialog_title_error), "This device is not supported.", getString(R.string.btn_ok), Constants.POPUP_API_CALL_FAILED, popupCallback);
					dialogError.show();
				}
			}
			return false;
		}
		return true;
	}

	/*
    private void registerGCM(){
    	checkNotNull(SERVER_URL, "SERVER_URL");
        checkNotNull(Constants.getGCMSenderID(), "SENDER_ID");
        // Make sure the device has the proper dependencies.
        GCMRegistrar.checkDevice(this);
        // Make sure the manifest was properly set - comment out this line
        // while developing the app, then uncomment it when it's ready.
        GCMRegistrar.checkManifest(this);
        
        registerReceiver(mHandleMessageReceiver, new IntentFilter(DISPLAY_MESSAGE_ACTION));
        final String regId = GCMRegistrar.getRegistrationId(this);
        //appPrefs.setGCMRegistrantId(regId);
        if(Constants.DEBUG_MODE){
        	Log.d(Constants.TAG, "registerGCM 01");
        }
        if (regId.equals("")) {
            // Automatically registers application on startup.
            GCMRegistrar.register(this, Constants.getGCMSenderID());
            if(Constants.DEBUG_MODE){
            	Log.d(Constants.TAG, "registerGCM 02");
            }
        } else {
        	
        	appPrefs.setGCMRegistrantId(regId);
        	
            // Device is already registered on GCM, check server.
            if (GCMRegistrar.isRegisteredOnServer(this)) {
                // Skips registration.
                //mDisplay.append(getString(R.string.already_registered) + "\n");
            	if(Constants.DEBUG_MODE){
                	Log.d(Constants.TAG, "registerGCM 03");
                }
            	
            	if(Constants.DEBUG_MODE){
                	Log.d(Constants.TAG, getString(R.string.already_registered));
                }
            	//appPrefs.setGCMRegistrantId(regId);
            	
            } else {
            	if(Constants.DEBUG_MODE){
                	Log.d(Constants.TAG, "registerGCM 04");
                }
                // Try to register again, but not in the UI thread.
                // It's also necessary to cancel the thread onDestroy(),
                // hence the use of AsyncTask instead of a raw thread.
                final Context context = this;
                mRegisterTask = new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {
                        ServerUtilities.register(context, regId);
                        //appPrefs.setGCMRegistrantId(regId);
                        if(Constants.DEBUG_MODE){
                        	Log.d(Constants.TAG, "registerGCM 05");
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {
                        mRegisterTask = null;
                    }

                };
                mRegisterTask.execute(null, null, null);
            }
        }
    }
    */

    
    private void init(){
    	jsonCallback = SplashActivity.this;
    	
    	//nameValuePairsMasterData = new ArrayList<NameValuePair>();
    	//new HTTPGetAsyncTask(SplashActivity.this, Constants.API_GET_MASTER_DATA, jsonCallback, Constants.ID_API_GET_MASTER_DATA, false, false);
    	
    	tvWelcome = (TextView) findViewById(R.id.tv_welcome);
    	tvWelcome.setTypeface(Utils.getFontAvenirLight(SplashActivity.this));
    	
    	YoYo.with(Techniques.Landing)
        	.duration(Constants.SPLASH_DISPLAY_TIME_MILLISECONDS)
        	.playOn(tvWelcome);
    	
    	
    }

	// old gcm
	/*
	private void checkNotNull(Object reference, String name) {
        if (reference == null) {
            throw new NullPointerException(getString(R.string.error_config, name));
        }
    }

    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            if(Constants.DEBUG_MODE){
            	Log.d(Constants.TAG, newMessage);
            }
        }
    };
	*/

	@Override
	public void callbackJson(String result, int processID, int index) {
		if(Constants.DEBUG_MODE){
        	Log.d(Constants.TAG, "HTTPGetAsyncTask 03");
        }
		if(Constants.DEBUG_MODE){
        	Log.d(Constants.TAG, "processID: " + processID);
        }
		if(processID == Constants.ID_API_GET_MASTER_DATA){
			
			if(result!=null && !result.equalsIgnoreCase("")){
				try{
					JSONObject jsonObj = new JSONObject(result);
					
					int errorCode = jsonObj.getInt("errorCode");
					String message = jsonObj.getString("message");
					if(Constants.DEBUG_MODE){
		            	Log.d(Constants.TAG, "errorCode: " + errorCode);
		            	Log.d(Constants.TAG, "message: " + message);
		            }
					if(errorCode == Constants.ERROR_CODE_SUCCESS){
						if(jsonObj.has("app_version") && jsonObj.getString("app_version").startsWith("{")){
							//appPrefs.setAppVersionJSON(jsonObj.getString("app_version"));
							JSONObject appVersionObj = jsonObj.getJSONObject("app_version");
							if(appVersionObj.has("android") && appVersionObj.getString("android").startsWith("{")){
								JSONObject androidAppVersionObj = appVersionObj.getJSONObject("android");
								String appVersionName = androidAppVersionObj.has("version")?androidAppVersionObj.getString("version"):"1.0.0";
								boolean isForceUpdate = androidAppVersionObj.has("force_update")?androidAppVersionObj.getBoolean("force_update"):false;
								String appVersionMessage = androidAppVersionObj.has("message")?androidAppVersionObj.getString("message"):"";
								appPrefs.setAppVersionName(appVersionName);
								appPrefs.setIsForceUpdate(isForceUpdate);
								appPrefs.setAppVersionMessage(appVersionMessage);
							}
							
						}
						if(jsonObj.has("emergency") && jsonObj.getString("emergency").startsWith("{")){
							JSONObject emergencyObj = jsonObj.getJSONObject("emergency");
							
							boolean emergencyStatus = emergencyObj.has("status")?emergencyObj.getBoolean("status"):false;
							String emergencyMessage = emergencyObj.has("message")?emergencyObj.getString("message"):"";
							String emergencyRedirectURL = emergencyObj.has("redirect")?emergencyObj.getString("redirect"):"";
							
							appPrefs.setEmergencyStatus(emergencyStatus);
							appPrefs.setEmergencyMessage(emergencyMessage);
							appPrefs.setEmergencyRedirectURL(emergencyRedirectURL);
							
							if(Constants.DEBUG_MODE){
								Log.e(Constants.TAG, "emergencyStatus: " + emergencyStatus);
							}
						}
						if(jsonObj.has("master_data")){
							appPrefs.setMasterDataJSON(jsonObj.getString("master_data"));
						}
						
					}else{
						if(Constants.DEBUG_MODE){
							Log.e(Constants.TAG, "JSONException: " + message);
						}
					}
				}catch(JSONException e){
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "JSONException: " + e.toString());
					}
				}catch(Exception e){
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "Exception: " + e.toString());
					}
				}
			}
		}
	}

	@Override
	public void jsonError(String msg, int processID) {
		if(processID == Constants.ID_API_GET_MASTER_DATA){
			if(Constants.DEBUG_MODE){
				Log.i(Constants.TAG, "jsonError: " + msg);
			}
		}
	}

	@Override
	public void callBackPopup(Object data, int processID, int index) {
		if(processID == Constants.POPUP_API_CALL_FAILED){
			this.finish();
		}
	}

	
}

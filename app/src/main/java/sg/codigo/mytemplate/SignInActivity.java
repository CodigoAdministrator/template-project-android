package sg.codigo.mytemplate;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sg.codigo.mytemplate.callbacks.JsonCallback;
import sg.codigo.mytemplate.callbacks.PopupCallback;
import sg.codigo.mytemplate.dialogs.DialogOK;
import sg.codigo.mytemplate.dialogs.DialogTwoButtons;
import sg.codigo.mytemplate.models.FacebookUserInfo;
import sg.codigo.mytemplate.storages.AppPreferences;
import sg.codigo.mytemplate.utils.Constants;
import sg.codigo.mytemplate.utils.HTTPPostAsyncTask;
import sg.codigo.mytemplate.utils.Utils;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.facebook.AppEventsLogger;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionDefaultAudience;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.internal.SessionTracker;
import com.facebook.model.GraphUser;
import com.nineoldandroids.animation.Animator;

import android.os.Bundle;
import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.content.Context;
import android.content.Intent;

public class SignInActivity extends Activity implements OnClickListener, PopupCallback, JsonCallback{
	// data
	private AppPreferences appPrefs;
	private PopupCallback popupCallback;
	private int animationIndex = 1;
	private ArrayList<NameValuePair> nameValuePairs;
	private JsonCallback jsonCallback;
	private DialogOK dialogOK;
	private DialogOK dialogError;
	private DialogTwoButtons dialogAccountActivation;
	private DialogTwoButtons dialogSignIn;
	private Boolean isFBAuthorized = false;
	
	// facebook
	private SessionTracker mSessionTracker;
	private Session activeSession;
	private FacebookUserInfo fbUserInfo;
	private UiLifecycleHelper uiHelper;
	
		
	// layout
	private RelativeLayout layoutBtnFB;
	private Button btnFb;
	private ImageView ivDividerOR;
	private RelativeLayout layoutEmail;
	private ImageView icEmail;
	private EditText etEmail;
	private TextView tvLabelEmail;
	private RelativeLayout layoutPassword;
	private ImageView icPassword;
	private EditText etPassword;
	private TextView tvLabelPassword;
	private Button btnForgotPassword;
	private RelativeLayout layoutBtnSignIn;
	private Button btnSignIn;
	
	// facebook
    private Session.StatusCallback fbCallback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
        	if(Constants.DEBUG_MODE){
 				Log.d(Constants.TAG, "loginViaFacebook 04");
 			}
            onSessionStateChange(session, state, exception);
        }
    };
    
    // facebook
    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
 	   if(Constants.DEBUG_MODE){
 			Log.d(Constants.TAG, "loginViaFacebook 05");
 			Log.d(Constants.TAG, "state.isOpened(): " + state.isOpened());
 			Log.d(Constants.TAG, "isFBAuthorized: " + isFBAuthorized);
 			
 	   }
 	   if(state.isOpened() && isFBAuthorized) {
 			
 		   	fetchFacebookUserData(session);
 		    isFBAuthorized = false;
 			if(Constants.DEBUG_MODE){
  				Log.d(Constants.TAG, "loginViaFacebook 06");
  			}
 		}else{
  		
  			if(Constants.DEBUG_MODE){
  				Log.d(Constants.TAG, "loginViaFacebook 07");
  			}
  			
  		}
    }
    
  
    public void loginViaFacebook() {
    	mSessionTracker = new SessionTracker(SignInActivity.this, fbCallback);
    	isFBAuthorized = true;
        String applicationId = "";
        //if(Constants.IS_PRODUCTION){
        	applicationId = getString(R.string.fb_app_id); //_prod);//Utility.getMetadataApplicationId(context);
        //}else{
        //	applicationId = getString(R.string.fb_app_id_stag);
        //}
        
        activeSession = mSessionTracker.getSession();

    	
    	if (activeSession == null || activeSession.isClosed()) {
 			//Session.openActiveSession(MainActivity.this, true, callback);
    		mSessionTracker.setSession(null);
            Session session = new Session.Builder(SignInActivity.this).setApplicationId(applicationId).build();
            
            Session.setActiveSession(session);
            activeSession = session;
    	}else{
    		
    	}

    	if (!activeSession.isOpened()) {
	        Session.OpenRequest openRequest = null;
	        openRequest = new Session.OpenRequest((Activity) SignInActivity.this);
	
	        if (openRequest != null) {
	            openRequest.setDefaultAudience(SessionDefaultAudience.FRIENDS);
	            openRequest.setPermissions(Arrays.asList(Constants.PERMISSIONS_READ));
	            openRequest.setLoginBehavior(SessionLoginBehavior.SSO_WITH_FALLBACK);
	
	            activeSession.openForRead(openRequest);
	        	
	            //fetchFacebookUserData();
	        }
 		} else {
 			//publishFeedDialog();
 			if(isFBAuthorized){
 				fetchFacebookUserData(activeSession);
 				isFBAuthorized = false;
 			}
 		}
 	}
    
    private void fetchFacebookUserData(final Session session){ //, final int actionType){
    	
    	// make request to the /me API
        Request.newMeRequest(session, new Request.GraphUserCallback() {

        	// callback after Graph API response with user object
        	@Override
        	public void onCompleted(GraphUser user, Response response) {
        		if (user != null) {
        			
        			if(session.getAccessToken()!=null){
        				if(Constants.DEBUG_MODE){
        					Log.d(Constants.TAG, "session.getAccessToken(): " + session.getAccessToken());
        				}
        				appPrefs.setFBToken(session.getAccessToken());
        			}
        			
        			if(Constants.DEBUG_MODE){
        				Log.d(Constants.TAG, "fb id: " + user.getId());
						Log.d(Constants.TAG, "fb name: " + user.getName());
						//Log.d(Constants.TAG, "fb gender: " + user.getProperty("gender").toString());
						//Log.d(Constants.TAG, "fb bday: " + user.getBirthday());
						Log.d(Constants.TAG, "fb email: " + user.getProperty("email"));
						
					}
        			if(user.getId()!=null){	
						fbUserInfo.setId(user.getId());
					}
        			
					if(user.getName()!=null){	
						fbUserInfo.setName(user.getName());
					}
					/*
					if(user.getBirthday()!=null){	
						String bday = user.getBirthday();
						fbUserInfo.setDOB(bday);
					}
					if(user.getProperty("gender")!=null){		
						fbUserInfo.setGender(user.getProperty("gender").toString());
					}
					*/
					if(user.getProperty("email")!=null){					
						fbUserInfo.setEmail(user.getProperty("email").toString());
					}
					
					appPrefs.setFBUserInfo(fbUserInfo);
					appPrefs.setFBLoginStatus(true);
					
					/*
					nameValuePairs.clear();
					nameValuePairs.add(new BasicNameValuePair("name", appPrefs.getFBUserInfo().getName()));
	    	    	nameValuePairs.add(new BasicNameValuePair("email", appPrefs.getFBUserInfo().getEmail()));
	    	    	nameValuePairs.add(new BasicNameValuePair("facebook_id", appPrefs.getFBUserInfo().getId()));
	    	    	nameValuePairs.add(new BasicNameValuePair("device_id", appPrefs.getGCMRegistrantId()));
	    	    	nameValuePairs.add(new BasicNameValuePair("device_type", Constants.DEVICE_TYPE));
	    		    
	    			new HTTPPostAsyncTask(SignInActivity.this, nameValuePairs, Constants.API_REGISTER_CUSTOMER_ACCOUNT_BY_FB, jsonCallback, Constants.ID_API_REGISTER_CUSTOMER_ACCOUNT_BY_FB, true, false);
					*/
					
        		}
        		else{
        			appPrefs.setFBLoginStatus(false);
        			appPrefs.clearFBUserInfo();
        		}
        	}

        }).executeAsync();
        
    }
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Utils.updateActionBar(SignInActivity.this, getString(R.string.title_sign_in), true, true, false, null);
    	
        setContentView(R.layout.activity_sign_in);
        
        if(appPrefs == null){
    		appPrefs = new AppPreferences(this);
    	}
    	appPrefs.setIsFirstLaunch(false);
    	
        // facebook
    	uiHelper = new UiLifecycleHelper(SignInActivity.this, fbCallback);
        uiHelper.onCreate(savedInstanceState);
        
        
        init();
    }
    
    @Override
    public void onStart() {
      super.onStart();
    }
    
    @Override
    protected void onResume(){
		super.onResume();
		// facebook
    	uiHelper.onResume();
		// Logs 'install' and 'app activate' App Events.
		AppEventsLogger.activateApp(this);
		
		Utils.updateGA(this, getString(R.string.ga_screen_sign_in));
		if(appPrefs == null){
    		appPrefs = new AppPreferences(this);
    	}
		
		if(appPrefs.getEmergencyStatus()){
        	Intent intent = new Intent(this, EmergencyActivity.class);
    		startActivity(intent);
    		this.finish();
        }
    }
    
    @Override
    public void onPause() {
        super.onPause();
     	// facebook
        uiHelper.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }
    
    @Override
    protected void onDestroy(){
    	super.onDestroy();
    	this.setResult(Constants.ACTION_FINISH_ACTIVITY);
    	// facebook
    	uiHelper.onDestroy();
    }
    
    @Override
	protected void onStop() {
    	super.onStop();
    	
	}
    
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        	finish();
        }else if (keyCode == KeyEvent.KEYCODE_MENU) {
        
        }else if (keyCode == KeyEvent.KEYCODE_HOME) {
        
        }

        return false;
        //return super.onKeyDown(keyCode, event);
    }
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		if(requestCode == Constants.SCREEN_SIGN_IN && resultCode == Constants.ACTION_FINISH_ACTIVITY) 
	     {
	    	 this.setResult(Constants.ACTION_FINISH_ACTIVITY);
	    	 this.finish();
	     }else{
	    	 // facebook
			 uiHelper.onActivityResult(requestCode, resultCode, data);
			
			 if(Session.getActiveSession()!=null){
				 Session.getActiveSession().onActivityResult(SignInActivity.this, requestCode, resultCode, data);
			 }
	    	 super.onActivityResult(requestCode, resultCode, data);
	     }
	     
	}
	
	
    @Override
	public void onLowMemory(){
    	System.gc();
	}
	
    @Override
	public void onSaveInstanceState (Bundle outState) {
    	super.onSaveInstanceState(outState);
    	
    	// facebook
	    uiHelper.onSaveInstanceState(outState);
    }
   
    @Override
    protected void onRestoreInstanceState (Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
        	this.finish();
        }
        return true;
    }
    
    private void init(){
    	
    	//hide keybaord
    	getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    	
    	appPrefs = new AppPreferences(SignInActivity.this);
    	popupCallback = this;
    	jsonCallback = this;
        nameValuePairs = new ArrayList<NameValuePair>();
        
        // facebook
    	fbUserInfo = new FacebookUserInfo();
    	
    	layoutBtnFB = (RelativeLayout)findViewById(R.id.layout_btn_fb);
    	btnFb = (Button)findViewById(R.id.btn_fb);
    	btnFb.setTypeface(Utils.getFontAvenirBook(SignInActivity.this));
    	btnFb.setOnClickListener(this);
    	ivDividerOR = (ImageView)findViewById(R.id.iv_divider_or);
    	layoutEmail = (RelativeLayout)findViewById(R.id.layout_email);
    	icEmail = (ImageView)findViewById(R.id.ic_email);
    	etEmail = (EditText)findViewById(R.id.et_email);
    	etEmail.setTypeface(Utils.getFontAvenirLight(SignInActivity.this));
    	tvLabelEmail = (TextView)findViewById(R.id.tv_label_email);
    	tvLabelEmail.setTypeface(Utils.getFontAvenirBlack(SignInActivity.this));
    	layoutPassword = (RelativeLayout)findViewById(R.id.layout_password);
    	icPassword = (ImageView)findViewById(R.id.ic_password);
    	etPassword = (EditText)findViewById(R.id.et_password);
    	etPassword.setTypeface(Utils.getFontAvenirLight(SignInActivity.this));
    	tvLabelPassword = (TextView)findViewById(R.id.tv_label_password);
    	tvLabelPassword.setTypeface(Utils.getFontAvenirBlack(SignInActivity.this));
    	
    	btnForgotPassword = (Button)findViewById(R.id.btn_forgot_password);
    	btnForgotPassword.setTypeface(Utils.getFontAvenirMedium(SignInActivity.this));
    	btnForgotPassword.setOnClickListener(this);
    	layoutBtnSignIn = (RelativeLayout)findViewById(R.id.layout_btn_sign_in);
    	btnSignIn = (Button)findViewById(R.id.btn_sign_in);
    	btnSignIn.setTypeface(Utils.getFontAvenirBook(SignInActivity.this));
    	btnSignIn.setOnClickListener(this);
    	
    	TextWatcher emailTextWatcher = new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if(tvLabelEmail.getVisibility()==View.GONE && s.length() > 0){
					YoYo.with(Techniques.FadeInDown)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	tvLabelEmail.setVisibility(View.VISIBLE);
                        	icEmail.setBackgroundResource(R.drawable.ic_email);
                        	etEmail.setHint(getString(R.string.hint_email));
                			etEmail.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelEmail);
					
					
					
				}else if(tvLabelEmail.getVisibility()==View.VISIBLE && s.length() <= 0){
					YoYo.with(Techniques.FadeOutUp)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	icEmail.setBackgroundResource(R.drawable.ic_email);
                        	etEmail.setHint(getString(R.string.hint_email));
                			etEmail.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	tvLabelEmail.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelEmail);
					
					
		    		
				}
			}
    		
    	};
    	
    	etEmail.addTextChangedListener(emailTextWatcher);
    	
    	TextWatcher passwordTextWatcher = new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if(tvLabelPassword.getVisibility()==View.GONE && s.length() > 0){
					YoYo.with(Techniques.FadeInDown)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	tvLabelPassword.setVisibility(View.VISIBLE);
                        	icPassword.setBackgroundResource(R.drawable.ic_password);
                        	etPassword.setHint(getString(R.string.hint_password));
                        	etPassword.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelPassword);
					
					
					
				}else if(tvLabelPassword.getVisibility()==View.VISIBLE && s.length() <= 0){
					YoYo.with(Techniques.FadeOutUp)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	icPassword.setBackgroundResource(R.drawable.ic_password);
                        	etPassword.setHint(getString(R.string.hint_password));
                        	etPassword.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	tvLabelPassword.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelPassword);
				}
			}
    		
    	};
    	
    	etPassword.addTextChangedListener(passwordTextWatcher);
    	
    	etEmail.setText(appPrefs.getLastLoggedInEmail());
    	
    	/*
    	YoYo.with(Techniques.FadeIn)
    		.duration(Constants.animationTime * animationIndex++)
    		.playOn(layoutBtnFB);
    	
    	YoYo.with(Techniques.FadeIn)
    		.duration(Constants.animationTime * animationIndex++)
    		.playOn(ivDividerOR);
    	
    	YoYo.with(Techniques.FadeIn)
			.duration(Constants.animationTime * animationIndex++)
			.playOn(layoutEmail);
    	
    	YoYo.with(Techniques.FadeIn)
			.duration(Constants.animationTime * animationIndex++)
			.playOn(layoutPassword);

    	YoYo.with(Techniques.FadeIn)
			.duration(Constants.animationTime * animationIndex++)
			.playOn(btnForgotPassword);

    	YoYo.with(Techniques.FadeIn)
			.duration(Constants.animationTime * animationIndex++)
			.playOn(layoutBtnSignIn);
		*/
    	
		/*
    	if(Constants.IS_GA_ON){
			
    		if(MainActivity.trackerCDG!=null){
				// Set screen name.
				MainActivity.trackerCDG.setScreenName(getString(R.string.ga_screen_terms));
	
		        // Send a screen view.
				MainActivity.trackerCDG.send(new HitBuilders.AppViewBuilder().build());
    		}
    		
    		if(MainActivity.trackerSP!=null){
				// Set screen name.
				MainActivity.trackerSP.setScreenName(getString(R.string.ga_screen_terms));
	
		        // Send a screen view.
				MainActivity.trackerSP.send(new HitBuilders.AppViewBuilder().build());
    		}
    	}
    	*/
    }
    
	
    public void onClick(View v) {
    	Utils.hideSoftKeyboard(SignInActivity.this);
    	if(v == btnFb){
    		etEmail.setText("");
    		etPassword.setText("");
    		loginViaFacebook();
    	}
    	else if(v == btnForgotPassword){
    		boolean hasError = false;
    		View viewHighlighted = null;
    		
    		if(etEmail.getText().toString().trim().equalsIgnoreCase("")){
    			hasError = true;
    			etEmail.setText("");
    			etEmail.setHint(getString(R.string.error_message_email_01));
    			etEmail.setHintTextColor(getResources().getColor(R.color.error_red));
    			icEmail.setBackgroundResource(R.drawable.ic_email_error);
    			YoYo.with(Techniques.Shake)
    				.duration(Constants.animationTime)
    				.playOn(icEmail);
    			
    			if(viewHighlighted == null){
    				viewHighlighted = etEmail;
    			}
    			
    			//DialogOK dialogOK = new DialogOK(SignInActivity.this, getString(R.string.dialog_title_alert), getString(R.string.error_message_email_01), getString(R.string.btn_ok), Constants.POPUP_INVALID_EMAIL, popupCallback);
    			//dialogOK.show();
    		}
    		
    		else if(!Utils.emailValidation(etEmail.getText().toString().trim())){
    			hasError = true;
    			etEmail.setText("");
    			etEmail.setHint(getString(R.string.error_message_email_02));
    			etEmail.setHintTextColor(getResources().getColor(R.color.error_red));
    			icEmail.setBackgroundResource(R.drawable.ic_email_error);
    			YoYo.with(Techniques.Shake)
    				.duration(Constants.animationTime)
    				.playOn(icEmail);
    			
    			if(viewHighlighted == null){
    				viewHighlighted = etEmail;
    			}
    			//DialogOK dialogOK = new DialogOK(SignInActivity.this, getString(R.string.dialog_title_alert), getString(R.string.error_message_email_02), getString(R.string.btn_ok), Constants.POPUP_INVALID_EMAIL, popupCallback);
    			//dialogOK.show();
    			
    		}
    		
    	
	    	if(!hasError){
				/*
	    		nameValuePairs.clear();
				nameValuePairs.add(new BasicNameValuePair("email", etEmail.getText().toString().trim()));
				new HTTPPostAsyncTask(SignInActivity.this, nameValuePairs, Constants.API_RECOVER_PASSWORD, jsonCallback, Constants.ID_API_RECOVER_PASSWORD, true, false);
				*/
	    	}else{
				if(viewHighlighted != null){
					viewHighlighted.requestFocus();
				}
			}
		    	
    	}
    	else if(v == btnSignIn){
    		boolean hasError = false;
    		View viewHighlighted = null;
    		
    		if(etEmail.getText().toString().trim().equalsIgnoreCase("")){
    			hasError = true;
    			etEmail.setText("");
    			etEmail.setHint(getString(R.string.error_message_email_01));
    			etEmail.setHintTextColor(getResources().getColor(R.color.error_red));
    			icEmail.setBackgroundResource(R.drawable.ic_email_error);
    			YoYo.with(Techniques.Shake)
    				.duration(Constants.animationTime)
    				.playOn(icEmail);
    			
    			if(viewHighlighted == null){
    				viewHighlighted = etEmail;
    			}
    			
    			//DialogOK dialogOK = new DialogOK(SignInActivity.this, getString(R.string.dialog_title_alert), getString(R.string.error_message_email_01), getString(R.string.btn_ok), Constants.POPUP_INVALID_EMAIL, popupCallback);
    			//dialogOK.show();
    		}
    		
    		else if(!Utils.emailValidation(etEmail.getText().toString().trim())){
    			hasError = true;
    			etEmail.setText("");
    			etEmail.setHint(getString(R.string.error_message_email_02));
    			etEmail.setHintTextColor(getResources().getColor(R.color.error_red));
    			icEmail.setBackgroundResource(R.drawable.ic_email_error);
    			YoYo.with(Techniques.Shake)
    				.duration(Constants.animationTime)
    				.playOn(icEmail);
    			
    			if(viewHighlighted == null){
    				viewHighlighted = etEmail;
    			}
    			//DialogOK dialogOK = new DialogOK(SignInActivity.this, getString(R.string.dialog_title_alert), getString(R.string.error_message_email_02), getString(R.string.btn_ok), Constants.POPUP_INVALID_EMAIL, popupCallback);
    			//dialogOK.show();
    			
    		}
    		
    		if(etPassword.getText().toString().trim().equalsIgnoreCase("")){
    			hasError = true;
    			etPassword.setText("");
    			etPassword.setHint(getString(R.string.error_message_password_01));
    			etPassword.setHintTextColor(getResources().getColor(R.color.error_red));
    			icPassword.setBackgroundResource(R.drawable.ic_password_error);
    			YoYo.with(Techniques.Shake)
    				.duration(Constants.animationTime)
    				.playOn(icPassword);
    			
    			if(viewHighlighted == null){
    				viewHighlighted = etPassword;
    			}
    			//DialogOK dialogOK = new DialogOK(SignInActivity.this, getString(R.string.dialog_title_alert), getString(R.string.error_message_password_01), getString(R.string.btn_ok), Constants.POPUP_INVALID_PASSWORD, popupCallback);
    			//dialogOK.show();
    		}
    		else if(etPassword.getText().toString().trim().length() < 6){
    			hasError = true;
    			etPassword.setText("");
    			etPassword.setHint(getString(R.string.error_message_password_02));
    			etPassword.setHintTextColor(getResources().getColor(R.color.error_red));
    			icPassword.setBackgroundResource(R.drawable.ic_password_error);
    			YoYo.with(Techniques.Shake)
    				.duration(Constants.animationTime)
    				.playOn(icPassword);
    			
    			if(viewHighlighted == null){
    				viewHighlighted = etPassword;
    			}
    			//DialogOK dialogOK = new DialogOK(SignInActivity.this, getString(R.string.dialog_title_alert), getString(R.string.error_message_password_02), getString(R.string.btn_ok), Constants.POPUP_INVALID_PASSWORD, popupCallback);
    			//dialogOK.show();
    		}
    		
    		if(!hasError){

				/*
    			nameValuePairs.clear();
    			nameValuePairs.add(new BasicNameValuePair("email", etEmail.getText().toString().trim()));
    	    	nameValuePairs.add(new BasicNameValuePair("facebook_token", ""));
    	    	nameValuePairs.add(new BasicNameValuePair("password", etPassword.getText().toString().trim()));
    	    	nameValuePairs.add(new BasicNameValuePair("device_id", appPrefs.getGCMRegistrantId()));
    	    	nameValuePairs.add(new BasicNameValuePair("device_type", Constants.DEVICE_TYPE));
    		    
    		    
    			new HTTPPostAsyncTask(SignInActivity.this, nameValuePairs, Constants.API_LOGIN, jsonCallback, Constants.ID_API_LOGIN, true, false);
    	    	*/

    		}else{
    			if(viewHighlighted != null){
    				viewHighlighted.requestFocus();
    			}
    		}
    	}
    }

	@Override
	public void callBackPopup(Object data, int processID, int index) {
		if(processID == Constants.POPUP_API_CALL_SUCCEED){
			
		}
		
		else if(processID == Constants.POPUP_API_CALL_FAILED){
			
		}
		/*
		else if(processID == Constants.POPUP_API_CALL_FAILED_ACCOUNT_EXISTS_ACTIVATE_ACCOUNT){
			if(index == Constants.DIALOG_BUTTON_RIGHT){
				Customer customer = new Customer();
	    		customer.setEmail(etEmail.getText().toString().trim());
				
				appPrefs.setCustomerInfo(customer);
				
	    		Intent intent = new Intent(this, AccountActivationActivity.class);
				startActivityForResult(intent, Constants.SCREEN_SIGN_IN);
			}
		}
		*/
	}

	@Override
	public void callbackJson(String result, int processID, int index) {
		/*
		if(processID == Constants.ID_API_LOGIN){
			if(result!=null && !result.equalsIgnoreCase("")){
				try{
					JSONObject jsonObj = new JSONObject(result);
					
					int errorCode = jsonObj.getInt("errorCode");
					String message = jsonObj.getString("message");
					
					if(errorCode == Constants.ERROR_CODE_SUCCESS){
						if(jsonObj.has("customer_info") && jsonObj.getString("customer_info").startsWith("[")){
					
							JSONArray customerArray = jsonObj.getJSONArray("customer_info");
							for(int i = 0; i < customerArray.length(); i++){
								JSONObject customerObj = customerArray.getJSONObject(i);
								Customer customer = new Customer();
								customer.setToken((customerObj.has("customer_token") && customerObj.getString("customer_token")!=null)?customerObj.getString("customer_token"):"");
								customer.setName((customerObj.has("customer_name") && customerObj.getString("customer_name")!=null)?customerObj.getString("customer_name"):"");
								customer.setCode((customerObj.has("customer_code") && customerObj.getString("customer_code")!=null)?customerObj.getString("customer_code"):"");
								customer.setMobile((customerObj.has("customer_mobile") && customerObj.getString("customer_mobile")!=null)?customerObj.getString("customer_mobile"):"");
								customer.setEmail((customerObj.has("customer_email") && customerObj.getString("customer_email")!=null)?customerObj.getString("customer_email"):"");
								customer.setCompanyName((customerObj.has("company_name") && customerObj.getString("company_name")!=null)?customerObj.getString("company_name"):"");
								customer.setRegDate((customerObj.has("reg_date") && customerObj.getString("reg_date")!=null)?customerObj.getString("reg_date"):"");
								customer.setPhotoUrl((customerObj.has("customer_photo") && customerObj.getString("customer_photo")!=null)?customerObj.getString("customer_photo"):"");
								customer.setMemberType((customerObj.has("member_type") && customerObj.getString("member_type")!=null)?customerObj.getString("member_type"):"");
								customer.setPaymentType((customerObj.has("payment_type") && customerObj.getString("payment_type")!=null)?customerObj.getString("payment_type"):"");
								
								appPrefs.setCustomerInfo(customer);
							}
							
							appPrefs.setIsLoggedIn(true);
							appPrefs.setLastLoggedInEmail(appPrefs.getCustomerInfo().getEmail());
							appPrefs.setIsLoginThruFB(false);
							Intent intent = new Intent(this, MainActivity.class);
				    		startActivity(intent);
				    		this.setResult(Constants.ACTION_FINISH_ACTIVITY);
				    		this.finish();
						}
						else if(jsonObj.has("customer_info") && jsonObj.getString("customer_info").startsWith("{")){
					
							JSONObject customerObj = jsonObj.getJSONObject("customer_info");
							Customer customer = new Customer();
							customer.setToken((customerObj.has("customer_token") && customerObj.getString("customer_token")!=null)?customerObj.getString("customer_token"):"");
							customer.setName((customerObj.has("customer_name") && customerObj.getString("customer_name")!=null)?customerObj.getString("customer_name"):"");
							customer.setCode((customerObj.has("customer_code") && customerObj.getString("customer_code")!=null)?customerObj.getString("customer_code"):"");
							customer.setMobile((customerObj.has("customer_mobile") && customerObj.getString("customer_mobile")!=null)?customerObj.getString("customer_mobile"):"");
							customer.setEmail((customerObj.has("customer_email") && customerObj.getString("customer_email")!=null)?customerObj.getString("customer_email"):"");
							customer.setCompanyName((customerObj.has("company_name") && customerObj.getString("company_name")!=null)?customerObj.getString("company_name"):"");
							customer.setRegDate((customerObj.has("reg_date") && customerObj.getString("reg_date")!=null)?customerObj.getString("reg_date"):"");
							customer.setPhotoUrl((customerObj.has("customer_photo") && customerObj.getString("customer_photo")!=null)?customerObj.getString("customer_photo"):"");
							customer.setMemberType((customerObj.has("member_type") && customerObj.getString("member_type")!=null)?customerObj.getString("member_type"):"");
							customer.setPaymentType((customerObj.has("payment_type") && customerObj.getString("payment_type")!=null)?customerObj.getString("payment_type"):"");
							
							appPrefs.setCustomerInfo(customer);
							
							appPrefs.setIsLoggedIn(true);
							appPrefs.setLastLoggedInEmail(appPrefs.getCustomerInfo().getEmail());
							appPrefs.setIsLoginThruFB(false);
							Intent intent = new Intent(this, MainActivity.class);
				    		startActivity(intent);
				    		this.setResult(Constants.ACTION_FINISH_ACTIVITY);
				    		this.finish();
						}else{
							if(dialogError==null || !dialogError.isShowing()){
								dialogError = new DialogOK(SignInActivity.this, getString(R.string.dialog_title_error), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_FAILED, popupCallback);
								dialogError.show();
							}
						}
					}
					else if(errorCode == Constants.ERROR_CODE_ACCOUNT_NOT_ACTIVATE){
						if(dialogAccountActivation==null || !dialogAccountActivation.isShowing()){
							dialogAccountActivation = new DialogTwoButtons(SignInActivity.this, getString(R.string.dialog_title_error), message + getString(R.string.alert_message_account_exists_please_activate), getString(R.string.btn_cancel), getString(R.string.btn_activate_now), Constants.POPUP_API_CALL_FAILED_ACCOUNT_EXISTS_ACTIVATE_ACCOUNT, popupCallback);
							dialogAccountActivation.show();
						}
					}else{
						if(dialogError==null || !dialogError.isShowing()){
							dialogError = new DialogOK(SignInActivity.this, getString(R.string.dialog_title_error), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_FAILED, popupCallback);
							dialogError.show();
						}
					}
				}catch(JSONException e){
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "JSONException: " + e.toString());
					}
				}catch(Exception e){
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "Exception: " + e.toString());
					}
				}
			}
			
		}
		else if(processID == Constants.ID_API_REGISTER_CUSTOMER_ACCOUNT_BY_FB){
			
			if(result!=null && !result.equalsIgnoreCase("")){
				try{
					JSONObject jsonObj = new JSONObject(result);
					
					int errorCode = jsonObj.getInt("errorCode");
					String message = jsonObj.getString("message");
					
					if(errorCode == Constants.ERROR_CODE_SUCCESS){
						if(jsonObj.has("customer_info") && jsonObj.getString("customer_info").startsWith("[")){
							JSONArray customerArray = jsonObj.getJSONArray("customer_info");
							for(int i = 0; i < customerArray.length(); i++){
								JSONObject customerObj = customerArray.getJSONObject(i);
								Customer customer = new Customer();
								customer.setToken((customerObj.has("customer_token") && customerObj.getString("customer_token")!=null)?customerObj.getString("customer_token"):"");
								customer.setName((customerObj.has("customer_name") && customerObj.getString("customer_name")!=null)?customerObj.getString("customer_name"):"");
								customer.setCode((customerObj.has("customer_code") && customerObj.getString("customer_code")!=null)?customerObj.getString("customer_code"):"");
								customer.setMobile((customerObj.has("customer_mobile") && customerObj.getString("customer_mobile")!=null)?customerObj.getString("customer_mobile"):"");
								customer.setEmail((customerObj.has("customer_email") && customerObj.getString("customer_email")!=null)?customerObj.getString("customer_email"):"");
								customer.setCompanyName((customerObj.has("company_name") && customerObj.getString("company_name")!=null)?customerObj.getString("company_name"):"");
								customer.setRegDate((customerObj.has("reg_date") && customerObj.getString("reg_date")!=null)?customerObj.getString("reg_date"):"");
								customer.setPhotoUrl((customerObj.has("customer_photo") && customerObj.getString("customer_photo")!=null)?customerObj.getString("customer_photo"):"");
								customer.setMemberType((customerObj.has("member_type") && customerObj.getString("member_type")!=null)?customerObj.getString("member_type"):"");
								customer.setPaymentType((customerObj.has("payment_type") && customerObj.getString("payment_type")!=null)?customerObj.getString("payment_type"):"");
								
								appPrefs.setCustomerInfo(customer);
							}
							
							appPrefs.setIsLoggedIn(true);
							appPrefs.setIsLoginThruFB(true);
							Intent intent = new Intent(this, MainActivity.class);
				    		startActivity(intent);
				    		this.setResult(Constants.ACTION_FINISH_ACTIVITY);
				    		this.finish();
						}else if(jsonObj.has("customer_info") && jsonObj.getString("customer_info").startsWith("{")){
							
							JSONObject customerObj = jsonObj.getJSONObject("customer_info");
							Customer customer = new Customer();
							customer.setToken((customerObj.has("customer_token") && customerObj.getString("customer_token")!=null)?customerObj.getString("customer_token"):"");
							customer.setName((customerObj.has("customer_name") && customerObj.getString("customer_name")!=null)?customerObj.getString("customer_name"):"");
							customer.setCode((customerObj.has("customer_code") && customerObj.getString("customer_code")!=null)?customerObj.getString("customer_code"):"");
							customer.setMobile((customerObj.has("customer_mobile") && customerObj.getString("customer_mobile")!=null)?customerObj.getString("customer_mobile"):"");
							customer.setEmail((customerObj.has("customer_email") && customerObj.getString("customer_email")!=null)?customerObj.getString("customer_email"):"");
							customer.setCompanyName((customerObj.has("company_name") && customerObj.getString("company_name")!=null)?customerObj.getString("company_name"):"");
							customer.setRegDate((customerObj.has("reg_date") && customerObj.getString("reg_date")!=null)?customerObj.getString("reg_date"):"");
							customer.setPhotoUrl((customerObj.has("customer_photo") && customerObj.getString("customer_photo")!=null)?customerObj.getString("customer_photo"):"");
							customer.setMemberType((customerObj.has("member_type") && customerObj.getString("member_type")!=null)?customerObj.getString("member_type"):"");
							customer.setPaymentType((customerObj.has("payment_type") && customerObj.getString("payment_type")!=null)?customerObj.getString("payment_type"):"");
							
							appPrefs.setCustomerInfo(customer);
							
							appPrefs.setIsLoggedIn(true);
							appPrefs.setIsLoginThruFB(true);
							Intent intent = new Intent(this, MainActivity.class);
				    		startActivity(intent);
				    		this.setResult(Constants.ACTION_FINISH_ACTIVITY);
				    		this.finish();
						}else{
							if(dialogError==null || !dialogError.isShowing()){
								dialogError = new DialogOK(SignInActivity.this, getString(R.string.dialog_title_error), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_FAILED, popupCallback);
								dialogError.show();
							}
						}
					}else{
						if(dialogError==null || !dialogError.isShowing()){
							dialogError = new DialogOK(SignInActivity.this, getString(R.string.dialog_title_error), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_FAILED, popupCallback);
							dialogError.show();
						}
					}
				}catch(JSONException e){
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "JSONException: " + e.toString());
					}
				}catch(Exception e){
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "Exception: " + e.toString());
					}
				}
			}
		}
		else if(processID == Constants.ID_API_RECOVER_PASSWORD){
			if(result!=null && !result.equalsIgnoreCase("")){
				try{
					JSONObject jsonObj = new JSONObject(result);
					
					int errorCode = jsonObj.getInt("errorCode");
					String message = jsonObj.getString("message");
					
					if(errorCode == Constants.ERROR_CODE_SUCCESS){
						if(dialogOK==null || !dialogOK.isShowing()){
							dialogOK = new DialogOK(SignInActivity.this, getString(R.string.dialog_title_alert), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_SUCCEED, popupCallback);
							dialogOK.show();
						}
					}else{
						if(dialogError==null || !dialogError.isShowing()){
							dialogError = new DialogOK(SignInActivity.this, getString(R.string.dialog_title_error), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_FAILED, popupCallback);
							dialogError.show();
						}
					}
				}catch(JSONException e){
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "JSONException: " + e.toString());
					}
				}catch(Exception e){
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "Exception: " + e.toString());
					}
				}
			}
		}
		*/
	}

	@Override
	public void jsonError(String msg, int processID) {
		//if(processID == Constants.ID_API_LOGIN || processID == Constants.ID_API_RECOVER_PASSWORD){
			if(Constants.DEBUG_MODE){
				Log.i(Constants.TAG, "jsonError: " + msg);
			}
			if(dialogError==null || !dialogError.isShowing()){
				dialogError = new DialogOK(SignInActivity.this, getString(R.string.dialog_title_error), msg, getString(R.string.btn_ok), Constants.POPUP_API_CALL_FAILED, popupCallback);
				dialogError.show();
			}
		//}
	}
}

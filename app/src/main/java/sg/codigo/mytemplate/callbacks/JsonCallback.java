package sg.codigo.mytemplate.callbacks;

public interface JsonCallback {

	public void callbackJson(String result, int processID, int index);

	public void jsonError(String msg, int processID);

}

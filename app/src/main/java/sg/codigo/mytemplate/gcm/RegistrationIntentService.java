/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sg.codigo.mytemplate.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import sg.codigo.mytemplate.callbacks.JsonCallback;
import sg.codigo.mytemplate.storages.AppPreferences;
import sg.codigo.mytemplate.utils.Constants;
import sg.codigo.mytemplate.utils.HTTPPostAsyncTask;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class RegistrationIntentService extends IntentService implements JsonCallback{

    private static final String TAG = Constants.TAG; // + " RegIntentService";
    private static final String[] TOPICS = {"global"};

    private AppPreferences appPrefs;
    private JsonCallback jsonCallback;
    private ArrayList<NameValuePair> nameValuePairs;

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        //SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        appPrefs = new AppPreferences(getApplicationContext());
        jsonCallback = this;
        nameValuePairs = new ArrayList<NameValuePair>();

        try {
            // [START register_for_gcm]
            // Initially this call goes out to the network to retrieve the token, subsequent calls
            // are local.
            // [START get_token]
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(Constants.getGCMSenderID(), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            // [END get_token]
            if(Constants.DEBUG_MODE) {
                Log.i(TAG, "GCM Registration Token: " + token);
            }

            // Subscribe to topic channels
            subscribeTopics(token);

            // You should store a boolean that indicates whether the generated token has been
            // sent to your server. If the boolean is false, send the token to your server,
            // otherwise your server should have already received the token.
            //sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, true).apply();
            //appPrefs.setIsTokenSentToServer(true);
            appPrefs.setGCMRegistrantId(token);

            // TODO: Implement this method to send any registration to your app's servers.
            //if(!appPrefs.getIsTokenSentToServer()) {
                sendRegistrationToServer(token);
            //}

            // [END register_for_gcm]
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);
            // If an exception happens while fetching the new token or updating our registration data
            // on a third-party server, this ensures that we'll attempt the update at a later time.
            //sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false).apply();
            appPrefs.setIsTokenSentToServer(false);

        }
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(QuickstartPreferences.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    /**
     * Persist registration to third-party servers.
     *
     * Modify this method to associate the user's GCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
        nameValuePairs.clear();
        nameValuePairs.add(new BasicNameValuePair("device_id", appPrefs.getGCMRegistrantId()));
        nameValuePairs.add(new BasicNameValuePair("device_type", Constants.DEVICE_TYPE));

        //new HTTPPostAsyncTask(getApplicationContext(), nameValuePairs, Constants.API_REGISTER_DEVICE, jsonCallback, Constants.ID_API_REGISTER_DEVICE, false, false);

    }

    /**
     * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
     *
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */
    // [START subscribe_topics]
    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }

    @Override
    public void callbackJson(String result, int processID, int index) {
        if(processID == Constants.ID_API_REGISTER_DEVICE){

            if(result!=null && !result.equalsIgnoreCase("")) {
                try {
                    JSONObject jsonObj = new JSONObject(result);

                    int errorCode = jsonObj.getInt("errorCode");
                    String message = jsonObj.getString("message");

                    if (errorCode == Constants.ERROR_CODE_SUCCESS) {
                        appPrefs.setIsTokenSentToServer(true);
                    }else{
                        appPrefs.setIsTokenSentToServer(false);
                        if(Constants.DEBUG_MODE){
                            //Log.e(TAG, Constants.API_REGISTER_DEVICE + " error: " + message);

                        }
                    }
                }catch(JSONException e){
                    if(Constants.DEBUG_MODE){
                        Log.e(TAG, "JSONException: " + e.toString());
                    }
                }catch(Exception e){
                    if(Constants.DEBUG_MODE){
                        Log.e(TAG, "Exception: " + e.toString());
                    }
                }
            }
        }

    }

    @Override
    public void jsonError(String msg, int processID) {
        if(Constants.DEBUG_MODE){
            Log.i(TAG, "jsonError: " + msg);
        }

    }
    // [END subscribe_topics]

}

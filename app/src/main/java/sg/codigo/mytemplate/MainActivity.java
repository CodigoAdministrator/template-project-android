/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sg.codigo.mytemplate;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sg.codigo.mytemplate.adapters.SideMenuArrayAdapter;
import sg.codigo.mytemplate.callbacks.JsonCallback;
import sg.codigo.mytemplate.callbacks.PopupCallback;
import sg.codigo.mytemplate.dialogs.DialogOK;
import sg.codigo.mytemplate.dialogs.DialogTwoButtons;
import sg.codigo.mytemplate.fragments.AboutFragment;
import sg.codigo.mytemplate.fragments.FeedbackFragment;
import sg.codigo.mytemplate.fragments.FullMapFragment;
import sg.codigo.mytemplate.fragments.MyProfileFragment;

import sg.codigo.mytemplate.maps.MyLocation;
import sg.codigo.mytemplate.maps.MyLocation.LocationResult;
import sg.codigo.mytemplate.models.CustomNotification;
import sg.codigo.mytemplate.models.SideMenuItem;
import sg.codigo.mytemplate.storages.AppPreferences;
import sg.codigo.mytemplate.utils.Constants;
import sg.codigo.mytemplate.utils.HTTPGetAsyncTask;
import sg.codigo.mytemplate.utils.HTTPPostAsyncTask;
import sg.codigo.mytemplate.utils.Utils;
import com.facebook.Session;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This example illustrates a common usage of the DrawerLayout widget
 * in the Android support library.
 * <p/>
 * <p>When a navigation (left) drawer is present, the host activity should detect presses of
 * the action bar's Up affordance as a signal to open and close the navigation drawer. The
 * ActionBarDrawerToggle facilitates this behavior.
 * Items within the drawer should fall into one of two categories:</p>
 * <p/>
 * <ul>
 * <li><strong>View switches</strong>. A view switch follows the same basic policies as
 * list or tab navigation in that a view switch does not create navigation history.
 * This pattern should only be used at the root activity of a task, leaving some form
 * of Up navigation active for activities further down the navigation hierarchy.</li>
 * <li><strong>Selective Up</strong>. The drawer allows the user to choose an alternate
 * parent for Up navigation. This allows a user to jump across an app's navigation
 * hierarchy at will. The application should treat this as it treats Up navigation from
 * a different task, replacing the current task stack using TaskStackBuilder or similar.
 * This is the only form of navigation drawer that should be used outside of the root
 * activity of a task.</li>
 * </ul>
 * <p/>
 * <p>Right side drawers should be used for actions, not navigation. This follows the pattern
 * established by the Action Bar that navigation should be to the left and actions to the right.
 * An action should be an operation performed on the current contents of the window,
 * for example enabling or disabling a data overlay on top of the current content.</p>
 */
public class MainActivity extends Activity implements OnClickListener, PopupCallback, JsonCallback{
	// data
	public static FragmentManager fm;
	public static Context context;
	private static AppPreferences appPrefs;
	private static Animation fadeIn;
	private static Animation fadeOut;
	//private static Animation spinClockWise;
	//private static Animation spinAntiClockWise;
	public static TranslateAnimation slideUpReservationAlert;
	public static TranslateAnimation slideDownReservationAlert;
    private String mDrawerTitle;
    private String mTitle;
    //public static int currentScreenId = Constants.SCREEN_MAIN;
    //public static boolean isReturnTrip = false;
	//private boolean isSwitchFragment = false;
    //private int selectedPosition = -1;
    private PopupCallback popupCallback;
    public static Location myCurrentLocation;

	private static ArrayList<NameValuePair> nameValuePairs;
    private static JsonCallback jsonCallback;
	private DialogOK dialogOK;
	private DialogOK dialogError;
	private DialogOK dialogForceUpdate;
	private DialogTwoButtons dialogOptionalUpdate;
	private DialogTwoButtons dialogRedirectToLanding;
	private int exitNumber = 0;

	private Handler updateDeviceIdHandler;
	private ArrayList<NameValuePair> nameValuePairsUpdateDeviceId;
	
	// layout
	public static DrawerLayout mDrawerLayout;
	public static ListView mDrawerList;
	private static ActionBarDrawerToggle mDrawerToggle;
    private static RelativeLayout leftMenuHeader;
    private static TextView tvNameMenu;
    public static SimpleDraweeView ivProfileMenu;
    public static SideMenuArrayAdapter drawerAdapter;
    private static List<SideMenuItem> leftMenuItemList;
	
    public static RelativeLayout layoutReservationAlert;
    public static TextView tvReservationMessage;
	public static Button btnReserve;
	
	/*
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        	CustomNotification notification = (CustomNotification) intent.getExtras().getSerializable(EXTRA_NOTIFICATION);
        	if(notification != null){
        		if(Constants.DEBUG_MODE){
        			Log.e(Constants.TAG, notification.toString());
    	    		Log.e(Constants.TAG, "push notification clicked - 5");
    	    	}
        		handleNotification(notification);
        	}
        }
    };
	*/
    
	private void callFacebookLogout() {
        Session session = Session.getActiveSession();
        if (session != null) {
            if (!session.isClosed()) {
                session.closeAndClearTokenInformation();
                //clear your preferences if saved
            }
        } else {

            session = new Session(context);
            Session.setActiveSession(session);

            session.closeAndClearTokenInformation();
            //clear your preferences if saved
        }
        
        appPrefs.setLastSignInUser(appPrefs.getFBUserInfo());
		appPrefs.setFBLoginStatus(false);
		appPrefs.setFBToken("");
		appPrefs.clearFBUserInfo();
		
        
        if(Constants.DEBUG_MODE){
        	Log.d(Constants.TAG, "appPrefs.getFBLoginStatus(): " + appPrefs.getFBLoginStatus());
        }
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY); //.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //getActionBar().hide();
        Fresco.initialize(MainActivity.this);
		
        Utils.updateActionBar(MainActivity.this, "", true, false, true, null);
    	
        setContentView(R.layout.activity_main);
        
        if(appPrefs == null){
    		appPrefs = new AppPreferences(this);
    	}
    	appPrefs.setIsFirstLaunch(false);
    	
        //registerReceiver(mHandleMessageReceiver, new IntentFilter(DISPLAY_NOTIFICATION_ACTION));
		
        init(savedInstanceState);
        
        Bundle extras = getIntent().getExtras();
		if(extras!=null){
			CustomNotification customNotification = (CustomNotification) extras.getSerializable("customNotification");
			if(Constants.DEBUG_MODE){
	    		Log.e(Constants.TAG, "push notification clicked - 3" );
	    	}
			if(customNotification!=null){
				handleNotification(customNotification);
			}
		}
		
    }
    
    private void handleNotification(CustomNotification notification){
    	if(Constants.DEBUG_MODE){
			Log.d(Constants.TAG, notification.toString());
		}

    }
    
    public static void checkNumberOfActiveBooking(Context context){
    	nameValuePairs.clear();
		//nameValuePairs.add(new BasicNameValuePair("customer_token", appPrefs.getCustomerInfo().getToken()));
		
		//String url = Utils.getPayloadHttpGet(Constants.API_GET_ACTIVE_BOOKING, nameValuePairs);
		//new HTTPGetAsyncTask(context, url, jsonCallback, Constants.ID_API_GET_ACTIVE_BOOKING, false, false);
    	
    }
    
    private Runnable rUpdateDeviceId = new Runnable(){

		@Override
		public void run() {
			
		nameValuePairsUpdateDeviceId.clear();
		//nameValuePairsUpdateDeviceId.add(new BasicNameValuePair("customer_token", appPrefs.getCustomerInfo().getToken()));
		//nameValuePairsUpdateDeviceId.add(new BasicNameValuePair("device_id", appPrefs.getGCMRegistrantId()));
		//nameValuePairsUpdateDeviceId.add(new BasicNameValuePair("device_type", Constants.DEVICE_TYPE));


		//new HTTPPostAsyncTask(MainActivity.this, nameValuePairsUpdateDeviceId, Constants.API_POST_UPDATE_DEVICE_ID, jsonCallback, Constants.ID_API_POST_UPDATE_DEVICE_ID, false, false);
			
		}
	};
	
    private void init(Bundle savedInstanceState){

    	context = MainActivity.this;
    	appPrefs= new AppPreferences(MainActivity.this);
    	
    	if(appPrefs.getEmergencyStatus()){
    		/*
        	Intent intent = new Intent(this, EmergencyActivity.class);
    		startActivity(intent);
    		this.finish();
    		*/
        }else{
    	
	    	popupCallback = MainActivity.this;
	    	jsonCallback = MainActivity.this;
	        nameValuePairs = new ArrayList<NameValuePair>();

	        mTitle = mDrawerTitle = "";
	        
	        checkNumberOfActiveBooking(MainActivity.this);
	        
	        /*
			PackageInfo pInfo;
			try {
				pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
				versionNumber = pInfo.versionName;
				
			} catch (NameNotFoundException e) {
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, "NameNotFoundException: " + e.toString());
				}
			} catch (Exception e) {
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, "Exception: " + e.toString());
				}
			}
			*/
			
			updateSideNav();
			setupSideNav();
			drawerAdapter.notifyDataSetChanged();
			
	        fm = getFragmentManager();
	        
	        if (savedInstanceState == null) {
	            selectItem(0);
	        }
	        
	        if(appPrefs.getIsLoggedIn()){
	        	tvNameMenu.setText("my name"); //appPrefs.getCustomerInfo().getName());
	        	ivProfileMenu.setVisibility(View.VISIBLE);
				/*
	        	if(!appPrefs.getCustomerInfo().getPhotoUrl().equalsIgnoreCase("")){

	        		Uri uri = Uri.parse(appPrefs.getCustomerInfo().getPhotoUrl());
	        		ivProfileMenu.setImageURI(uri);
	    			
	        	}else{
	        		ivProfileMenu.setVisibility(View.GONE);
	        		
	        	}
	        	*/
	        }else{
	        	tvNameMenu.setText("My Name");
	        	ivProfileMenu.setVisibility(View.VISIBLE);
	    		
	        }
	        
	        
	        LocationResult locationResult = new LocationResult(){
			    @Override
			    public void gotLocation(Location location){
			    	myCurrentLocation = location;
			    	if(myCurrentLocation!=null){
	    		    	if(Constants.DEBUG_MODE){
	    		    		Log.d(Constants.TAG, "myCurrentLocation.getLatitude(): " + myCurrentLocation.getLatitude());
	    		    		Log.d(Constants.TAG, "myCurrentLocation.getLongitude(): " + myCurrentLocation.getLongitude());
	    		    	}
	    		    	//final LatLng myLocationLatLng = new LatLng(myCurrentLocation.getLatitude(), myCurrentLocation.getLongitude());
	    		    	
			    	}
			    }
			};
			
			MyLocation myLocation = new MyLocation();
			if(myLocation.getLocation(MainActivity.this, locationResult)){
				// user location found
			}else{
				/*
				DialogOK dialogGPS = new DialogOK(context, context.getString(R.string.dialog_title_alert), context.getString(R.string.alert_message_no_gps_detected), context.getString(R.string.btn_ok), Constants.POPUP_SWITCH_ON_GPS, this.popupCallback);
	        	dialogGPS.show();
	        	*/
			}
    	}
    	
    	
    	// update device ID
    	nameValuePairsUpdateDeviceId = new ArrayList<NameValuePair>();
        updateDeviceIdHandler = new Handler();
        
    	if(appPrefs.getIsLoggedIn() && !appPrefs.getGCMRegistrantId().equalsIgnoreCase("")){
        	updateDeviceIdHandler.post(rUpdateDeviceId);
        }
    }

    public static void toggleDrawer(boolean isChecked){
    	if(isChecked){
			mDrawerLayout.openDrawer(mDrawerList);
		}else{
			mDrawerLayout.closeDrawer(mDrawerList);
		}
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        
        menu.findItem(R.id.action_settings).setVisible(Constants.isShowActionMenu);
        //menu.findItem(R.id.action_edit).setVisible(Constants.isShowActionEdit);
        menu.findItem(R.id.action_add).setVisible(Constants.isShowActionAdd);
        menu.findItem(R.id.action_logout).setVisible(Constants.isShowActionLogout);
        
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
         // The action bar home/up action should open or close the drawer.
         // ActionBarDrawerToggle will take care of this.
        /*
    	if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        */
    	
        // Handle action buttons
        switch(item.getItemId()) {
        case android.R.id.home:
        	if(fm!=null && fm.getBackStackEntryCount() > 0 && Utils.isShowBackBtn){
        		fm.popBackStack();
        		return true;
        	}else{
        		if (mDrawerToggle.onOptionsItemSelected(item)) {
                    return true;
                }
        	}
        	
        	
        case R.id.action_settings:
        	/*
            // create intent to perform web search for this planet
            Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
            intent.putExtra(SearchManager.QUERY, getActionBar().getTitle());
            // catch event that there's no activity to handle intent
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            } else {
                Toast.makeText(this, R.string.app_not_available, Toast.LENGTH_LONG).show();
            }
            */
            return true;
        /*    
        case R.id.action_edit:
        	Toast.makeText(this, R.string.action_edit, Toast.LENGTH_LONG).show();
        	if(currentScreenId == Constants.SCREEN_FAVOURITES_FROM_SIDE_NAV){
        		
        	}else if(currentScreenId == Constants.SCREEN_FAVOURITES_FROM_CHOOSE_ADDRESS){
        		
        	}
        	
        	return true;
        	*/
        case R.id.action_add:
        	//Toast.makeText(this, R.string.action_add, Toast.LENGTH_LONG).show();
			/*
        	if(currentScreenId == Constants.SCREEN_FAVOURITES_FROM_SIDE_NAV){
        		//FavouritesFragment.addAddress();
        		Bundle bundle = new Bundle();
        		bundle.putString("title", getString(R.string.title_add_favourite));
        		bundle.putInt("address_for", Constants.SCREEN_FAVOURITES_FROM_SIDE_NAV);
        		MainActivity.switchFragment(getString(R.string.fragment_choose_address), bundle, true);
        	}
        	*/
        	return true;
        
        case R.id.action_logout:
        	//Toast.makeText(this, R.string.action_logout, Toast.LENGTH_LONG).show();
			appPrefs.setIsLoggedIn(false);
        	//appPrefs.clearCustomerInfo();
        	Constants.isShowActionLogout = false;
			invalidateOptionsMenu();
			callFacebookLogout();
        	this.finish();
        	
        	return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
    
    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            
        	//selectedPosition = position;
        	//isSwitchFragment = true;
        	
        	
        	if(leftMenuItemList.get(position-1).getIsEnabled()){
	        	selectItem(position-1);
	        	if(Constants.DEBUG_MODE){
	        		Log.d(Constants.TAG, "position-1= " +  (position-1));
	        	}
	        	// update selected item and title, then close the drawer
	            mDrawerList.setItemChecked(position, true);
	            //mDrawerLayout.closeDrawer(mDrawerList);
	            //layoutActionBar.startAnimation(fadeIn); 
	            
	            new Handler().post(new Runnable(){
	
					@Override
					public void run() {
						mDrawerLayout.closeDrawer(mDrawerList);
			            
					}
					
				});
        	}else{
        		dialogRedirectToLanding = new DialogTwoButtons(MainActivity.this, getString(R.string.dialog_title_alert), getString(R.string.text_redirect_to_landing), getString(R.string.btn_cancel), getString(R.string.btn_ok), Constants.POPUP_REDIRECT_TO_LANDING, popupCallback);
        		dialogRedirectToLanding.show();
        	}
        }
    }

    public static void selectItem(int position) {
    	
    	if(fm!=null){
    		while(fm.getBackStackEntryCount() > 0){
				fm.popBackStackImmediate();
			}
    		Log.d(Constants.TAG, "fm.getBackStackEntryCount()= " +  fm.getBackStackEntryCount());
    	}

		switchFragment(leftMenuItemList.get(position).getLabel(), null, false);

    }

    @Override
    public void onResume(){
    	super.onResume();
    	if(appPrefs == null){
    		appPrefs = new AppPreferences(this);
    	}
		
		if(appPrefs.getEmergencyStatus()){
        	Intent intent = new Intent(this, EmergencyActivity.class);
    		startActivity(intent);
    		this.finish();
        }else{

        }    	
    }
    
    @Override
    protected void onDestroy() {
        //unregisterReceiver(mHandleMessageReceiver);
    	Utils.deleteDir(MainActivity.this);
        super.onDestroy();
        
    }
    
    /*
    public void setTitle(String title) {
    	mTitle = title;
        getActionBar().setTitle(mTitle);
    }
	*/
    
    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    
    public static void switchFragment(String fragmentName, Bundle bundle, boolean isAddToBackStack){
    	
    	if (fm != null) {
   			
			
			// Perform the FragmentTransaction to load in the list tab content.
			// Using FragmentTransaction#replace will destroy any Fragments
			// currently inside R.id.fragment_content and add the new Fragment
			// in its place.
			FragmentTransaction ft = fm.beginTransaction();
			//activeFragment = position;
			
			Fragment fragment = null;

			if(fm.findFragmentByTag(fragmentName) == null && fragmentName.equalsIgnoreCase(context.getString(R.string.side_nav_my_profile))){
				fragment = new MyProfileFragment();
			}
			else if(fm.findFragmentByTag(fragmentName) == null && fragmentName.equalsIgnoreCase(context.getString(R.string.side_nav_feedback))){
				fragment = new FeedbackFragment();
			}
			else if(fm.findFragmentByTag(fragmentName) == null && fragmentName.contains(context.getString(R.string.side_nav_about))){
				fragment = new AboutFragment();
			}
			else if(fm.findFragmentByTag(fragmentName) == null && fragmentName.equalsIgnoreCase(context.getString(R.string.fragment_full_map))){
				fragment = new FullMapFragment();
			}

			
			if(fragment !=null && ft !=null){

				Constants.isShowActionMenu = true;
			    //Constants.isShowActionEdit = false;
			    Constants.isShowActionAdd = false;
			    Constants.isShowActionLogout = false;
			   
				fragment.setRetainInstance(false);
				fragment.setArguments(bundle);
				ft.replace(R.id.content_frame, fragment, fragmentName);
				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				if(isAddToBackStack){
					ft.addToBackStack(null);
				}
				//ft.commit();
				     
				ft.commitAllowingStateLoss();
			}
			
			
		}
    	
    	checkNumberOfActiveBooking(context);
	        
    }
   	
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        	if(fm!=null && fm.getBackStackEntryCount() > 0){
   				fm.popBackStack();
   				/*
   				if(fm.getBackStackEntryCount() == 1){
   					showButton(context, getString(R.string.side_nav_merchants));
   					setTitle(getString(R.string.side_nav_merchants));
   				}
   				*/
   			}else{
   				if(fm!=null && fm.findFragmentByTag(getString(R.string.side_nav_my_profile))==null){

   					selectItem(0);
   					
   				}else if(fm!=null && fm.findFragmentByTag(getString(R.string.side_nav_my_profile))!=null){
					if(appPrefs.getIsLoggedIn()){


						if(exitNumber > 0){
							moveTaskToBack(true);
							exitNumber = 0;
						}else{
							Toast.makeText(MainActivity.this, getString(R.string.alert_message_press_again_to_exit), Toast.LENGTH_SHORT).show();
							exitNumber++;

						}
					}else{
						this.finish();
					}
   				}else{
   					if(mDrawerLayout.isDrawerOpen(mDrawerList)){
   						toggleDrawer(false);
   					}else{
		   				if(appPrefs.getIsLoggedIn()){
		   					
		   					
		   					if(exitNumber > 0){
		   						moveTaskToBack(true);
		   						exitNumber = 0;
		   					}else{
		   						Toast.makeText(MainActivity.this, getString(R.string.alert_message_press_again_to_exit), Toast.LENGTH_SHORT).show();
		   						exitNumber++;
			   					
		   					}
		   				}else{
		   					this.finish();
		   				}
   					}
   				}
   			}
        	
        }else if (keyCode == KeyEvent.KEYCODE_MENU) {
        	
        }else if (keyCode == KeyEvent.KEYCODE_HOME) {
        
        }else if (keyCode == KeyEvent.KEYCODE_APP_SWITCH) {
            
        }
        
        return false;
    }
    
    
   	public void onClick(View v){
   		/*
   		if(v == btnCloseLeftMenu){
   			toggleDrawer(false);
   		}
   		*/
   		/*
   		else if(v == btnBack){
   			if(fm!=null && fm.getBackStackEntryCount() > 0){
   				fm.popBackStack();
   				if(fm.getBackStackEntryCount() == 1){
   					showButton(context, getString(R.string.side_nav_merchants));
   					setTitle(getString(R.string.side_nav_merchants));
   				}
   			}
   		}
   		else if(v == btnFavourite){
   			
   		}
   		
   		else if(v == btnShare){
   			
   		}
   		*/
   		
   		
   	}

	@Override
	public void callBackPopup(Object data, int processID, int index) {
		if(processID == Constants.POPUP_APP_FORCE_UPDATE){
			
			if(Constants.DEBUG_MODE){
				Log.d(Constants.TAG, "getPackageName(): " + getPackageName());
			}
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
			this.finish();
		}
		else if(processID == Constants.POPUP_APP_OPTIONAL_UPDATE){
			if(index == Constants.DIALOG_BUTTON_RIGHT){
				if(Constants.DEBUG_MODE){
					Log.d(Constants.TAG, "getPackageName(): " + getPackageName());
				}
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
				this.finish();
			}
		}
		else if(processID == Constants.POPUP_REDIRECT_TO_LANDING){
			if(index == Constants.DIALOG_BUTTON_RIGHT){
				this.finish();
			}
			else if(index == Constants.DIALOG_BUTTON_LEFT){
				
			}
		}
		else if(processID == Constants.POPUP_API_CALL_SUCCEED){
			
		}
		else if(processID == Constants.POPUP_API_CALL_FAILED){
			
		}
	}
   	
   	/*
   	public static void showButton(Context context, String screenName){
   		btnMenu.setVisibility(View.GONE);
        //btnBack.setVisibility(View.GONE);
        //btnFavourite.setVisibility(View.GONE);
        //btnShare.setVisibility(View.GONE);
        btnLogout.setVisibility(View.GONE);
	        
   		if(screenName.equalsIgnoreCase(context.getString(R.string.side_nav_arrange_delivery))){
   			btnMenu.setVisibility(View.VISIBLE);
   	        
   		}else if(screenName.equalsIgnoreCase(context.getString(R.string.side_nav_delivery_status))){
   			btnMenu.setVisibility(View.VISIBLE);
   			
   		}else if(screenName.equalsIgnoreCase(context.getString(R.string.side_nav_favourites))){
   			btnMenu.setVisibility(View.VISIBLE);
   			
   		}else if(screenName.equalsIgnoreCase(context.getString(R.string.side_nav_history))){
   			btnMenu.setVisibility(View.VISIBLE);
   			
   		}else if(screenName.equalsIgnoreCase(context.getString(R.string.side_nav_my_profile))){
   			btnMenu.setVisibility(View.VISIBLE);
   			
   		}else if(screenName.equalsIgnoreCase(context.getString(R.string.side_nav_feedback))){
   			btnMenu.setVisibility(View.VISIBLE);
   			
   		}else if(screenName.equalsIgnoreCase(context.getString(R.string.side_nav_help))){
   			btnMenu.setVisibility(View.VISIBLE);
   			
   		}else if(screenName.equalsIgnoreCase(context.getString(R.string.side_nav_about))){
   			//btnBack.setVisibility(View.VISIBLE);
   			//btnFavourite.setVisibility(View.VISIBLE);
   	        
   		}
   	}
   	*/
	
	@Override
	public void callbackJson(String result, int processID, int index) {
		if(processID == Constants.ID_API_POST_UPDATE_DEVICE_ID){
			if(result!=null && !result.equalsIgnoreCase("")){
				try{
					JSONObject jsonObj = new JSONObject(result);
					
					int errorCode = jsonObj.getInt("errorCode");
					String message = jsonObj.getString("message");
					
					if(errorCode == Constants.ERROR_CODE_SUCCESS){
						
					}
				}catch(JSONException e){
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "JSONException: " + e.toString());
					}
				}catch(Exception e){
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "Exception: " + e.toString());
					}
				}
			}
		}
		else if(processID == Constants.ID_API_GET_MASTER_DATA){
			
			if(result!=null && !result.equalsIgnoreCase("")){
				try{
					JSONObject jsonObj = new JSONObject(result);
					
					int errorCode = jsonObj.getInt("errorCode");
					String message = jsonObj.getString("message");
					if(Constants.DEBUG_MODE){
		            	Log.d(Constants.TAG, "errorCode: " + errorCode);
		            	Log.d(Constants.TAG, "message: " + message);
		            }
					if(errorCode == Constants.ERROR_CODE_SUCCESS){
						if(jsonObj.has("app_version") && jsonObj.getString("app_version").startsWith("{")){
							//appPrefs.setAppVersionJSON(jsonObj.getString("app_version"));
							JSONObject appVersionObj = jsonObj.getJSONObject("app_version");
							if(appVersionObj.has("android") && appVersionObj.getString("android").startsWith("{")){
								JSONObject androidAppVersionObj = appVersionObj.getJSONObject("android");
								String appVersionName = androidAppVersionObj.has("version")?androidAppVersionObj.getString("version"):"1.0.0";
								boolean isForceUpdate = androidAppVersionObj.has("force_update")?androidAppVersionObj.getBoolean("force_update"):false;
								String appVersionMessage = androidAppVersionObj.has("message")?androidAppVersionObj.getString("message"):"";
								
								appPrefs.setAppVersionName(appVersionName);
								appPrefs.setIsForceUpdate(isForceUpdate);
								appPrefs.setAppVersionMessage(appVersionMessage);
							}
							
						}
						if(jsonObj.has("emergency") && jsonObj.getString("emergency").startsWith("{")){
							JSONObject emergencyObj = jsonObj.getJSONObject("emergency");
							
							boolean emergencyStatus = emergencyObj.has("status")?emergencyObj.getBoolean("status"):false;
							String emergencyMessage = emergencyObj.has("message")?emergencyObj.getString("message"):"";
							String emergencyRedirectURL = emergencyObj.has("redirect")?emergencyObj.getString("redirect"):"";
							
							appPrefs.setEmergencyStatus(emergencyStatus);
							appPrefs.setEmergencyMessage(emergencyMessage);
							appPrefs.setEmergencyRedirectURL(emergencyRedirectURL);
							
							if(Constants.DEBUG_MODE){
								Log.e(Constants.TAG, "emergencyStatus: " + emergencyStatus);
							}
						}
						if(jsonObj.has("master_data")){
							appPrefs.setMasterDataJSON(jsonObj.getString("master_data"));
						}
						
						if(appPrefs.getEmergencyStatus()){
				        	Intent intent = new Intent(this, EmergencyActivity.class);
				    		startActivity(intent);
				    		this.finish();
				        }else{
						
							String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
							
					        if(appPrefs.getAppVersionName().compareToIgnoreCase(versionName) > 0){
								if(appPrefs.getIsForceUpdate()){
									// force user to update
									if(Constants.DEBUG_MODE){
										Log.e(Constants.TAG, "force user to update");
									}
									if(dialogForceUpdate == null || !dialogForceUpdate.isShowing()){
										dialogForceUpdate = new DialogOK(MainActivity.this, 
												getString(R.string.dialog_title_app_update), 
												appPrefs.getAppVersionMessage(), //getString(R.string.alert_message_app_update), 
												getString(R.string.btn_go_to_play_store), 
												Constants.POPUP_APP_FORCE_UPDATE, 
												popupCallback);
										dialogForceUpdate.show();
									}
								}else{
									// prompt user to update (optional)
									if(Constants.DEBUG_MODE){
										Log.e(Constants.TAG, "prompt user to update (optional)");
									}
									Calendar todayCalendar = Calendar.getInstance();
									SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);
									if(!appPrefs.getOptionalUpdateLastCheckingDate().equalsIgnoreCase(sdf.format(todayCalendar.getTime())) && (dialogOptionalUpdate == null || !dialogOptionalUpdate.isShowing())){
										dialogOptionalUpdate = new DialogTwoButtons(MainActivity.this, 
												getString(R.string.dialog_title_app_update), 
												appPrefs.getAppVersionMessage(), //getString(R.string.alert_message_app_update), 
												getString(R.string.btn_maybe_later), 
												getString(R.string.btn_go_to_play_store), 
												Constants.POPUP_APP_OPTIONAL_UPDATE, 
												popupCallback);
										dialogOptionalUpdate.show();
										appPrefs.setOptionalUpdateLastCheckingDate(sdf.format(todayCalendar.getTime()));
									}
								}
					        }
				        }
						
					}else{
						if(Constants.DEBUG_MODE){
							Log.e(Constants.TAG, "JSONException: " + message);
						}
					}
				}catch(JSONException e){
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "JSONException: " + e.toString());
					}
				}catch(Exception e){
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "Exception: " + e.toString());
					}
				}
			}
		}
	}


	@Override
	public void jsonError(String msg, int processID) {
		if(Constants.DEBUG_MODE){
			Log.i(Constants.TAG, "jsonError: " + msg);
		}
		if(dialogError==null || !dialogError.isShowing()){
			dialogError = new DialogOK(MainActivity.this, getString(R.string.dialog_title_error), msg, getString(R.string.btn_ok), Constants.POPUP_API_CALL_FAILED, popupCallback);
			dialogError.show();
		}
	}
	
	public static void updateSideNav(){
		if(leftMenuItemList == null){
			leftMenuItemList = new ArrayList<SideMenuItem>();
		}else{
			leftMenuItemList.clear();
		}
		

  		if(appPrefs.getIsLoggedIn()){
			leftMenuItemList.add(new SideMenuItem(4, context.getString(R.string.side_nav_my_profile), R.drawable.ic_profile_menu, 1, 0, true));
	    	leftMenuItemList.add(new SideMenuItem(5, context.getString(R.string.side_nav_feedback), R.drawable.ic_comment, 1, 0, true));
	    	leftMenuItemList.add(new SideMenuItem(7, context.getString(R.string.side_nav_about), R.drawable.ic_comment, 1, 0, true));
  		}else{
  			leftMenuItemList.add(new SideMenuItem(4, context.getString(R.string.side_nav_my_profile), R.drawable.ic_profile_menu, 1, 0, true));
	    	leftMenuItemList.add(new SideMenuItem(5, context.getString(R.string.side_nav_feedback), R.drawable.ic_comment, 1, 0, true));
	    	leftMenuItemList.add(new SideMenuItem(7, context.getString(R.string.side_nav_about), R.drawable.ic_comment, 1, 0, true));
  		}
	}
	
	public void setupSideNav(){
		
  		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

    	drawerAdapter = new SideMenuArrayAdapter(context, leftMenuItemList);
	    
        
        if(mDrawerList.getHeaderViewsCount()>0 && leftMenuHeader!=null){
        	mDrawerList.removeHeaderView(leftMenuHeader);
        }
    	LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	
    	leftMenuHeader = (RelativeLayout) inflator.inflate(R.layout.row_left_menu_header_02, null);
    	//btnCloseLeftMenu = (ImageButton) leftMenuHeader.findViewById(R.id.btn_close_left_menu);
    	tvNameMenu = (TextView) leftMenuHeader.findViewById(R.id.tv_name_menu);
    	tvNameMenu.setTypeface(Utils.getFontAvenirLight(context));
    	ivProfileMenu = (SimpleDraweeView) leftMenuHeader.findViewById(R.id.iv_profile_menu);
    	
		mDrawerList.addHeaderView(leftMenuHeader, null, false);
    	//}
		
        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        
        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(drawerAdapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        
        /*
        mDrawerList.setOnItemSelectedListener(new OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				selectItem(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				
			}
        	
        });
        */
        
        /*
		spinClockWise = AnimationUtils.loadAnimation(MainActivity.this, R.anim.spin_clockwise);
		spinClockWise.setRepeatCount(0);
		spinAntiClockWise = AnimationUtils.loadAnimation(MainActivity.this, R.anim.spin_anticlockwise);
		spinAntiClockWise.setRepeatCount(0);
		*/
        
		//layoutActionBar.startAnimation(fadeIn); 
		
        // enable ActionBar app icon to behave as action to toggle nav drawer
        //getActionBar().setDisplayHomeAsUpEnabled(true);
        //getActionBar().setHomeButtonEnabled(true);
        //getActionBar().setCustomView(R.layout.action_bar);
        //getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);// | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
        //getActionBar().hide();
        
        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_menu_white_24dp_with_left_padding,  /* nav drawer image to replace 'Up' caret */
                R.string.side_nav_drawer_open,  /* "open drawer" description for accessibility */
                R.string.side_nav_drawer_close  /* "close drawer" description for accessibility */
                ) {
        	
            public void onDrawerClosed(View view) {
            	//Utils.setActionBarTitle(MainActivity.this, mTitle);
            	getActionBar().show();
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                //layoutActionBar.startAnimation(fadeIn); 
                
                /*
                if(isSwitchFragment){
                	selectItem(selectedPosition-1);
                	isSwitchFragment = false;
                }
                */
            }

            public void onDrawerOpened(View drawerView) {
                //getActionBar().setTitle(mDrawerTitle);
                getActionBar().hide();
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                //layoutActionBar.startAnimation(fadeOut); 
                
            }
            
            
            public void onDrawerStateChanged(int newState){
            	
            	if(newState != DrawerLayout.STATE_IDLE){
            		//btnCloseLeftMenu.startAnimation(spinClockWise);
            		getActionBar().hide();
            	}else if(!mDrawerLayout.isDrawerOpen(mDrawerList)){
            		getActionBar().show();
            	}
            }
            
            
            /*
            public void onDrawerSlide(View drawerView, float slideOffset){
            	getActionBar().hide();
            }
            */
        };
        
        
        mDrawerLayout.setDrawerListener(mDrawerToggle);

	}
	
	public static void backToScreen(String screenName){
		if(fm!=null){
			if(fm.findFragmentByTag(screenName)!=null){
				while(fm.getBackStackEntryCount() > 0 && !fm.findFragmentByTag(screenName).isVisible()){
					if(Constants.DEBUG_MODE){
						Log.d(Constants.TAG, "fm.findFragmentByTag(" + screenName + "): " + fm.findFragmentByTag(screenName).getTag());
						Log.d(Constants.TAG, "fm.getBackStackEntryCount(): " + fm.getBackStackEntryCount());
					}
					//fm.popBackStack(screenName, FragmentManager.POP_BACK_STACK_INCLUSIVE);
					fm.popBackStackImmediate();
				}
			}else{
				while(fm.getBackStackEntryCount() > 0 ){
					fm.popBackStackImmediate();
				}
				
				MainActivity.switchFragment(context.getString(R.string.side_nav_my_profile), new Bundle(), false);
    				
			}
		}
	}

}
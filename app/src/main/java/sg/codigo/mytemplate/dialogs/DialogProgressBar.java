package sg.codigo.mytemplate.dialogs;

import sg.codigo.mytemplate.R;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;




public class DialogProgressBar extends Dialog {
	
	public DialogProgressBar(Context context) {
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_progress);
		getWindow().setBackgroundDrawableResource (android.R.color.transparent);
     	setCancelable(false);
     	
		
	}
	
	public DialogProgressBar(Context context,boolean cancelable) {
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_progress);
		getWindow().setBackgroundDrawableResource (android.R.color.transparent);
		setCancelable(cancelable);
		

	}
	
}

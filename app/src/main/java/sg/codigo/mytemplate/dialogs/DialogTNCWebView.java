package sg.codigo.mytemplate.dialogs;

import sg.codigo.mytemplate.callbacks.PopupCallback;
import sg.codigo.mytemplate.R;
import sg.codigo.mytemplate.utils.Constants;
import sg.codigo.mytemplate.utils.Utils;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


public class DialogTNCWebView extends Dialog{

	private WebView wvTNC;
	
	private Context context;
	private int animationIndex = 1;
	
	public DialogTNCWebView(Context context, String url) {
		super(context);
		this.context = context;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_tnc_webview);
		setCancelable(true);
		setCanceledOnTouchOutside(true);
		getWindow().setBackgroundDrawable(new ColorDrawable(0));
		getWindow().setLayout(android.view.ViewGroup.LayoutParams.MATCH_PARENT,android.view.ViewGroup.LayoutParams.MATCH_PARENT);
		
		wvTNC = (WebView) findViewById(R.id.wv_tnc);
		
		if(!Utils.isShowingLoadingDialog(context)){
			Utils.showLoadingDialog(context);
		}
		wvTNC.loadUrl(url);
    	
		wvTNC.setWebViewClient(webViewClient);
		wvTNC.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
			    return true;
			}
		});
		wvTNC.setLongClickable(false);
		
		YoYo.with(Techniques.FadeIn)
		.duration(Constants.animationTime * animationIndex)
		.playOn(wvTNC);
	}

	WebViewClient webViewClient= new WebViewClient(){
	    @Override
	    public boolean shouldOverrideUrlLoading(WebView  view, String  url){
	    	if(Constants.DEBUG_MODE){
	    		Log.d(Constants.TAG, "shouldOverrideUrlLoading url: " + url);
	    	}
	        if(url.contains("mailto")){
	        	String[] emailData = url.split(":");
	        	if(emailData.length>1){
	        	Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(emailData[0],emailData[1], null));
	        		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "FastFast Delivery(Customer)");
	        		context.startActivity(Intent.createChooser(emailIntent, "Send email"));
	        	}
	        }
	        
	        else{
	        	Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
	        	context.startActivity(browserIntent);
	        }
	        
	        return true;
	    }
	    
	    @Override
	    public void onPageFinished(WebView view, String url){
	    	if(Utils.isShowingLoadingDialog(context)){
	    		Utils.hideLoadingDialog(context);
	    	}
	    }
	    
	    /*
	    @Override
	    public void onLoadResource(WebView view, String url){
	    	
	    	if(Constants.DEBUG_MODE){
	    		Log.d(Constants.TAG, "onLoadResource url: " + url);
	    	}
	        if(url.contains("mailto")){
	        	String[] emailData = url.split(":");
	        	if(emailData.length>1){
	        	Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(emailData[0],emailData[1], null));
	        		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "FastFast Delivery(Customer)");
	        		context.startActivity(Intent.createChooser(emailIntent, "Send email"));
	        	}
	        }
	        
	    }
	    */
	};
	
}

package sg.codigo.mytemplate.dialogs;

import sg.codigo.mytemplate.callbacks.PopupCallback;
import sg.codigo.mytemplate.R;
import sg.codigo.mytemplate.models.Address;
import sg.codigo.mytemplate.utils.Constants;
import sg.codigo.mytemplate.utils.Utils;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class DialogChangePassword extends Dialog implements Button.OnClickListener{
	private Context mContext;
	private PopupCallback popupCallback;
	private int processID;
	private int animationIndex = 1;
	private Address address;
	
	private RelativeLayout layoutDialog;
	private TextView dialogTitle;
	private TextView tvLabelCurrentPassword;
	private EditText etCurrentPassword;
	private TextView tvLabelNewPassword;
	private EditText etNewPassword;
	private TextView tvLabelRetypeNewPassword;
	private EditText etRetypeNewPassword;
	private Button btnLeft, btnRight;
	
	public DialogChangePassword(Context context, String title, String btnLeftName, String btnRightName, int processID, PopupCallback popupCallback, Address address) {
		super(context);
		this.mContext = context;
		this.address = address;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_change_password);
		setCancelable(false);
		setCanceledOnTouchOutside(false);
		getWindow().setBackgroundDrawable(new ColorDrawable(0));
		getWindow().setLayout(android.view.ViewGroup.LayoutParams.MATCH_PARENT,android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		
		this.popupCallback = popupCallback;
		this.processID = processID;
		
		layoutDialog = (RelativeLayout) findViewById(R.id.layout_dialog);
		dialogTitle = (TextView) findViewById(R.id.dialog_title);
		dialogTitle.setTypeface(Utils.getFontAvenirHeavy(context));
		tvLabelCurrentPassword = (TextView) findViewById(R.id.tv_label_current_password);
		tvLabelCurrentPassword.setTypeface(Utils.getFontAvenirBlack(context));
		etCurrentPassword = (EditText) findViewById(R.id.et_current_password);
		etCurrentPassword.setTypeface(Utils.getFontAvenirLight(context));
		tvLabelNewPassword = (TextView) findViewById(R.id.tv_label_new_password);
		tvLabelNewPassword.setTypeface(Utils.getFontAvenirBlack(context));
		etNewPassword = (EditText) findViewById(R.id.et_new_password);
		etNewPassword.setTypeface(Utils.getFontAvenirLight(context));
		tvLabelRetypeNewPassword = (TextView) findViewById(R.id.tv_label_retype_new_password);
		tvLabelRetypeNewPassword.setTypeface(Utils.getFontAvenirBlack(context));
		etRetypeNewPassword = (EditText) findViewById(R.id.et_retype_new_password);
		etRetypeNewPassword.setTypeface(Utils.getFontAvenirLight(context));
		
		TextWatcher currentPasswordTextWatcher = new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if(tvLabelCurrentPassword.getVisibility()==View.GONE && s.length() > 0){
					YoYo.with(Techniques.FadeInDown)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	tvLabelCurrentPassword.setVisibility(View.VISIBLE);
                        	etCurrentPassword.setHint(mContext.getString(R.string.hint_current_password));
                        	etCurrentPassword.setHintTextColor(mContext.getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelCurrentPassword);
					
					
					
				}else if(tvLabelCurrentPassword.getVisibility()==View.VISIBLE && s.length() <= 0){
					YoYo.with(Techniques.FadeOutUp)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	etCurrentPassword.setHint(mContext.getString(R.string.hint_current_password));
                        	etCurrentPassword.setHintTextColor(mContext.getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	tvLabelCurrentPassword.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelCurrentPassword);
					
				}
			}
    		
    	};
    	
    	etCurrentPassword.addTextChangedListener(currentPasswordTextWatcher);
    	
    	TextWatcher newPasswordTextWatcher = new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if(tvLabelNewPassword.getVisibility()==View.GONE && s.length() > 0){
					YoYo.with(Techniques.FadeInDown)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	tvLabelNewPassword.setVisibility(View.VISIBLE);
                        	etNewPassword.setHint(mContext.getString(R.string.hint_new_password));
                        	etNewPassword.setHintTextColor(mContext.getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelNewPassword);
					
					
					
				}else if(tvLabelNewPassword.getVisibility()==View.VISIBLE && s.length() <= 0){
					YoYo.with(Techniques.FadeOutUp)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	etNewPassword.setHint(mContext.getString(R.string.hint_new_password));
                        	etNewPassword.setHintTextColor(mContext.getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	tvLabelNewPassword.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelNewPassword);
					
				}
			}
    		
    	};
    	
    	etNewPassword.addTextChangedListener(newPasswordTextWatcher);
    	
    	TextWatcher retypeNewPasswordWatcher = new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if(tvLabelRetypeNewPassword.getVisibility()==View.GONE && s.length() > 0){
					YoYo.with(Techniques.FadeInDown)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	tvLabelRetypeNewPassword.setVisibility(View.VISIBLE);
                        	etRetypeNewPassword.setHint(mContext.getString(R.string.hint_retype_new_password));
                        	etRetypeNewPassword.setHintTextColor(mContext.getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelRetypeNewPassword);
					
					
					
				}else if(tvLabelRetypeNewPassword.getVisibility()==View.VISIBLE && s.length() <= 0){
					YoYo.with(Techniques.FadeOutUp)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	etRetypeNewPassword.setHint(mContext.getString(R.string.hint_retype_new_password));
                        	etRetypeNewPassword.setHintTextColor(mContext.getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	tvLabelRetypeNewPassword.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelRetypeNewPassword);
					
				}
			}
    		
    	};
    	
    	etRetypeNewPassword.addTextChangedListener(retypeNewPasswordWatcher);
    	
		
    	etCurrentPassword.setText("");
    	etNewPassword.setText("");
    	etRetypeNewPassword.setText("");
		
		btnLeft = (Button) findViewById(R.id.dialog_btn_left);
		btnLeft.setTypeface(Utils.getFontAvenirHeavy(context));
		btnLeft.setOnClickListener(this);
		
		btnRight = (Button) findViewById(R.id.dialog_btn_right);
		btnRight.setTypeface(Utils.getFontAvenirHeavy(context));
		btnRight.setOnClickListener(this);
		
		if (!title.equals("")) {
			dialogTitle.setVisibility(View.VISIBLE);
			dialogTitle.setText(title);
		}else{
			dialogTitle.setVisibility(View.INVISIBLE);
		}
		
		btnLeft.setText(btnLeftName);
		btnRight.setText(btnRightName);
		
		
    	YoYo.with(Techniques.FadeIn)
		.duration(Constants.animationTime * animationIndex)
		.playOn(layoutDialog);
    	
	}

	@Override
	public void onClick(View v) {
		if(v == btnLeft){
			this.popupCallback.callBackPopup("", this.processID, Constants.DIALOG_BUTTON_LEFT);
			YoYo.with(Techniques.FadeOut)
			.duration(Constants.animationTime * animationIndex)
			.withListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                	dismiss();
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                   
                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            })
			.playOn(layoutDialog);
			
			
		}
		else if(v == btnRight){
			boolean hasError = false;
    		View viewHighlighted = null;
    		
			if(etCurrentPassword.getText().toString().trim().equalsIgnoreCase("")){
				hasError = true;
				etCurrentPassword.setText("");
				etCurrentPassword.setHint(mContext.getString(R.string.error_message_current_password_01));
				etCurrentPassword.setHintTextColor(mContext.getResources().getColor(R.color.error_red));
				
				YoYo.with(Techniques.Shake)
    			.duration(Constants.animationTime * animationIndex)
    			.playOn(etCurrentPassword);
    	    	
				if(viewHighlighted == null){
    				viewHighlighted = etCurrentPassword;
    			}
			}
			/*
			else if(etCurrentPassword.getText().toString().trim().equalsIgnoreCase("")){
				hasError = true;
				etCurrentPassword.setText("");
				etCurrentPassword.setHint(mContext.getString(R.string.error_message_current_password_02));
				etCurrentPassword.setHintTextColor(mContext.getResources().getColor(R.color.error_red));
				
				YoYo.with(Techniques.Shake)
    			.duration(Constants.animationTime * animationIndex)
    			.playOn(etCurrentPassword);
    	    	
				if(viewHighlighted == null){
    				viewHighlighted = etCurrentPassword;
    			}
			}
			*/
			if(etNewPassword.getText().toString().trim().equalsIgnoreCase("")){
				hasError = true;
				etNewPassword.setText("");
				etNewPassword.setHint(mContext.getString(R.string.error_message_new_password_01));
				etNewPassword.setHintTextColor(mContext.getResources().getColor(R.color.error_red));
				
				YoYo.with(Techniques.Shake)
    			.duration(Constants.animationTime * animationIndex)
    			.playOn(etNewPassword);
    	    	
				if(viewHighlighted == null){
    				viewHighlighted = etNewPassword;
    			}
			}else if(etNewPassword.getText().toString().trim().length() < 6){
				hasError = true;
				etNewPassword.setText("");
				etNewPassword.setHint(mContext.getString(R.string.error_message_new_password_02));
				etNewPassword.setHintTextColor(mContext.getResources().getColor(R.color.error_red));
				
				YoYo.with(Techniques.Shake)
    			.duration(Constants.animationTime * animationIndex)
    			.playOn(etNewPassword);
    	    	
				if(viewHighlighted == null){
    				viewHighlighted = etNewPassword;
    			}
			}
			
			if(etRetypeNewPassword.getText().toString().trim().equalsIgnoreCase("")){
				hasError = true;
				etRetypeNewPassword.setText("");
				etRetypeNewPassword.setHint(mContext.getString(R.string.error_message_new_password_01));
				etRetypeNewPassword.setHintTextColor(mContext.getResources().getColor(R.color.error_red));
				
				YoYo.with(Techniques.Shake)
    			.duration(Constants.animationTime * animationIndex)
    			.playOn(etRetypeNewPassword);
    	    	
				if(viewHighlighted == null){
    				viewHighlighted = etRetypeNewPassword;
    			}
			}else if(etRetypeNewPassword.getText().toString().trim().length() < 6){
				hasError = true;
				etRetypeNewPassword.setText("");
				etRetypeNewPassword.setHint(mContext.getString(R.string.error_message_new_password_02));
				etRetypeNewPassword.setHintTextColor(mContext.getResources().getColor(R.color.error_red));
				
				YoYo.with(Techniques.Shake)
    			.duration(Constants.animationTime * animationIndex)
    			.playOn(etRetypeNewPassword);
    	    	
				if(viewHighlighted == null){
    				viewHighlighted = etRetypeNewPassword;
    			}
			}else if(!etRetypeNewPassword.getText().toString().trim().equalsIgnoreCase(etNewPassword.getText().toString().trim())){
				hasError = true;
				etRetypeNewPassword.setText("");
				etRetypeNewPassword.setHint(mContext.getString(R.string.error_message_new_password_03));
				etRetypeNewPassword.setHintTextColor(mContext.getResources().getColor(R.color.error_red));
				
				YoYo.with(Techniques.Shake)
    			.duration(Constants.animationTime * animationIndex)
    			.playOn(etRetypeNewPassword);
    	    	
				if(viewHighlighted == null){
    				viewHighlighted = etRetypeNewPassword;
    			}
			}
			
			if(!hasError){
				String[] passwords = new String[2];
				
				passwords[0] = etCurrentPassword.getText().toString().trim();
				passwords[1] = etRetypeNewPassword.getText().toString().trim();
				this.popupCallback.callBackPopup(passwords, this.processID, Constants.DIALOG_BUTTON_RIGHT);
				
				this.dismiss();
    		}else{
    			if(viewHighlighted != null){
    				viewHighlighted.requestFocus();
    			}
    		}
			
		}
		
	}
}

package sg.codigo.mytemplate.dialogs;

import sg.codigo.mytemplate.callbacks.PopupCallback;
import sg.codigo.mytemplate.R;
import sg.codigo.mytemplate.utils.Constants;
import sg.codigo.mytemplate.utils.Utils;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class DialogOK extends Dialog implements Button.OnClickListener{

	private RelativeLayout layoutDialog;
	private TextView dialogTitle;
	private TextView dialogMessage;
	private Button btnNeutral;
	
	private PopupCallback popupCallback;
	private int processID;
	private int animationIndex = 1;
	
	public DialogOK(Context context, String title, String message, String btn, int processID, PopupCallback popupCallback) {
		super(context);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_with_single_button);
		setCancelable(false);
		setCanceledOnTouchOutside(false);
		getWindow().setBackgroundDrawable(new ColorDrawable(0));
		getWindow().setLayout(android.view.ViewGroup.LayoutParams.MATCH_PARENT,android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		
		this.popupCallback = popupCallback;
		this.processID = processID;
		
		layoutDialog = (RelativeLayout) findViewById(R.id.layout_dialog);
		dialogTitle = (TextView) findViewById(R.id.dialog_title);
		dialogTitle.setTypeface(Utils.getFontAvenirHeavy(context));
		dialogMessage = (TextView) findViewById(R.id.dialog_message);
		dialogMessage.setTypeface(Utils.getFontAvenirLight(context));
		btnNeutral = (Button) findViewById(R.id.dialog_btn_neutral);
		btnNeutral.setTypeface(Utils.getFontAvenirHeavy(context));
		btnNeutral.setOnClickListener(this);
		
		if (!title.equals("")) {
			dialogTitle.setVisibility(View.VISIBLE);
			dialogTitle.setText(title);
		}else{
			dialogTitle.setVisibility(View.INVISIBLE);
		}
		
		dialogMessage.setText(message);
		btnNeutral.setText(btn);
		
		YoYo.with(Techniques.FadeIn)
		.duration(Constants.animationTime * animationIndex)
		.playOn(layoutDialog);
	}

	@Override
	public void onClick(View v) {
		if(v == btnNeutral){
			this.popupCallback.callBackPopup("", this.processID, Constants.DIALOG_BUTTON_OK);
			YoYo.with(Techniques.FadeOut)
			.duration(Constants.animationTime * animationIndex)
			.withListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                	dismiss();
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                   
                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            })
			.playOn(layoutDialog);
		}
		
	}
}

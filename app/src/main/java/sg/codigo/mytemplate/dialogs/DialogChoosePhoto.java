package sg.codigo.mytemplate.dialogs;

import java.util.ArrayList;
import java.util.List;

import sg.codigo.mytemplate.adapters.ActionArrayAdapter;
import sg.codigo.mytemplate.callbacks.PopupCallback;
import sg.codigo.mytemplate.R;
import sg.codigo.mytemplate.models.ActionItem;
import sg.codigo.mytemplate.utils.Constants;
import sg.codigo.mytemplate.utils.Utils;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


public class DialogChoosePhoto extends Dialog{

	private LinearLayout layoutDialog;
	private ListView lvChoosePhoto;
	private List<ActionItem> actionList;
	private ActionArrayAdapter actionArrayAdapter;
	
	private PopupCallback popupCallback;
	private int processID;
	private int animationIndex = 1;
	
	public DialogChoosePhoto(Context context, final int processID, final PopupCallback popupCallback) {
		super(context);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_choose_photo);
		setCancelable(true);
		setCanceledOnTouchOutside(true);
		//getWindow().setBackgroundDrawable(new ColorDrawable(0));
		//getWindow().setLayout(android.view.ViewGroup.LayoutParams.MATCH_PARENT,android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		
		this.popupCallback = popupCallback;
		this.processID = processID;
		
		actionList = new ArrayList<ActionItem>();
		actionList.add(new ActionItem(1, "Take photo", R.drawable.ic_camera));
		actionList.add(new ActionItem(2, "Attach photo", R.drawable.ic_insert_photo));
		
		layoutDialog = (LinearLayout) findViewById(R.id.layout_dialog);
		lvChoosePhoto = (ListView) findViewById(R.id.lv_choose_photo);
		
		lvChoosePhoto.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if(position == 0){
					popupCallback.callBackPopup("", processID, Constants.DIALOG_BUTTON_TAKE_PHOTO);
					dismiss();
				}else{
					popupCallback.callBackPopup("", processID, Constants.DIALOG_BUTTON_ATTACH_PHOTO);
					dismiss();
				}
			}
			
		});
		
		actionArrayAdapter = new ActionArrayAdapter(context, actionList);
		lvChoosePhoto.setAdapter(actionArrayAdapter);
		
		YoYo.with(Techniques.FadeIn)
		.duration(Constants.animationTime * animationIndex)
		.playOn(layoutDialog);
	}

	
}

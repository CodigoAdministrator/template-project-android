package sg.codigo.mytemplate.dialogs;

import sg.codigo.mytemplate.callbacks.PopupCallback;
import sg.codigo.mytemplate.R;
import sg.codigo.mytemplate.utils.Constants;
import sg.codigo.mytemplate.utils.Utils;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class DialogTwoButtons extends Dialog implements Button.OnClickListener{
	private Context mContext;
	private PopupCallback popupCallback;
	private int processID;
	private int animationIndex = 1;
	
	private RelativeLayout layoutDialog;
	private TextView dialogTitle;
	private TextView dialogMessage;
	private Button btnLeft, btnRight;
	
	public DialogTwoButtons(Context context, String title, String message, String btnLeftName, String btnRightName, int processID, PopupCallback popupCallback) {
		super(context);
		this.mContext = context;
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_with_two_buttons);
		setCancelable(false);
		setCanceledOnTouchOutside(false);
		getWindow().setBackgroundDrawable(new ColorDrawable(0));
		getWindow().setLayout(android.view.ViewGroup.LayoutParams.MATCH_PARENT,android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		
		this.popupCallback = popupCallback;
		this.processID = processID;
		
		layoutDialog = (RelativeLayout) findViewById(R.id.layout_dialog);
		dialogTitle = (TextView) findViewById(R.id.dialog_title);
		dialogTitle.setTypeface(Utils.getFontAvenirHeavy(context));
		dialogMessage = (TextView) findViewById(R.id.dialog_message);
		dialogMessage.setTypeface(Utils.getFontAvenirLight(context));
		
		btnLeft = (Button) findViewById(R.id.dialog_btn_left);
		btnLeft.setTypeface(Utils.getFontAvenirHeavy(context));
		btnLeft.setOnClickListener(this);
		
		btnRight = (Button) findViewById(R.id.dialog_btn_right);
		btnRight.setTypeface(Utils.getFontAvenirHeavy(context));
		btnRight.setOnClickListener(this);
		
		if (!title.equals("")) {
			dialogTitle.setVisibility(View.VISIBLE);
			dialogTitle.setText(title);
		}else{
			dialogTitle.setVisibility(View.INVISIBLE);
		}
		
		dialogMessage.setText(message);
		
		btnLeft.setText(btnLeftName);
		btnRight.setText(btnRightName);
		
		
    	YoYo.with(Techniques.FadeIn)
		.duration(Constants.animationTime * animationIndex)
		.playOn(layoutDialog);
    	
	}

	@Override
	public void onClick(View v) {
		if(v == btnLeft){
			this.popupCallback.callBackPopup("", this.processID, Constants.DIALOG_BUTTON_LEFT);
			YoYo.with(Techniques.FadeOut)
			.duration(Constants.animationTime * animationIndex)
			.withListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                	dismiss();
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                   
                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            })
			.playOn(layoutDialog);
			
		}
		else if(v == btnRight){
			this.popupCallback.callBackPopup("", this.processID, Constants.DIALOG_BUTTON_RIGHT);
			dismiss();
		}
	}
}

package sg.codigo.mytemplate.dialogs;

import sg.codigo.mytemplate.callbacks.PopupCallback;
import sg.codigo.mytemplate.R;
import sg.codigo.mytemplate.utils.Constants;
import sg.codigo.mytemplate.utils.Utils;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


public class DialogTNC extends Dialog{

	private LinearLayout layoutDialog;
	private TextView dialogTitle;
	private TextView dialogMessage;
	
	private int animationIndex = 1;
	
	public DialogTNC(Context context, String title, String message) {
		super(context);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_tnc);
		setCancelable(true);
		setCanceledOnTouchOutside(true);
		getWindow().setBackgroundDrawable(new ColorDrawable(0));
		getWindow().setLayout(android.view.ViewGroup.LayoutParams.MATCH_PARENT,android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		
		layoutDialog = (LinearLayout) findViewById(R.id.layout_dialog);
		dialogTitle = (TextView) findViewById(R.id.dialog_title);
		dialogTitle.setTypeface(Utils.getFontAvenirHeavy(context));
		dialogMessage = (TextView) findViewById(R.id.dialog_message);
		dialogMessage.setTypeface(Utils.getFontAvenirLight(context));
		
		if (!title.equals("")) {
			dialogTitle.setVisibility(View.VISIBLE);
			dialogTitle.setText(title);
		}else{
			dialogTitle.setVisibility(View.INVISIBLE);
		}
		
		dialogMessage.setText(message);
		
		YoYo.with(Techniques.FadeIn)
		.duration(Constants.animationTime * animationIndex)
		.playOn(layoutDialog);
	}

}

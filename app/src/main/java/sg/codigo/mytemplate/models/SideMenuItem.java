package sg.codigo.mytemplate.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SideMenuItem implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4409430353043288927L;
	private int id;
	private String label;
	private int iconRes;
	private int type; 		// 0: header,  1: item 
	private int notificationNo;
	private boolean isExpand;
	private boolean isEnabled;
	private List<String> subItems;
	
	public SideMenuItem(int id, String label, int iconRes, int type, int notificationNo, boolean isEnabled){
		this.id = id;
		this.label = label; 
		this.iconRes = iconRes;
		this.type = type; 		// 0: header,  1: item
		this.notificationNo = notificationNo;
		this.isExpand = false;
		this.isEnabled = isEnabled;
		this.subItems = new ArrayList<String>();
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public void setLabel(String label){
		this.label = label;
	}
	
	public void setIconRes(int iconRes){
		this.iconRes = iconRes;
	}
	
	public void setType(int type){
		this.type = type;
	}
	
	public void setNotificationNo(int notificationNo){
		this.notificationNo = notificationNo;
	}
	
	public void setIsExpand(boolean isExpand){
		this.isExpand = isExpand;
	}
	
	public void setIsEnabled(boolean isEnabled){
		this.isEnabled = isEnabled;
	}
	
	public void setSubItems(List<String> subItems){
		this.subItems = subItems;
	}
	
	public void addSubItem(String subItem){
		this.subItems.add(subItem);
	}
	
	public int getId(){
		return this.id;
	}
	
	public String getLabel(){
		return this.label;
	}
	
	public int getIconRes(){
		return this.iconRes;
	}
	
	public int getType(){
		return this.type;
	}
	
	public int getNotificationNo(){
		return this.notificationNo;
	}
	
	public boolean getIsExpand(){
		return this.isExpand;
	}
	
	public boolean getIsEnabled(){
		return this.isEnabled;
	}
	
	public List<String> getSubItems(){
		return this.subItems;
	}
}

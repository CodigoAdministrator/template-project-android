package sg.codigo.mytemplate.models;

import java.io.Serializable;

public class Location implements Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = 234964052927293322L;
	private double lat;
	private double lon;
	
	public Location(){
		this.lat = 0;
		this.lon = 0; 
	}
	
	public Location(double lat, double lon){
		this.lat = lat;
		this.lon = lon; 
	}
	
	public void setLat(double lat){
		this.lat = lat;
	}
	
	public void setLon(double lon){
		this.lon = lon;
	}
	
	
	public double getLat(){
		return this.lat;
	}
	
	public double getLon(){
		return this.lon;
	}
	
	
}

package sg.codigo.mytemplate.models;

import java.io.Serializable;

public class Image implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5212467806197375902L;
	private int id;
	private String name;
	private String path;
	
	public Image(){
		this.id = 0;
		this.name = ""; 
		this.path = "";
	}
	
	public Image(int id, String name, String path){
		this.id = id;
		this.name = name; 
		this.path = path;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setPath(String path){
		this.path = path;
	}
	
	
	public int getId(){
		return this.id;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getPath(){
		return this.path;
	}
	
}

package sg.codigo.mytemplate.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ActionItem implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3767062998788341874L;
	private int id;
	private String label;
	private int iconRes;
	
	public ActionItem(int id, String label, int iconRes){
		this.id = id;
		this.label = label; 
		this.iconRes = iconRes;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public void setLabel(String label){
		this.label = label;
	}
	
	public void setIconRes(int iconRes){
		this.iconRes = iconRes;
	}
	
	public int getId(){
		return this.id;
	}
	
	public String getLabel(){
		return this.label;
	}
	
	public int getIconRes(){
		return this.iconRes;
	}
}

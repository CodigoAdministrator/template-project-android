package sg.codigo.mytemplate.models;

import java.io.Serializable;

public class Country implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2560667727453079765L;
	private int id;
	private String name;
	
	public Country(int id, String name){
		this.id = id;
		this.name = name; 
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	
	public int getId(){
		return this.id;
	}
	
	public String getName(){
		return this.name;
	}
	
	
}

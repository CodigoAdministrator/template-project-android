package sg.codigo.mytemplate.models;

import java.io.Serializable;

public class APIResult implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3793300840508688246L;
	private int errorCode;
	private String message;
	private Object data;
	private String jsonString;
	
	public APIResult(){
		this.setErrorCode(0);
		this.setMessage(""); 
		this.setData(null);
		this.setJSONString("");
	}
	
	public void setErrorCode(int errorCode){
		this.errorCode = errorCode;
	}
	
	public void setMessage(String message){
		this.message = message;
	}
	
	public void setData(Object data){
		this.data = data;
	}
	
	public void setJSONString(String jsonString){
		this.jsonString = jsonString;
	}
	
	public int getErrorCode(){
		return this.errorCode;
	}
	
	public String getMessage(){
		return this.message;
	}
	
	public Object getData(){
		return this.data;
	}
	
	public String toString(){
		return this.jsonString;
	}
}

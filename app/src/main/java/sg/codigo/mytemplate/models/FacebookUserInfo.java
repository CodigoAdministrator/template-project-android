package sg.codigo.mytemplate.models;

import java.io.Serializable;

public class FacebookUserInfo  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6978314087746513340L;
	private String id;
	private String name;
	private String email;
	private String mobile;	
	private String gender;
	private String dob;
	private String country;
	private String city;
	private String profilePic;
	
	public FacebookUserInfo(){
		this.id="";
		this.name="";
		this.email="";
		this.mobile="";
		this.gender="";
		this.dob="";
		this.country = "";
		this.city = "";
		this.profilePic = "";
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setDOB(String dob) {
		this.dob = dob;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public void setProfilePic(String profilePic){
		this.profilePic = profilePic;
	}
	
	public String getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getMobile() {
		return mobile;
	}
	
	public String getGender() {
		return gender;
	}

	public String getDOB() {
		return dob;
	}
	
	public String getCountry(){
		return this.country;
	}
	
	public String getCity(){
		return this.city;
	}
	
	public String setProfilePic(){
		return this.profilePic;
	}
	
}

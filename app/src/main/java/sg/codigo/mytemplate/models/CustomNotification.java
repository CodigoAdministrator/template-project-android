package sg.codigo.mytemplate.models;

import java.io.Serializable;

public class CustomNotification implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1729706563791032406L;
	private int id;
	private String from;
	private String timestamp;
	private String type;
	private String message;
	private String sound;
	private String collapseKey;
	
	public CustomNotification(){
		this.id=0;
		this.from = "";
		this.timestamp = "";
		this.type = "";
		this.message = "";
		this.sound = "";
		this.collapseKey = "";
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public void setFrom(String from){
		this.from = from;
	}
	
	public void setTimestamp(String timestamp){
		this.timestamp = timestamp;
	}
	
	public void setType(String type){
		this.type = type;
	}
	
	public void setMessage(String message){
		this.message = message;
	}
	
	public void setSound(String sound){
		this.sound = sound;
	}
	
	public void setCollapseKey(String collapseKey){
		this.collapseKey = collapseKey;
	}
	
	public int getId(){
		return this.id;
	}

	public String getFrom(){
		return this.from;
	}
	
	public String getTimestamp(){
		return this.timestamp;
	}
	
	public String getType(){
		return this.type;
	}
	
	public String getMessage(){
		return this.message;
	}
	
	public String getSound(){
		return this.sound;
	}
	
	public String getCollapseKey(){
		return this.collapseKey;
	}
	
	public String toString(){
		return "id: " + this.getId() + ", "
				+"from: " + this.getFrom() + ", "
				+"timestamp: " + this.getTimestamp() + ", "
				+"type: " + this.getType() + ", "
				+"message: " + this.getMessage() + ", "
				+"sound: " + this.getSound() + ", "
				+"collapseKey: " + this.getCollapseKey()
				;
				
	}
}

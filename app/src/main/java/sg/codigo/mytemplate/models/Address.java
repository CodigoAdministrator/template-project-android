package sg.codigo.mytemplate.models;

import java.io.Serializable;

public class Address implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5587477214922513712L;
	private int id;
	private String placeId;
	private String title;
	private String fullAddress;
	private String unitNumber;
	private double lat;
	private double lon;
	private String postalCode;
	private double distance;
	private boolean isShowFavourite;
	private boolean isFavourited;
	private boolean isHeader;
	
	public Address(){
		this.id = 0;
		this.placeId = "";
		this.title = "";
		this.fullAddress = ""; 
		this.unitNumber = "";
		this.lat = 0;
		this.lon = 0;
		this.postalCode = "";
		this.distance = 0;
		this.isShowFavourite = false;
		this.isFavourited = false;
		this.isHeader = false;
		
	}
	
	/*
	public Address(String title, String fullAddress, boolean isShowFavourite, boolean isFavourited){
		this.title = title;
		this.fullAddress = fullAddress; 
		this.isShowFavourite = isShowFavourite;
		this.isFavourited = isFavourited;
	}
	*/
	
	public void setId(int id){
		this.id = id;
	}
	
	public void setPlaceId(String placeId){
		this.placeId = placeId;
	}
	
	public void setTitle(String title){
		this.title = title;
	}
	
	public void setFullAddress(String fullAddress){
		this.fullAddress = fullAddress;
	}
	
	public void setUnitNumber(String unitNumber){
		this.unitNumber = unitNumber;
	}
	
	public void setLat(double lat){
		this.lat = lat;
	}

	public void setLon(double lon){
		this.lon = lon;
	}
	
	public void setPostalCode(String postalCode){
		this.postalCode = postalCode;
	}
	
	public void setDistance(double distance){
		this.distance = distance;
	}
	
	public void setIsShowFavourite(boolean isShowFavourite){
		this.isShowFavourite = isShowFavourite;
	}
	
	public void setIsFavourited(boolean isFavourited){
		this.isFavourited = isFavourited;
	}
	
	public void setIsHeader(boolean isHeader){
		this.isHeader = isHeader;
	}
	
	
	public int getId(){
		return this.id;
	}
	
	public String getPlaceId(){
		return this.placeId;
	}
	
	public String getTitle(){
		return this.title;
	}
	
	public String getFullAddress(){
		return this.fullAddress;
	}
	
	public String getUnitNumber(){
		return this.unitNumber;
	}
	
	public double getLat(){
		return this.lat;
	}

	public double getLon(){
		return this.lon;
	}
	
	public String getPostalCode(){
		return this.postalCode;
	}
	
	public double getDistance(){
		return this.distance;
	}
	
	public boolean getIsShowFavourite(){
		return this.isShowFavourite;
	}
	
	public boolean getIsFavourited(){
		return this.isFavourited;
	}
	
	public boolean getIsHeader(){
		return this.isHeader;
	}
	
}

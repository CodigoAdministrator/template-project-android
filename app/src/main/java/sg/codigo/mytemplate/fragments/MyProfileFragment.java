package sg.codigo.mytemplate.fragments;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sg.codigo.mytemplate.MainActivity;
import sg.codigo.mytemplate.adapters.CountryArrayAdapter;
import sg.codigo.mytemplate.callbacks.JsonCallback;
import sg.codigo.mytemplate.callbacks.PopupCallback;
import sg.codigo.mytemplate.R;
import sg.codigo.mytemplate.dialogs.DialogChangePassword;
import sg.codigo.mytemplate.dialogs.DialogChoosePhoto;
import sg.codigo.mytemplate.dialogs.DialogOK;
import sg.codigo.mytemplate.maps.MyLocation;
import sg.codigo.mytemplate.maps.MyLocation.LocationResult;
import sg.codigo.mytemplate.models.Address;
import sg.codigo.mytemplate.models.Country;
import sg.codigo.mytemplate.storages.AppPreferences;
import sg.codigo.mytemplate.storages.DatabaseHandler;
import sg.codigo.mytemplate.utils.Constants;
import sg.codigo.mytemplate.utils.HTTPGetAsyncTask;
import sg.codigo.mytemplate.utils.HTTPPostAsyncTask;
import sg.codigo.mytemplate.utils.Utils;
import sg.codigo.mytemplate.views.CircularImageView;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.facebook.drawee.view.SimpleDraweeView;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Browser;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.view.inputmethod.EditorInfo;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabContentFactory;
import android.widget.TextView;


public class MyProfileFragment extends Fragment implements OnItemSelectedListener, OnClickListener, JsonCallback, PopupCallback{
	// data
	private static Context context;
	private AppPreferences appPrefs;
	private DatabaseHandler db;
	private int animationIndex = 1;
	private List<Country> countryList;
	private CountryArrayAdapter countryArrayAdapter;
	private ArrayList<NameValuePair> nameValuePairs;
	private JsonCallback jsonCallback;
	private PopupCallback popupCallback;
	private DialogOK dialogOK;
	private DialogOK dialogError;
	private DialogChangePassword dialogChangePassword;
	private Bitmap profilePhoto;
	private File mFileTemp;
	
	// layout
	private TextView tvName;
	private SimpleDraweeView ivProfileImage;
	private LinearLayout layoutProfileDetails;
	private ImageView icEmail;
	private TextView tvLabelEmail;
	private EditText etEmail;
	private ImageView icMobile;
	private TextView tvLabelMobile;
	private EditText etMobile;
	private Spinner spCountry;
	private TextView tvLabelCurrentlyLocated;
	private RelativeLayout layoutChangePassword;
	private Button btnChangePassword;
	
    public MyProfileFragment() {
        // Empty constructor required for fragment subclasses
    }

    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
    	
        //hide keybaord
    	getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    	
        Utils.updateActionBar(getActivity(), getString(R.string.title_profile), true, false, true, null);
        
        View rootView = inflater.inflate(R.layout.fragment_my_profile, container, false);
        //getActivity().setTitle(getString(R.string.title_merchants));
        
        context = getActivity();
        if(appPrefs == null){
    		appPrefs = new AppPreferences(getActivity());
    	}
    	
    	db = new DatabaseHandler(getActivity());
    	countryList = Utils.getCountryList();
    	jsonCallback = this;
    	popupCallback = this;
        nameValuePairs = new ArrayList<NameValuePair>();
        profilePhoto = BitmapFactory.decodeResource(context.getResources(), R.drawable.bg_profile_dummy);
        
    	tvName = (TextView) rootView.findViewById(R.id.tv_name);
    	tvName.setTypeface(Utils.getFontAvenirLight(getActivity()));
    	ivProfileImage = (SimpleDraweeView) rootView.findViewById(R.id.iv_profile_image);
    	layoutProfileDetails = (LinearLayout) rootView.findViewById(R.id.layout_profile_details);
    	icEmail = (ImageView)rootView.findViewById(R.id.ic_email);
    	tvLabelEmail = (TextView) rootView.findViewById(R.id.tv_label_email);
    	tvLabelEmail.setTypeface(Utils.getFontAvenirBlack(getActivity()));
    	etEmail = (EditText) rootView.findViewById(R.id.et_email);
    	etEmail.setTypeface(Utils.getFontAvenirLight(getActivity()));
    	icMobile = (ImageView)rootView.findViewById(R.id.ic_mobile);
    	tvLabelMobile = (TextView) rootView.findViewById(R.id.tv_label_mobile);
    	tvLabelMobile.setTypeface(Utils.getFontAvenirBlack(getActivity()));
    	etMobile = (EditText) rootView.findViewById(R.id.et_mobile);
    	etMobile.setTypeface(Utils.getFontAvenirLight(getActivity()));
    	
    	spCountry = (Spinner) rootView.findViewById(R.id.sp_country);
    	tvLabelCurrentlyLocated = (TextView) rootView.findViewById(R.id.tv_label_country);
    	tvLabelCurrentlyLocated.setTypeface(Utils.getFontAvenirBlack(getActivity()));
    	layoutChangePassword = (RelativeLayout) rootView.findViewById(R.id.layout_change_password);
    	btnChangePassword = (Button) rootView.findViewById(R.id.btn_change_password);
    	btnChangePassword.setTypeface(Utils.getFontAvenirLight(getActivity()));
    	
    	etEmail.setEnabled(false);
    	
    	if(appPrefs.getIsLoginThruFB()){
    		layoutChangePassword.setVisibility(View.GONE);
    	}else{
    		layoutChangePassword.setVisibility(View.VISIBLE);
    	}
    	
    	TextWatcher emailTextWatcher = new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if(tvLabelEmail.getVisibility()==View.GONE && s.length() > 0){
					YoYo.with(Techniques.FadeInDown)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	tvLabelEmail.setVisibility(View.VISIBLE);
                        	icEmail.setBackgroundResource(R.drawable.ic_email);
                        	etEmail.setHint(getString(R.string.hint_email));
                        	etEmail.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelEmail);
					
					
					
				}else if(tvLabelEmail.getVisibility()==View.VISIBLE && s.length() <= 0){
					YoYo.with(Techniques.FadeOutUp)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	icEmail.setBackgroundResource(R.drawable.ic_email);
                        	etEmail.setHint(getString(R.string.hint_email));
                        	etEmail.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	tvLabelEmail.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelEmail);
					
				}
			}
    		
    	};
    	
    	etEmail.addTextChangedListener(emailTextWatcher);
    	
    	TextWatcher mobileTextWatcher = new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if(tvLabelMobile.getVisibility()==View.GONE && s.length() > 0){
					YoYo.with(Techniques.FadeInDown)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	tvLabelMobile.setVisibility(View.VISIBLE);
                        	icMobile.setBackgroundResource(R.drawable.ic_mobile);
                        	etMobile.setHint(getString(R.string.hint_mobile));
                        	etMobile.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelMobile);
					
					
					
				}else if(tvLabelMobile.getVisibility()==View.VISIBLE && s.length() <= 0){
					YoYo.with(Techniques.FadeOutUp)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	icMobile.setBackgroundResource(R.drawable.ic_mobile);
                        	etMobile.setHint(getString(R.string.hint_mobile));
                        	etMobile.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	tvLabelMobile.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelMobile);
					
				}
			}
    		
    	};
    	
    	etMobile.addTextChangedListener(mobileTextWatcher);
    	etMobile.setOnEditorActionListener(
	        new EditText.OnEditorActionListener() {
	            @Override
	            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
	                if (actionId == EditorInfo.IME_ACTION_DONE ||
	                        event.getAction() == KeyEvent.ACTION_DOWN &&
	                        event.getKeyCode() == KeyEvent.KEYCODE_ENTER
	                        ){
	                    
	                	if(!etMobile.getText().toString().trim().equalsIgnoreCase("")){
							/*
	                		nameValuePairs.clear();
	                		nameValuePairs.add(new BasicNameValuePair("customer_token", appPrefs.getCustomerInfo().getToken()));
	                		nameValuePairs.add(new BasicNameValuePair("customer_name", appPrefs.getCustomerInfo().getName()));
	                		nameValuePairs.add(new BasicNameValuePair("customer_mobile", etMobile.getText().toString().trim()));
	                		nameValuePairs.add(new BasicNameValuePair("company_name", appPrefs.getCustomerInfo().getCompanyName()));
	            	    
	                		new HTTPPostAsyncTask(getActivity(), nameValuePairs, Constants.API_POST_UPDATE_PROFILE, jsonCallback, Constants.ID_API_POST_UPDATE_PROFILE, true, false);
	                		*/
	                	}else{
	                		
	                	}
	                    return true;
	                }
	                return false;
	            }
	        }
	    );
    	
    	ivProfileImage.setOnClickListener(this);
    
    	countryArrayAdapter = new CountryArrayAdapter(getActivity(), countryList);
    	spCountry.setAdapter(countryArrayAdapter);
    	spCountry.setOnItemSelectedListener(this);
    	spCountry.setEnabled(false);
    	
    	btnChangePassword.setOnClickListener(this);
    	
    	/*
    	nameValuePairs.clear();
		nameValuePairs.add(new BasicNameValuePair("customer_token", appPrefs.getCustomerInfo().getToken()));
	    
		String payload = Utils.getPayloadHttpGet(Constants.API_GET_PROFILE, nameValuePairs);
		
		new HTTPGetAsyncTask(getActivity(), payload, jsonCallback, Constants.ID_API_GET_PROFILE, true, false);
		*/


		tvName.setText("My name");
		etEmail.setText("My email");
		etMobile.setText("My mobile");

		MainActivity.ivProfileMenu.setVisibility(View.VISIBLE);

		ivProfileImage.setImageBitmap(profilePhoto);

		return rootView;
    }

    
    @Override 
    public void onDestroyView(){
    	super.onDestroyView();
    }
   
    @Override
    public void onResume(){
    	super.onResume();
    	
    	if(appPrefs == null){
    		appPrefs = new AppPreferences(getActivity());
    	}

		/*
    	//Constants.isShowActionEdit = false;
        Constants.isShowActionAdd = false;
        if(appPrefs.getIsLoggedIn()){
        	Constants.isShowActionLogout = true;
        }else{
        	Constants.isShowActionLogout = false;
        }
        */
    	
        Utils.updateGA(getActivity(), getString(R.string.ga_screen_my_profile));
		
    }
    
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) { 
    	super.onActivityResult(requestCode, resultCode, imageReturnedIntent); 
    	
    	if(Constants.DEBUG_MODE){
    		Log.d(Constants.TAG, "requestCode: " + requestCode);
    		Log.d(Constants.TAG, "resultCode: " + resultCode);
    		Log.d(Constants.TAG, "RESULT_OK: " + Activity.RESULT_OK);
    	}
    	
    	if(requestCode == Constants.ACTION_TAKE_PROFILE_PHOTO){
    		if(resultCode == Activity.RESULT_OK){  
    			//profilePhoto = (Bitmap) imageReturnedIntent.getExtras().get("data"); 
    			//if(profilePhoto!=null){
	    			
    				//ivProfileImage.setImageBitmap(profilePhoto);
    				//MainActivity.ivProfileMenu.setImageBitmap(profilePhoto);
    				
    				/*
	    			ByteArrayOutputStream baos = new ByteArrayOutputStream();  
	    			profilePhoto.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object   
	    			byte[] b = baos.toByteArray(); 
	    			String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
	    			*/
    				
    				/*
    				File myDir = new File(Environment.getExternalStorageDirectory() + "/fastfast");  
    				if(!myDir.exists()){
    					myDir.mkdirs();
    				}
        			File file = new File(myDir, "profile.jpeg");
        			FileOutputStream out;
        		    try
        		    {
        		    	out = new FileOutputStream(file);
        		    	profilePhoto.compress(Bitmap.CompressFormat.JPEG, 100, out);
        		    	out.flush();
        		    	out.close();
        		    	out = null;
        		    } catch (Exception e){
        		        e.printStackTrace();
        		    }
        		    */
    				
    				File myDir = Utils.createDir(getActivity());
	        	
    				String filePath = myDir.getAbsolutePath() + "/"+Constants.ACTION_TAKE_PROFILE_PHOTO+".jpg";
		            
    				BitmapFactory.Options options = new BitmapFactory.Options();
                	options.inSampleSize = Constants.IMAGE_SAMPLE_SIZE;
					options.inPreferredConfig = Bitmap.Config.ARGB_8888;
					profilePhoto = BitmapFactory.decodeFile(filePath, options);
					
        		    String encodedImage = Utils.encodeTobase64(profilePhoto);    //Base64.encodeToString(b, Base64.DEFAULT);

					/*
	    			nameValuePairs.clear();
	    			nameValuePairs.add(new BasicNameValuePair("customer_token", appPrefs.getCustomerInfo().getToken()));
	    			nameValuePairs.add(new BasicNameValuePair("customer_photo", encodedImage)); 
	    		    
	    			new HTTPPostAsyncTask(getActivity(), nameValuePairs, Constants.API_POST_UPDATE_PROFILE_PICTURE, jsonCallback, Constants.ID_API_POST_UPDATE_PROFILE_PICTURE, true, false);
	    	    	*/

    			//}
    			
    			
    		}
    	}else if(requestCode == Constants.ACTION_ATTACH_PROFILE_PHOTO){
    	    if(resultCode == Activity.RESULT_OK){  
    	    	try {
    	    		
    	    		//mFileTemp = Utils.createTempFile(getActivity());
    	        	
    	    		InputStream inputStream = getActivity().getContentResolver().openInputStream(imageReturnedIntent.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                    Utils.copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();

                    
    	    		/*
    	    		BitmapFactory.Options options = new BitmapFactory.Options(); 
     	    		options.inSampleSize = 4; 
     	    		AssetFileDescriptor fileDescriptor =null; 
     	    		
     	    		fileDescriptor = getActivity().getContentResolver().openAssetFileDescriptor(imageReturnedIntent.getData(),"r"); 
     	    		profilePhoto = BitmapFactory.decodeFileDescriptor(fileDescriptor.getFileDescriptor(), null, options); 
     	    		fileDescriptor.close(); 
     	    		
    	    		//profilePhoto = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageReturnedIntent.getData());
    	    		if(profilePhoto!=null){
	    	    	*/	
    	    			//ivProfileImage.setImageBitmap(profilePhoto);
        				//MainActivity.ivProfileMenu.setImageBitmap(profilePhoto);
        				
    	    			/*
	    	    		ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		    			profilePhoto.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object   
		    			byte[] b = baos.toByteArray(); 
		    			String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
		    			*/
    	    			/*
    	    			File myDir = new File(Environment.getExternalStorageDirectory() + "/fastfast");  
        				if(!myDir.exists()){
        					myDir.mkdirs();
        				}
            			File file = new File(myDir, "profile.jpeg");
            			
            			FileOutputStream out;
            		    try
            		    {
            		    	out = new FileOutputStream(file);
            		    	profilePhoto.compress(Bitmap.CompressFormat.JPEG, 100, out);
            		    	out.flush();
            		    	out.close();
            		    	out = null;
            		    } catch (Exception e){
            		        e.printStackTrace();
            		    }
            		    */
                    if(mFileTemp!=null){
                    	String filePath = mFileTemp.getAbsolutePath();
                    
                    	BitmapFactory.Options options = new BitmapFactory.Options();
                    	options.inSampleSize = Constants.IMAGE_SAMPLE_SIZE;
    					options.inPreferredConfig = Bitmap.Config.ARGB_8888;
    					profilePhoto = BitmapFactory.decodeFile(filePath, options);
    					
    	    			if(profilePhoto!=null){// && !filePath.equalsIgnoreCase("")){
	            		    String encodedImage = Utils.encodeTobase64(profilePhoto);    //Base64.encodeToString(b, Base64.DEFAULT);

							/*
			    			nameValuePairs.clear();
			    			nameValuePairs.add(new BasicNameValuePair("customer_token", appPrefs.getCustomerInfo().getToken()));
			    			nameValuePairs.add(new BasicNameValuePair("customer_photo", encodedImage));
			    		    
			    			new HTTPPostAsyncTask(getActivity(), nameValuePairs, Constants.API_POST_UPDATE_PROFILE_PICTURE, jsonCallback, Constants.ID_API_POST_UPDATE_PROFILE_PICTURE, true, false);
    	    				*/
    	    			}
                    }
    	    			
    	    			
    	    		//}
					
				} 
    	    	/*
    	    	catch (FileNotFoundException e) {
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "FileNotFoundException: " + e.toString());
					}
				}
				*/
				catch (IOException e) {
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "IOException: " + e.toString());
					}
				}
				 
    	    	catch (Exception e) {
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "Exception: " + e.toString());
					}
				}
    	    	
    	        
    	    }
    	} 
    }
    
    @Override
    public void onClick(View v){
    	if(v == ivProfileImage){
    		DialogChoosePhoto dialogChoosePhoto = new DialogChoosePhoto(getActivity(), Constants.POPUP_ACTION_CHOOSE_PROFILE_PHOTO, popupCallback);
    		dialogChoosePhoto.show();
    	}else if(v == btnChangePassword){
    		if(dialogChangePassword == null || !dialogChangePassword.isShowing()){
    			dialogChangePassword = new DialogChangePassword(getActivity(), getString(R.string.dialog_title_change_password), getString(R.string.btn_cancel), getString(R.string.btn_change), Constants.POPUP_ACTION_CHANGE_PASSWORD, popupCallback, null);
    			dialogChangePassword.show();
    		}
    	}
    }

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void callbackJson(String result, int processID, int index) {
		if(processID == Constants.ID_API_GET_PROFILE){
			if(result!=null && !result.equalsIgnoreCase("")){
				try{
					JSONObject jsonObj = new JSONObject(result);
					
					int errorCode = jsonObj.getInt("errorCode");
					String message = jsonObj.getString("message");
					
					if(errorCode == Constants.ERROR_CODE_SUCCESS){
						/*
						if(jsonObj.has("customer_info") && jsonObj.getString("customer_info").startsWith("[")){
							JSONArray customerArray = jsonObj.getJSONArray("customer_info");
							for(int i = 0; i < customerArray.length(); i++){
								JSONObject customerObj = customerArray.getJSONObject(i);
								Customer customer = new Customer();
								customer.setToken((customerObj.has("customer_token") && customerObj.getString("customer_token")!=null)?customerObj.getString("customer_token"):"");
								customer.setName((customerObj.has("customer_name") && customerObj.getString("customer_name")!=null)?customerObj.getString("customer_name"):"");
								customer.setCode((customerObj.has("customer_code") && customerObj.getString("customer_code")!=null)?customerObj.getString("customer_code"):"");
								customer.setMobile((customerObj.has("customer_mobile") && customerObj.getString("customer_mobile")!=null)?customerObj.getString("customer_mobile"):"");
								customer.setEmail((customerObj.has("customer_email") && customerObj.getString("customer_email")!=null)?customerObj.getString("customer_email"):"");
								customer.setCompanyName((customerObj.has("company_name") && customerObj.getString("company_name")!=null)?customerObj.getString("company_name"):"");
								customer.setRegDate((customerObj.has("reg_date") && customerObj.getString("reg_date")!=null)?customerObj.getString("reg_date"):"");
								customer.setPhotoUrl((customerObj.has("customer_photo") && customerObj.getString("customer_photo")!=null)?customerObj.getString("customer_photo"):"");
								customer.setMemberType((customerObj.has("member_type") && customerObj.getString("member_type")!=null)?customerObj.getString("member_type"):"");
								customer.setPaymentType((customerObj.has("payment_type") && customerObj.getString("payment_type")!=null)?customerObj.getString("payment_type"):"");
								
								appPrefs.clearCustomerInfo();
								appPrefs.setCustomerInfo(customer);
							}
							
							appPrefs.setIsLoggedIn(true);
						}else if(jsonObj.has("customer_info") && jsonObj.getString("customer_info").startsWith("{")){
							JSONObject customerObj = jsonObj.getJSONObject("customer_info");
							Customer customer = new Customer();
							customer.setToken((customerObj.has("customer_token") && customerObj.getString("customer_token")!=null)?customerObj.getString("customer_token"):"");
							customer.setName((customerObj.has("customer_name") && customerObj.getString("customer_name")!=null)?customerObj.getString("customer_name"):"");
							customer.setCode((customerObj.has("customer_code") && customerObj.getString("customer_code")!=null)?customerObj.getString("customer_code"):"");
							customer.setMobile((customerObj.has("customer_mobile") && customerObj.getString("customer_mobile")!=null)?customerObj.getString("customer_mobile"):"");
							customer.setEmail((customerObj.has("customer_email") && customerObj.getString("customer_email")!=null)?customerObj.getString("customer_email"):"");
							customer.setCompanyName((customerObj.has("company_name") && customerObj.getString("company_name")!=null)?customerObj.getString("company_name"):"");
							customer.setRegDate((customerObj.has("reg_date") && customerObj.getString("reg_date")!=null)?customerObj.getString("reg_date"):"");
							customer.setPhotoUrl((customerObj.has("customer_photo") && customerObj.getString("customer_photo")!=null)?customerObj.getString("customer_photo"):"");
							customer.setMemberType((customerObj.has("member_type") && customerObj.getString("member_type")!=null)?customerObj.getString("member_type"):"");
							customer.setPaymentType((customerObj.has("payment_type") && customerObj.getString("payment_type")!=null)?customerObj.getString("payment_type"):"");
							
							appPrefs.clearCustomerInfo();
							appPrefs.setCustomerInfo(customer);
						
							appPrefs.setIsLoggedIn(true);
						}else{
							if(dialogError==null || !dialogError.isShowing()){
								dialogError = new DialogOK(getActivity(), getString(R.string.dialog_title_error), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_FAILED, popupCallback);
								dialogError.show();
							}
						}
						*/
					}else{
						if(dialogError==null || !dialogError.isShowing()){
							dialogError = new DialogOK(getActivity(), getString(R.string.dialog_title_error), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_FAILED, popupCallback);
							dialogError.show();
						}
					}
				}catch(JSONException e){
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "JSONException: " + e.toString());
					}
					
				}catch(Exception e){
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "Exception: " + e.toString());
					}
				}
			}
			

			//if(appPrefs.getIsLoggedIn()){
				tvName.setText("My name");
				etEmail.setText("My email");
	    		etMobile.setText("My mobile");
	    	
	    		MainActivity.ivProfileMenu.setVisibility(View.VISIBLE);
	        	/*
	    		if(!appPrefs.getCustomerInfo().getPhotoUrl().equalsIgnoreCase("")){

	    			Uri uri = Uri.parse(appPrefs.getCustomerInfo().getPhotoUrl());
	    			ivProfileImage.setImageURI(uri);
	    			
	    			Uri uri2 = Uri.parse(appPrefs.getCustomerInfo().getPhotoUrl());
	        		MainActivity.ivProfileMenu.setImageURI(uri2);
	    			
	    			
				}else{
				*/
					/*
					Uri uri = Utils.getImageUri(getActivity(), profilePhoto);
					ivProfileImage.setImageURI(uri);
	    			*/
	    			ivProfileImage.setImageBitmap(profilePhoto);
					
					//MainActivity.ivProfileMenu.setVisibility(View.GONE);
				//}
		    	

			//}
			
	    	
	    	YoYo.with(Techniques.ZoomInUp)
			.duration(Constants.animationTime * (animationIndex + 5))
			.withListener(new AnimatorListener(){

				@Override
				public void onAnimationCancel(Animator arg0) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onAnimationEnd(Animator arg0) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onAnimationRepeat(Animator arg0) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onAnimationStart(Animator arg0) {
					ivProfileImage.setVisibility(View.VISIBLE);
				}
			})
			.playOn(ivProfileImage);
	    	

	    	YoYo.with(Techniques.FadeInUp)
			.duration(Constants.animationTime * (animationIndex + 5))
			.withListener(new AnimatorListener(){

				@Override
				public void onAnimationCancel(Animator arg0) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onAnimationEnd(Animator arg0) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onAnimationRepeat(Animator arg0) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onAnimationStart(Animator arg0) {
					tvName.setVisibility(View.VISIBLE);
				}
			})
			.playOn(tvName);
	    	
	    	YoYo.with(Techniques.FadeIn)
			.duration(Constants.animationTime * animationIndex)
			.playOn(layoutProfileDetails);
	    	
		}
	}


	@Override
	public void jsonError(String msg, int processID) {
		if(processID == Constants.ID_API_GET_PROFILE){
			if(Constants.DEBUG_MODE){
				Log.i(Constants.TAG, "jsonError: " + msg);
			}
			if(dialogError==null || !dialogError.isShowing()){
				dialogError = new DialogOK(getActivity(), getString(R.string.dialog_title_error), msg, getString(R.string.btn_ok), Constants.POPUP_API_CALL_FAILED, popupCallback);
				dialogError.show();
			}
		}
	}


	@Override
	public void callBackPopup(Object data, int processID, int index) {
		if(processID == Constants.POPUP_ACTION_CHANGE_PASSWORD){
			if(index == Constants.DIALOG_BUTTON_RIGHT){
				String[] passwords = (String[]) data;
				if(passwords!=null && passwords.length >= 2){
					/*
					nameValuePairs.clear();
	    			nameValuePairs.add(new BasicNameValuePair("customer_token", appPrefs.getCustomerInfo().getToken()));
	    			nameValuePairs.add(new BasicNameValuePair("current_password", passwords[0]));
	    			nameValuePairs.add(new BasicNameValuePair("new_password", passwords[1]));
	    		    
	    			new HTTPPostAsyncTask(getActivity(), nameValuePairs, Constants.API_POST_UPDATE_PASSWORD, jsonCallback, Constants.ID_API_POST_UPDATE_PASSWORD, true, false);
	    			*/
				}
			}
		}else if(processID == Constants.POPUP_ACTION_CHOOSE_PROFILE_PHOTO){
			if(index == Constants.DIALOG_BUTTON_TAKE_PHOTO){
				File myDir = Utils.createDir(getActivity());
	        	
    			File file = new File(myDir, Constants.ACTION_TAKE_PROFILE_PHOTO+".jpg");
    			
	            Uri _fileUri = Uri.fromFile(file);
	            Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	    		//takePicture.putExtra("android.intent.extras.CAMERA_FACING", 1);
	    		takePicture.putExtra(MediaStore.EXTRA_OUTPUT, _fileUri);
	    		startActivityForResult(takePicture, Constants.ACTION_TAKE_PROFILE_PHOTO);//zero can be replaced with any action code
	    		
		          
	    		
			}else if(index == Constants.DIALOG_BUTTON_ATTACH_PHOTO){
				
				mFileTemp = Utils.createTempFile(getActivity());
	        	/*
    			File file = new File(mFileTemp, Constants.ACTION_ATTACH_PROFILE_PHOTO+".jpg");
    			
	            Uri _fileUri = Uri.fromFile(file);
	            */
	            Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				
				//pickPhoto.putExtra(MediaStore.EXTRA_OUTPUT, _fileUri);
	    		startActivityForResult(pickPhoto , Constants.ACTION_ATTACH_PROFILE_PHOTO);//one can be replaced with any action code
	        
			}
		}else if(processID == Constants.POPUP_API_CALL_SUCCEED){
			
		}
		else if(processID == Constants.POPUP_API_CALL_FAILED){
			
		}
		
	}
}

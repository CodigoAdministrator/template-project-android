package sg.codigo.mytemplate.fragments;


import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sg.codigo.mytemplate.MainActivity;
import sg.codigo.mytemplate.callbacks.JsonCallback;
import sg.codigo.mytemplate.callbacks.PopupCallback;
import sg.codigo.mytemplate.R;
import sg.codigo.mytemplate.dialogs.DialogOK;
import sg.codigo.mytemplate.maps.MyLocation;
import sg.codigo.mytemplate.maps.MyLocation.LocationResult;
import sg.codigo.mytemplate.storages.AppPreferences;
import sg.codigo.mytemplate.storages.DatabaseHandler;
import sg.codigo.mytemplate.utils.Constants;
import sg.codigo.mytemplate.utils.HTTPGetAsyncTask;
import sg.codigo.mytemplate.utils.HTTPPostAsyncTask;
import sg.codigo.mytemplate.utils.Utils;
import sg.codigo.mytemplate.views.CircularImageView;
import sg.codigo.mytemplate.views.InteractiveScrollView;
import sg.codigo.mytemplate.views.InteractiveScrollView.OnBottomReachedListener;
import sg.codigo.mytemplate.views.InteractiveScrollView.OnScrollDownListener;
import sg.codigo.mytemplate.views.InteractiveScrollView.OnScrollUpListener;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;
import com.squareup.picasso.Picasso;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabContentFactory;
import android.widget.TextView;


public class FeedbackFragment extends Fragment implements OnCheckedChangeListener, OnClickListener, JsonCallback, PopupCallback{
	// data
	private static Context context;
	private AppPreferences appPrefs;
	private DatabaseHandler db;
	private int animationIndex = 1;
	private ArrayList<NameValuePair> nameValuePairs;
	private JsonCallback jsonCallback;
	private PopupCallback popupCallback;
	private DialogOK dialogOK;
	private DialogOK dialogError;
	private boolean lockSlideUp = false;
	private boolean lockSlideDown = false;
	
	// layout
	private RelativeLayout layoutContent;
	private TextView tvFeedbackNote;
	private ImageView icName;
	private TextView tvLabelName;
	private EditText etName;
	private ImageView icEmail;
	private TextView tvLabelEmail;
	private EditText etEmail;
	private CheckBox cbFeedbackType;
	
	private RelativeLayout layoutComments;
	private ImageView icComments;
	private TextView tvLabelComments;
	private EditText etComments;
	
	private RelativeLayout layoutBtnSubmit;
	private Button btnSubmit;
	
    public FeedbackFragment() {
        // Empty constructor required for fragment subclasses
    }

    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
    	
        //hide keybaord
    	getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    	
        Utils.updateActionBar(getActivity(), getString(R.string.title_feedback), true, false, true, null);
        
        View rootView = inflater.inflate(R.layout.fragment_feedback, container, false);
        //getActivity().setTitle(getString(R.string.title_merchants));
        
        context = getActivity();
        if(appPrefs == null){
    		appPrefs = new AppPreferences(getActivity());
    	}
    	
    	db = new DatabaseHandler(getActivity());
    	jsonCallback = this;
    	popupCallback = this;
        nameValuePairs = new ArrayList<NameValuePair>();
         
        layoutContent = (RelativeLayout)rootView.findViewById(R.id.layout_content);
        tvFeedbackNote = (TextView) rootView.findViewById(R.id.tv_feedback_note);
        tvFeedbackNote.setTypeface(Utils.getFontAvenirLight(getActivity()));
        icName = (ImageView)rootView.findViewById(R.id.ic_name);
    	tvLabelName = (TextView) rootView.findViewById(R.id.tv_label_name);
    	tvLabelName.setTypeface(Utils.getFontAvenirBlack(getActivity()));
    	etName = (EditText) rootView.findViewById(R.id.et_name);
    	etName.setTypeface(Utils.getFontAvenirLight(getActivity()));
    	
    	icEmail = (ImageView)rootView.findViewById(R.id.ic_email);
    	tvLabelEmail = (TextView) rootView.findViewById(R.id.tv_label_email);
    	tvLabelEmail.setTypeface(Utils.getFontAvenirBlack(getActivity()));
    	etEmail = (EditText) rootView.findViewById(R.id.et_email);
    	etEmail.setTypeface(Utils.getFontAvenirLight(getActivity()));
    	
    	cbFeedbackType = (CheckBox)rootView.findViewById(R.id.cb_feedback_type);
    	
    	layoutComments = (RelativeLayout)rootView.findViewById(R.id.layout_comments);
    	icComments = (ImageView)rootView.findViewById(R.id.ic_comments);
    	tvLabelComments = (TextView) rootView.findViewById(R.id.tv_label_comments);
    	tvLabelComments.setTypeface(Utils.getFontAvenirBlack(getActivity()));
    	etComments = (EditText) rootView.findViewById(R.id.et_comments);
    	etComments.setTypeface(Utils.getFontAvenirLight(getActivity()));
    	
    	layoutBtnSubmit = (RelativeLayout)rootView.findViewById(R.id.layout_btn_submit);
    	btnSubmit = (Button)rootView.findViewById(R.id.btn_submit);
    	btnSubmit.setTypeface(Utils.getFontAvenirBook(getActivity()));
    	btnSubmit.setOnClickListener(this);
    	layoutBtnSubmit.setVisibility(View.VISIBLE);
    	
    	/*
    	svLayoutContent.setOnBottomReachedListener(new OnBottomReachedListener(){

			@Override
			public void onBottomReached() {
				if(layoutBtnSubmit.getVisibility() == View.GONE && !lockSlideUp){
					lockSlideUp = true;
					layoutBtnSubmit.setVisibility(View.VISIBLE);
			    	YoYo.with(Techniques.SlideInUp)
			    	.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	lockSlideUp = false;
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.duration(Constants.animationTime * animationIndex)
		    		.playOn(layoutBtnSubmit);
				}
			}
    		
    	});
    	*/
    	
    	/*
    	svLayoutContent.setOnScrollUpListener(new OnScrollUpListener(){

			@Override
			public void onScrollUp() {
				if(layoutBtnSubmit.getVisibility() == View.VISIBLE && !lockSlideDown){
					lockSlideDown = true;
					YoYo.with(Techniques.SlideOutDown)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	layoutBtnSubmit.setVisibility(View.GONE);
                        	lockSlideDown = false;
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.duration(Constants.animationTime * animationIndex)
		    		.playOn(layoutBtnSubmit)
		    		;
				}
			}
    		
    	});
    	
    	svLayoutContent.setOnScrollDownListener(new OnScrollDownListener(){

			@Override
			public void onScrollDown() {
				if(layoutBtnSubmit.getVisibility() == View.GONE && !lockSlideUp){
					lockSlideUp = true;
					layoutBtnSubmit.setVisibility(View.VISIBLE);
			    	YoYo.with(Techniques.SlideInUp)
			    	.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	lockSlideUp = false;
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.duration(Constants.animationTime * animationIndex)
		    		.playOn(layoutBtnSubmit);
				}
			}
    		
    	});
    	*/
    	
    	TextWatcher nameTextWatcher = new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if(tvLabelName.getVisibility()==View.GONE && s.length() > 0){
					YoYo.with(Techniques.FadeInDown)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	tvLabelName.setVisibility(View.VISIBLE);
                        	icName.setBackgroundResource(R.drawable.ic_name);
                        	etName.setHint(getString(R.string.hint_name));
                        	etName.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelName);
					
					
					
				}else if(tvLabelName.getVisibility()==View.VISIBLE && s.length() <= 0){
					YoYo.with(Techniques.FadeOutUp)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	icName.setBackgroundResource(R.drawable.ic_name);
                        	etName.setHint(getString(R.string.hint_name));
                        	etName.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	tvLabelName.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelName);
					
				}
			}
    		
    	};
    	
    	etName.addTextChangedListener(nameTextWatcher);
    	
    	TextWatcher emailTextWatcher = new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if(tvLabelEmail.getVisibility()==View.GONE && s.length() > 0){
					YoYo.with(Techniques.FadeInDown)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	tvLabelEmail.setVisibility(View.VISIBLE);
                        	icEmail.setBackgroundResource(R.drawable.ic_email);
                        	etEmail.setHint(getString(R.string.hint_email));
                        	etEmail.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelEmail);
					
					
					
				}else if(tvLabelEmail.getVisibility()==View.VISIBLE && s.length() <= 0){
					YoYo.with(Techniques.FadeOutUp)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	icEmail.setBackgroundResource(R.drawable.ic_email);
                        	etEmail.setHint(getString(R.string.hint_email));
                        	etEmail.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	tvLabelEmail.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelEmail);
					
				}
			}
    		
    	};
    	
    	etEmail.addTextChangedListener(emailTextWatcher);
    	
    	cbFeedbackType.setOnClickListener(this);
    	cbFeedbackType.setOnCheckedChangeListener(this);
    	
    	TextWatcher commentsTextWatcher = new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if(tvLabelComments.getVisibility()==View.GONE && s.length() > 0){
					YoYo.with(Techniques.FadeInDown)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	tvLabelComments.setVisibility(View.VISIBLE);
                        	icComments.setBackgroundResource(R.drawable.ic_comment_02);
                        	etComments.setHint(getString(R.string.hint_comments));
                        	etComments.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelComments);
					
					
					
				}else if(tvLabelComments.getVisibility()==View.VISIBLE && s.length() <= 0){
					YoYo.with(Techniques.FadeOutUp)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	icComments.setBackgroundResource(R.drawable.ic_comment_02);
                        	etComments.setHint(getString(R.string.hint_comments));
                        	etComments.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	tvLabelComments.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelComments);
					
				}
			}
    		
    	};
    	
    	etComments.addTextChangedListener(commentsTextWatcher);
    	

    	if(appPrefs.getIsLoggedIn()){ // && appPrefs.getCustomerInfo()!=null){
    		//etName.setText(appPrefs.getCustomerInfo().getName());
    		//etEmail.setText(appPrefs.getCustomerInfo().getEmail());
    		etComments.requestFocus();
    	}else{
    		etName.requestFocus();
    	}
    	
    	YoYo.with(Techniques.FadeIn)
		.duration(Constants.animationTime * animationIndex)
		.playOn(layoutContent);
    	
    	etComments.requestFocus();
        return rootView;
    }

    
    @Override 
    public void onDestroyView(){
    	super.onDestroyView();
    }
   
    @Override
    public void onResume(){
    	super.onResume();
    	Utils.updateGA(getActivity(), getString(R.string.ga_screen_feedback));
		
    }
    
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) { 
    	super.onActivityResult(requestCode, resultCode, imageReturnedIntent); 
    	
    }
    
    @Override
    public void onClick(View v){
    	Utils.hideSoftKeyboard(getActivity());
    	if(v == btnSubmit){
    		boolean hasError = false;
    		View viewHighlighted = null;
    		
    		if(etName.getText().toString().trim().equalsIgnoreCase("")){
    			hasError = true;
    			etName.setText("");
    			etName.setHint(getString(R.string.error_message_name));
    			etName.setHintTextColor(getResources().getColor(R.color.error_red));
    			icName.setBackgroundResource(R.drawable.ic_name_error);
    			YoYo.with(Techniques.Shake)
    				.duration(Constants.animationTime)
    				.playOn(icName);
    			
    			if(viewHighlighted == null){
    				viewHighlighted = etName;
    			}
    			//DialogOK dialogOK = new DialogOK(RegisterWithEmailActivity.this, getString(R.string.dialog_title_alert), getString(R.string.error_message_name), getString(R.string.btn_ok), Constants.POPUP_INVALID_NAME, popupCallback);
    			//dialogOK.show();
    		}
    		
    		if(etEmail.getText().toString().trim().equalsIgnoreCase("")){
    			hasError = true;
    			etEmail.setText("");
    			etEmail.setHint(getString(R.string.error_message_email_01));
    			etEmail.setHintTextColor(getResources().getColor(R.color.error_red));
    			icEmail.setBackgroundResource(R.drawable.ic_email_error);
    			YoYo.with(Techniques.Shake)
    				.duration(Constants.animationTime)
    				.playOn(icEmail);
    			
    			if(viewHighlighted == null){
    				viewHighlighted = etEmail;
    			}
    			
    			//DialogOK dialogOK = new DialogOK(RegisterWithEmailActivity.this, getString(R.string.dialog_title_alert), getString(R.string.error_message_email_01), getString(R.string.btn_ok), Constants.POPUP_INVALID_EMAIL, popupCallback);
    			//dialogOK.show();
    		}
    		else if(!Utils.emailValidation(etEmail.getText().toString().trim())){
    			hasError = true;
    			etEmail.setText("");
    			etEmail.setHint(getString(R.string.error_message_email_02));
    			etEmail.setHintTextColor(getResources().getColor(R.color.error_red));
    			icEmail.setBackgroundResource(R.drawable.ic_email_error);
    			YoYo.with(Techniques.Shake)
    				.duration(Constants.animationTime)
    				.playOn(icEmail);
    			
    			if(viewHighlighted == null){
    				viewHighlighted = etEmail;
    			}
    			//DialogOK dialogOK = new DialogOK(RegisterWithEmailActivity.this, getString(R.string.dialog_title_alert), getString(R.string.error_message_email_02), getString(R.string.btn_ok), Constants.POPUP_INVALID_EMAIL, popupCallback);
    			//dialogOK.show();
    		}
    		
    		if(etComments.getText().toString().trim().equalsIgnoreCase("")){
    			hasError = true;
    			etComments.setText("");
    			etComments.setHint(getString(R.string.error_message_feedback_comments));
    			etComments.setHintTextColor(getResources().getColor(R.color.error_red));
    			icComments.setBackgroundResource(R.drawable.ic_comment_02_error);
    			YoYo.with(Techniques.Shake)
    				.duration(Constants.animationTime)
    				.playOn(icComments);
    			
    			if(viewHighlighted == null){
    				viewHighlighted = etComments;
    			}
    			
    			//DialogOK dialogOK = new DialogOK(RegisterWithEmailActivity.this, getString(R.string.dialog_title_alert), getString(R.string.error_message_email_01), getString(R.string.btn_ok), Constants.POPUP_INVALID_EMAIL, popupCallback);
    			//dialogOK.show();
    		}
    		
    		if(!hasError){
    			nameValuePairs.clear();
    			
    			nameValuePairs.add(new BasicNameValuePair("name", etName.getText().toString().trim()));
    	    	nameValuePairs.add(new BasicNameValuePair("email", etEmail.getText().toString().trim()));
    	    	nameValuePairs.add(new BasicNameValuePair("type", cbFeedbackType.isChecked()?"report issue":"general"));
    	    	nameValuePairs.add(new BasicNameValuePair("content", etComments.getText().toString().trim()));
    	    	
    			new HTTPPostAsyncTask(getActivity(), nameValuePairs, Constants.API_POST_SEND_FEEDBACK, jsonCallback, Constants.ID_API_POST_SEND_FEEDBACK, true, false);
    	    	
    		}else{
    			if(viewHighlighted != null){
    				viewHighlighted.requestFocus();
    			}
    		}
    	}
    	else if(v == cbFeedbackType){
    		
    	}
    }

	@Override
	public void callbackJson(String result, int processID, int index) {
		if(processID == Constants.ID_API_POST_SEND_FEEDBACK){
			if(result!=null && !result.equalsIgnoreCase("")){
				try{
					JSONObject jsonObj = new JSONObject(result);
					
					int errorCode = jsonObj.getInt("errorCode");
					String message = jsonObj.getString("message");
					
					if(errorCode == Constants.ERROR_CODE_SUCCESS){
						if(dialogOK==null || !dialogOK.isShowing()){
							dialogOK = new DialogOK(getActivity(), getString(R.string.dialog_title_success), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_SUCCEED, popupCallback);
							dialogOK.show();
						}
					}else{
						if(dialogError==null || !dialogError.isShowing()){
							dialogError = new DialogOK(getActivity(), getString(R.string.dialog_title_error), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_FAILED, popupCallback);
							dialogError.show();
						}
					}
				}catch(JSONException e){
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "JSONException: " + e.toString());
					}
					
				}catch(Exception e){
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "Exception: " + e.toString());
					}
				}
			}
			
		}
	}


	@Override
	public void jsonError(String msg, int processID) {
		if(processID == Constants.ID_API_POST_SEND_FEEDBACK){
			if(Constants.DEBUG_MODE){
				Log.i(Constants.TAG, "jsonError: " + msg);
			}
			if(dialogError==null || !dialogError.isShowing()){
				dialogError = new DialogOK(getActivity(), getString(R.string.dialog_title_error), msg, getString(R.string.btn_ok), Constants.POPUP_API_CALL_FAILED, popupCallback);
				dialogError.show();
			}
		}
	}


	@Override
	public void callBackPopup(Object data, int processID, int index) {
		if(processID == Constants.POPUP_API_CALL_SUCCEED){
			
			//etName.setText("");
			//etEmail.setText("");
			etComments.setText("");
			
		}
		else if(processID == Constants.POPUP_API_CALL_FAILED){
			
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if(buttonView == cbFeedbackType){
			
			YoYo.with(Techniques.FadeIn)
			.duration(Constants.animationTime)
			.playOn(layoutComments);
			
			if(isChecked){
				
			}else{
				
			}
		}
	}
}

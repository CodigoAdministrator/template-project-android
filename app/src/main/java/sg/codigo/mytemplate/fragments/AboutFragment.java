package sg.codigo.mytemplate.fragments;


import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sg.codigo.mytemplate.MainActivity;
import sg.codigo.mytemplate.R;
import sg.codigo.mytemplate.callbacks.JsonCallback;
import sg.codigo.mytemplate.callbacks.PopupCallback;
import sg.codigo.mytemplate.dialogs.DialogOK;
import sg.codigo.mytemplate.maps.MyLocation;
import sg.codigo.mytemplate.maps.MyLocation.LocationResult;
import sg.codigo.mytemplate.storages.AppPreferences;
import sg.codigo.mytemplate.storages.DatabaseHandler;
import sg.codigo.mytemplate.utils.Constants;
import sg.codigo.mytemplate.utils.HTTPGetAsyncTask;
import sg.codigo.mytemplate.utils.HTTPPostAsyncTask;
import sg.codigo.mytemplate.utils.Utils;
import sg.codigo.mytemplate.views.CircularImageView;
import sg.codigo.mytemplate.views.InteractiveScrollView;
import sg.codigo.mytemplate.views.InteractiveScrollView.OnBottomReachedListener;
import sg.codigo.mytemplate.views.InteractiveScrollView.OnScrollDownListener;
import sg.codigo.mytemplate.views.InteractiveScrollView.OnScrollUpListener;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;
import com.squareup.picasso.Picasso;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabContentFactory;
import android.widget.TextView;


public class AboutFragment extends Fragment implements OnClickListener, JsonCallback, PopupCallback{
	// data
	private static Context context;
	private AppPreferences appPrefs;
	private DatabaseHandler db;
	private int animationIndex = 1;
	private ArrayList<NameValuePair> nameValuePairs;
	private JsonCallback jsonCallback;
	private PopupCallback popupCallback;
	private DialogOK dialogOK;
	private DialogOK dialogError;
	private static String versionNumber = "1.0.0";
	private LayoutInflater mLayoutInflater;
	
	// layout
	private RelativeLayout layoutLogo;
	private ImageView ivLogo;
	private TextView tvVersion;
	
	private RelativeLayout layoutContent;
	private RelativeLayout layoutAddress;
	private TextView tvLabelAddress;
	private TextView tvAddress;
	private RelativeLayout layoutTelephone;
	private TextView tvLabelTelephone;
	private TextView tvTelephone;
	private RelativeLayout layoutEmail;
	private TextView tvLabelEmail;
	private TextView tvEmail;
	private RelativeLayout layoutWebsite;
	private TextView tvLabelWebsite;
	private TextView tvWebsite;
	
	private View mPlaceholderView;
	
    public AboutFragment() {
        // Empty constructor required for fragment subclasses
    }

    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
    	
        PackageInfo pInfo;
		try {
			pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
			versionNumber = pInfo.versionName;
			
		} catch (NameNotFoundException e) {
			if(Constants.DEBUG_MODE){
				Log.e(Constants.TAG, "NameNotFoundException: " + e.toString());
			}
		} catch (Exception e) {
			if(Constants.DEBUG_MODE){
				Log.e(Constants.TAG, "Exception: " + e.toString());
			}
		}
		
        //hide keybaord
    	getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    	
        Utils.updateActionBar(getActivity(), getString(R.string.title_about), true, false, true, null);
        
        
        View rootView = inflater.inflate(R.layout.fragment_about, container, false);
        //getActivity().setTitle(getString(R.string.title_merchants));
        
        context = getActivity();
        if(appPrefs == null){
    		appPrefs = new AppPreferences(getActivity());
    	}
    	
    	db = new DatabaseHandler(getActivity());
    	jsonCallback = this;
    	popupCallback = this;
        nameValuePairs = new ArrayList<NameValuePair>();
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	
        layoutLogo = (RelativeLayout)rootView.findViewById(R.id.layout_logo);
    	ivLogo = (ImageView)rootView.findViewById(R.id.iv_logo);
    	tvVersion = (TextView)rootView.findViewById(R.id.tv_version);
    	tvVersion.setTypeface(Utils.getFontAvenirLight(getActivity()));
    	
    	
        layoutContent = (RelativeLayout)rootView.findViewById(R.id.layout_content);
        layoutAddress = (RelativeLayout)rootView.findViewById(R.id.layout_address);
        tvLabelAddress = (TextView)rootView.findViewById(R.id.tv_label_address);
        tvLabelAddress.setTypeface(Utils.getFontAvenirHeavy(getActivity()));
        tvAddress = (TextView)rootView.findViewById(R.id.tv_address);
        tvAddress.setTypeface(Utils.getFontAvenirLight(getActivity()));
        layoutTelephone = (RelativeLayout)rootView.findViewById(R.id.layout_telephone);
        tvLabelTelephone = (TextView)rootView.findViewById(R.id.tv_label_telephone);
        tvLabelTelephone.setTypeface(Utils.getFontAvenirHeavy(getActivity()));
        tvTelephone = (TextView)rootView.findViewById(R.id.tv_telephone);
        tvTelephone.setTypeface(Utils.getFontAvenirLight(getActivity()));
        layoutEmail = (RelativeLayout)rootView.findViewById(R.id.layout_email);
        tvLabelEmail = (TextView)rootView.findViewById(R.id.tv_label_email);
        tvLabelEmail.setTypeface(Utils.getFontAvenirHeavy(getActivity()));
        tvEmail = (TextView)rootView.findViewById(R.id.tv_email);
        tvEmail.setTypeface(Utils.getFontAvenirLight(getActivity()));
        layoutWebsite = (RelativeLayout)rootView.findViewById(R.id.layout_website);
        tvLabelWebsite = (TextView)rootView.findViewById(R.id.tv_label_website);
        tvLabelWebsite.setTypeface(Utils.getFontAvenirHeavy(getActivity()));
        tvWebsite = (TextView)rootView.findViewById(R.id.tv_website);
        tvWebsite.setTypeface(Utils.getFontAvenirLight(getActivity()));
    	
        /*
        svLayoutContent.setOnScrollUpListener(new OnScrollUpListener(){

			@Override
			public void onScrollUp() {
				View v = null;
				v = svLayoutContent.getChildAt(0);
				
				int top = (v == null) ? 0 : v.getTop();
				
				layoutLogo.setTranslationY((float) (top * 0.7));
			}
		});
        
        svLayoutContent.setOnScrollDownListener(new OnScrollDownListener(){

			@Override
			public void onScrollDown() {
				View v = null;
				v = svLayoutContent.getChildAt(0);
				
				int top = (v == null) ? 0 : v.getTop();
				
				layoutLogo.setTranslationY(-(layoutLogo.getHeight()+0));
			}
		});
        */
        
        layoutAddress.setOnClickListener(this);
        layoutTelephone.setOnClickListener(this);
        layoutEmail.setOnClickListener(this);
        layoutWebsite.setOnClickListener(this);
        
    	tvVersion.setText("VERSION " + versionNumber);
    	
    	YoYo.with(Techniques.FadeIn)
		.duration(Constants.animationTime * animationIndex)
		.playOn(layoutLogo);
    	
    	YoYo.with(Techniques.FadeIn)
		.duration(Constants.animationTime * (animationIndex+2))
		.playOn(layoutContent);
    	
    	
        return rootView;
    }

    
    @Override 
    public void onDestroyView(){
    	super.onDestroyView();
    }
   
    @Override
    public void onResume(){
    	super.onResume();

		//Utils.updateGA(getActivity(), getString(R.string.ga_screen_about));
    	
    }
    
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) { 
    	super.onActivityResult(requestCode, resultCode, imageReturnedIntent); 
    	
    }
    
    @Override
    public void onClick(View v){
    	Utils.hideSoftKeyboard(getActivity());
    	if(v == layoutAddress){
			/*
    		String label = "Codigo Private Limited";
    		String uriBegin = "geo:" + Constants.CONTACT_MAP_LAT + "," + Constants.CONTACT_MAP_LON;
    		String query = Constants.CONTACT_MAP_LAT + "," + Constants.CONTACT_MAP_LON + "(" + label + ")";
    		String encodedQuery = Uri.encode(query);
    		String uriString = uriBegin + "?q=" + encodedQuery + "&z=15";
    		Uri uri = Uri.parse(uriString);
    		
    		//Uri uri = Uri.parse(Constants.CONTACT_MAP);
    		Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
    		startActivity(intent);
    		*/
    	}
    	else if(v == layoutTelephone){
			/*
    		Intent intent = new Intent(Intent.ACTION_DIAL);
    		intent.setData(Uri.parse("tel:" + getActivity().getString(R.string.contact_telephone)));
    		startActivity(intent);
    		*/
    	}
    	else if(v == layoutEmail){
			/*
    		Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", getActivity().getString(R.string.contact_email), null));
    		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Contact us");
    		startActivity(Intent.createChooser(emailIntent, "Send email"));
    		*/
    	}
    	else if(v == layoutWebsite){
			/*
    		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://" + getActivity().getString(R.string.contact_website)));
    		startActivity(browserIntent);
    		*/
    	}
    }

	@Override
	public void callbackJson(String result, int processID, int index) {
		
	}


	@Override
	public void jsonError(String msg, int processID) {
		
	}


	@Override
	public void callBackPopup(Object data, int processID, int index) {
		
	}

}

package sg.codigo.mytemplate.fragments;

import java.io.Serializable;

import sg.codigo.mytemplate.R;
import sg.codigo.mytemplate.storages.AppPreferences;
import sg.codigo.mytemplate.utils.Constants;
import sg.codigo.mytemplate.utils.Utils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.squareup.picasso.Picasso;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

public final class FullScreenImageFragment extends Fragment {
    private static final String KEY_IMAGE = "FullScreenImageFragment:Image";
    private AppPreferences appPrefs;
    
    public static FullScreenImageFragment newInstance(String image) {
        FullScreenImageFragment fragment = new FullScreenImageFragment();

        fragment.image = image;
        
        return fragment;
    }

    private String image = "";

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_IMAGE, image);
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_IMAGE)) {
        	image = savedInstanceState.getString(KEY_IMAGE);
        }
        
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        appPrefs = new AppPreferences(getActivity()); 
        
    	RelativeLayout layout = new RelativeLayout(getActivity());
        layout.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        layout.setGravity(Gravity.CENTER);
        
        ImageView imageView = new ImageView(getActivity());
    	//Display display = getActivity().getWindowManager().getDefaultDisplay(); 
    	//int width = display.getWidth();  // deprecated
    	//int height = (int) ((width / 4) * 3);
    	//imageView.setLayoutParams(new RelativeLayout.LayoutParams(width, height));
    	
        RelativeLayout.LayoutParams params01 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        params01.addRule(RelativeLayout.CENTER_IN_PARENT);
		imageView.setLayoutParams(params01);//width, height));
    	imageView.setScaleType(ScaleType.FIT_CENTER);
    	//imageView.setImageResource(R.drawable.dummy_image);
    	imageView.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				
			}
    		
    	});
    	
    	
        layout.addView(imageView);
        
        /*
        final ProgressBar pb = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleSmall);
		pb.setIndeterminate(true);
		RelativeLayout.LayoutParams params02 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		params02.addRule(RelativeLayout.CENTER_IN_PARENT);
		pb.setLayoutParams(params02);
        layout.addView(pb);
        */
        
        if(image.toLowerCase().contains("http")){
        	
	        Picasso.with(getActivity())
			.load(image)
			.placeholder(R.drawable.bg_image_dummy)
			.error(R.drawable.bg_image_dummy)
			.skipMemoryCache()
			.into(imageView);
			/*
        	Uri uri = Uri.parse(image);
        	imageView.setImageURI(uri);
			*/
        }
        else if(image.toLowerCase().contains(".jpg")){
        	BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
			options.inSampleSize = Constants.IMAGE_SAMPLE_SIZE;
			Bitmap bitmap = BitmapFactory.decodeFile(image, options);
			if(bitmap!=null){
        		/*
        		Uri uri = Utils.getImageUri(getActivity(), bitmap);
        		ivParcelImages.setImageURI(uri);
    			*/
				imageView.setImageBitmap(bitmap);
        	}
        }
        else{
        	Bitmap bitmap = Utils.decodeBase64(image);
        	if(bitmap!=null){
        		/*
        		Uri uri = Utils.getImageUri(getActivity(), bitmap);
        		imageView.setImageURI(uri);
    			*/
        		
    			imageView.setImageBitmap(bitmap);
        	}
        }
        
        return layout;
    }

    
}

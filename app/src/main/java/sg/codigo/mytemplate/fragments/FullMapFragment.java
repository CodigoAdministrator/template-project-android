package sg.codigo.mytemplate.fragments;



import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sg.codigo.mytemplate.MainActivity;
import sg.codigo.mytemplate.callbacks.JsonCallback;
import sg.codigo.mytemplate.callbacks.PopupCallback;
import sg.codigo.mytemplate.R;
import sg.codigo.mytemplate.dialogs.DialogOK;
import sg.codigo.mytemplate.storages.AppPreferences;
import sg.codigo.mytemplate.storages.DatabaseHandler;
import sg.codigo.mytemplate.utils.Constants;
import sg.codigo.mytemplate.utils.HTTPGetAsyncTask;
import sg.codigo.mytemplate.utils.HTTPPostAsyncTask;
import sg.codigo.mytemplate.utils.Utils;
import sg.codigo.mytemplate.views.InteractiveScrollView;
import sg.codigo.mytemplate.views.RippleBackground;
import sg.codigo.mytemplate.views.InteractiveScrollView.OnBottomReachedListener;
import sg.codigo.mytemplate.views.InteractiveScrollView.OnScrollUpListener;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;


public class FullMapFragment extends Fragment implements OnClickListener, PopupCallback, JsonCallback, OnMapReadyCallback{
	// data
	private static Context context;
	private AppPreferences appPrefs;
	private DatabaseHandler db;
	private int animationIndex = 1;
	private static PopupCallback popupCallback;
	private ArrayList<NameValuePair> nameValuePairs;
	private JsonCallback jsonCallback;
	private DialogOK dialogOK;
	private DialogOK dialogError;
	private int screenId = 0;
	private Handler handlerRefresh;
	private boolean isSetup = false;
	
	// Google Map
	private MapFragment mapFragment;
	private View mvMap;
	private GoogleMap googleMap;
	private GoogleMapOptions options;
	//private List<Marker> markerList;
	private LatLng latlngMostNorthEast;
 	private LatLng latlngMostSouthWest;
   
 	// Define a DialogFragment that displays the error dialog
    public static class ErrorDialogFragment extends DialogFragment {
         // Global field to contain the error dialog
         private Dialog mDialog;
         // Default constructor. Sets the dialog field to null
         public ErrorDialogFragment() {
             super();
             mDialog = null;
         }
         // Set the dialog to display
         public void setDialog(Dialog dialog) {
             mDialog = dialog;
         }
         // Return a Dialog to the DialogFragment.
         @Override
         public Dialog onCreateDialog(Bundle savedInstanceState) {
             return mDialog;
         }
     }
     /*
      * Define a request code to send to Google Play services
      * This code is returned in Activity.onActivityResult
      */
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
 
	
    public FullMapFragment() {
        // Empty constructor required for fragment subclasses
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setRetainInstance(true); 
        //hide keybaord
    	getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    	
        View rootView = inflater.inflate(R.layout.fragment_full_map, container, false);
        //getActivity().setTitle(getString(R.string.title_merchants));
        
        context = getActivity();
    	appPrefs = new AppPreferences(getActivity());
    	db = new DatabaseHandler(getActivity());
    	popupCallback = this;
    	jsonCallback = this;
        nameValuePairs = new ArrayList<NameValuePair>();


    	if(handlerRefresh==null){
    		handlerRefresh = new Handler();
    	}

    	setUpMap();
    	
    	return rootView;
    }
    
    @Override
    public void onResume(){
    	super.onResume();
    	
    }
    
    @Override 
    public void onDestroyView(){
    	removeMap();
    	super.onDestroyView();
    	
    }
   
    public void onClick(View v){
    	
    }

    private void setUpMap() {
    	//markerList = new ArrayList<Marker>();
    	
    	mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map_full);
    	mapFragment.getMapAsync(this);
    	googleMap = mapFragment.getMap();
    	googleMap.getUiSettings().setZoomGesturesEnabled(true);
    	googleMap.getUiSettings().setZoomControlsEnabled(true);
    	
    	options = new GoogleMapOptions();
    	options.mapType(GoogleMap.MAP_TYPE_HYBRID)
    	.compassEnabled(true)
        .rotateGesturesEnabled(true)
        .scrollGesturesEnabled(true)
        .tiltGesturesEnabled(true)
        .zoomControlsEnabled(true)
        .zoomGesturesEnabled(true)
        ;
    	
        googleMap.setMyLocationEnabled(false);
        googleMap.setIndoorEnabled(true);
        
        mvMap = mapFragment.getView();
        
        
    }
	
    private void updateMapBorder(double lat, double lon){
    	
		if(latlngMostNorthEast.latitude < lat){
			latlngMostNorthEast = new LatLng(lat, latlngMostNorthEast.longitude);
		}
		if(latlngMostSouthWest.latitude > lat){
			latlngMostSouthWest = new LatLng(lat, latlngMostSouthWest.longitude);
		}
		if(latlngMostNorthEast.longitude < lon){
			latlngMostNorthEast = new LatLng(latlngMostNorthEast.latitude, lon);
		}
		if(latlngMostSouthWest.longitude > lon){
			latlngMostSouthWest = new LatLng(latlngMostSouthWest.latitude, lon);
		}
		
    }
    
	@Override
	public void callBackPopup(Object data, int processID, int index) {
		if(processID == Constants.POPUP_API_CALL_SUCCEED){
			
		}
		else if(processID == Constants.POPUP_API_CALL_FAILED){
			
		}
	}


	@Override
	public void callbackJson(String result, int processID, int index) {
		/*
		if(processID == Constants.ID_API_GET_BOOKING_STATUS){
			try{
				if(result!=null && !result.equalsIgnoreCase("")){
				
					JSONObject jsonObj = new JSONObject(result);
					
					int errorCode = jsonObj.getInt("errorCode");
					String message = jsonObj.getString("message");
					
					if(errorCode == Constants.ERROR_CODE_SUCCESS && jsonObj.has("booking_info") && jsonObj.getString("booking_info")!=null && jsonObj.getString("booking_info").startsWith("{")){
						JSONObject bookingObj = jsonObj.getJSONObject("booking_info");
						
						selectedBooking.setBookingId((bookingObj.has("booking_id") && bookingObj.getString("booking_id")!=null)?bookingObj.getString("booking_id"):"");
						selectedBooking.setType((bookingObj.has("type") && bookingObj.getString("type")!=null)?bookingObj.getString("type"):"");
						selectedBooking.setStatus((bookingObj.has("status") && bookingObj.getString("status")!=null)?bookingObj.getString("status"):"");
						selectedBooking.setCode((bookingObj.has("code") && bookingObj.getString("code")!=null)?bookingObj.getString("code"):"");
						selectedBooking.setDate((bookingObj.has("date") && bookingObj.getString("date")!=null)?bookingObj.getString("date"):"");
						selectedBooking.setSrcAddressDisplayName((bookingObj.has("src_address_display_name") && bookingObj.getString("src_address_display_name")!=null)?bookingObj.getString("src_address_display_name"):"");
						selectedBooking.setSrcAddress((bookingObj.has("src_address") && bookingObj.getString("src_address")!=null)?bookingObj.getString("src_address"):"");
						selectedBooking.setSrcAddressUnitNumber((bookingObj.has("src_unit_number") && bookingObj.getString("src_unit_number")!=null)?bookingObj.getString("src_unit_number"):"");
						selectedBooking.setSrcAddressPostalCode((bookingObj.has("src_postal_code") && bookingObj.getString("src_postal_code")!=null)?bookingObj.getString("src_postal_code"):"");
						selectedBooking.setIsGoodCondition((bookingObj.has("is_good_condition") && bookingObj.getString("is_good_condition")!=null)?bookingObj.getString("is_good_condition"):"true");
						selectedBooking.setConditionComment((bookingObj.has("condition_comment") && bookingObj.getString("condition_comment")!=null)?bookingObj.getString("condition_comment"):"");
						selectedBooking.setPaymentType((bookingObj.has("payment_type") && bookingObj.getString("payment_type")!=null)?bookingObj.getString("payment_type"):"");
						selectedBooking.setTotalAmountAfterDiscount((bookingObj.has("total_amount_after_discount") && bookingObj.get("total_amount_after_discount")!=null)?bookingObj.getDouble("total_amount_after_discount"):0);
						
						if(bookingObj.has("driver_info") && bookingObj.getString("driver_info").startsWith("{")){
							JSONObject driverObj = bookingObj.getJSONObject("driver_info");
							Driver driver = new Driver();
							driver.setId((driverObj.has("driver_id") && driverObj.getString("driver_id")!=null)?driverObj.getString("driver_id"):"");
							driver.setName((driverObj.has("driver_name") && driverObj.getString("driver_name")!=null)?driverObj.getString("driver_name"):"");
							driver.setMobile((driverObj.has("driver_mobile") && driverObj.getString("driver_mobile")!=null)?driverObj.getString("driver_mobile"):"");
							driver.setVehicleNumber((driverObj.has("driver_vehicle_number") && driverObj.getString("driver_vehicle_number")!=null)?driverObj.getString("driver_vehicle_number"):"");
							driver.setPhotoURL((driverObj.has("driver_photo") && driverObj.getString("driver_photo")!=null)?driverObj.getString("driver_photo"):"");
							driver.setTier((driverObj.has("driver_tier") && driverObj.getString("driver_tier")!=null)?driverObj.getString("driver_tier"):"");
							driver.setScore((driverObj.has("driver_score") && driverObj.get("driver_score")!=null)?driverObj.getDouble("driver_score"):0);
							
							sg.codigo.mytemplate.models.Location driverCurrentLocation = new sg.codigo.mytemplate.models.Location();
							driverCurrentLocation.setLat((driverObj.has("current_lat") && driverObj.get("current_lat")!=null)?driverObj.getDouble("current_lat"):0);
							driverCurrentLocation.setLon((driverObj.has("current_long") && driverObj.get("current_long")!=null)?driverObj.getDouble("current_long"):0);
							driver.setCurrentLocation(driverCurrentLocation);
							
							driver.setEstimateDistanceToSrcLocation((driverObj.has("estimate_distance_to_src_location") && driverObj.getString("estimate_distance_to_src_location")!=null)?driverObj.getString("estimate_distance_to_src_location"):"");
							driver.setEstimateTimeToSrcLocation((driverObj.has("estimate_time_to_src_location") && driverObj.getString("estimate_time_to_src_location")!=null)?driverObj.getString("estimate_time_to_src_location"):"");
							driver.setEstimateDistanceToDestLocation((driverObj.has("estimate_distance_to_dest_location") && driverObj.getString("estimate_distance_to_dest_location")!=null)?driverObj.getString("estimate_distance_to_dest_location"):"");
							driver.setEstimateTimeToDestLocation((driverObj.has("estimate_time_to_dest_location") && driverObj.getString("estimate_time_to_dest_location")!=null)?driverObj.getString("estimate_time_to_dest_location"):"");
							
							selectedBooking.setDriver(driver);
						}
						
					}else if(errorCode == Constants.ERROR_CODE_INVALID_TOKEN){
						getActivity().finish();

					}else if(errorCode == Constants.ERROR_CODE_ACCOUNT_BLOCKED){
						if(dialogOK==null || !dialogOK.isShowing()){
							dialogOK = new DialogOK(getActivity(), getString(R.string.dialog_title_error), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_ACCOUNT_BLOCKED, popupCallback);
							dialogOK.show();
						}
					}else{
						if(dialogError==null || !dialogError.isShowing()){
							dialogError = new DialogOK(getActivity(), getString(R.string.dialog_title_error), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_FAILED, popupCallback);
							dialogError.show();
						}
					}
				}
			}catch(JSONException e){
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, "JSONException: " + e.toString());
				}
			}catch(Exception e){
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, "Exception: " + e.toString());
				}
			}finally{
				setUpMap();
		    	
			}
		}
		*/
	}


	@Override
	public void jsonError(String msg, int processID) {
		
	}


	@Override
	public void onMapReady(final GoogleMap googleMap) {


		if(Constants.DEBUG_MODE){
			//Log.d(Constants.TAG, "selectedBooking: " + selectedBooking.getBookingId());

		}

		if(googleMap!=null){
			googleMap.clear();

			latlngMostNorthEast = Constants.DEFAULT_NORTH_EAST_POINT;
			latlngMostSouthWest = Constants.DEFAULT_SOUTH_WEST_POINT;



			final LatLngBounds bounds = new LatLngBounds.Builder()
			.include(latlngMostNorthEast)
			.include(latlngMostSouthWest)
			.build();

			try{
				if (mvMap.getViewTreeObserver().isAlive()) {
					if(!isSetup){
						googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 200));
						isSetup = true;
					}else{
						googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 200));
					}
				}
			}catch (IllegalStateException e) {
				// layout not yet initialized

				if (mvMap.getViewTreeObserver().isAlive()) {
					mvMap.getViewTreeObserver().addOnGlobalLayoutListener(
					new OnGlobalLayoutListener() {
						@SuppressWarnings("deprecation")
						@SuppressLint("NewApi")
						// We check which build version we are using.
						@Override
						public void onGlobalLayout() {
							if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
								mvMap.getViewTreeObserver().removeGlobalOnLayoutListener(this);
							} else {
								mvMap.getViewTreeObserver().removeOnGlobalLayoutListener(this);
							}
							if(!isSetup){
								googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 200));
								isSetup = true;
							}else{
								googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 200));
							}
						}
					});
				}
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, "IllegalStateException: " + e.toString());
				}

			} catch (Exception e) {
				if(Constants.DEBUG_MODE){
				 Log.e(Constants.TAG, "Exception: " + e.toString());
				}
			}

		}
	}
	
	private void removeMap(){
		try {
            MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map_full);
            if (mapFragment != null) {
            	getFragmentManager().beginTransaction().remove(mapFragment).commitAllowingStateLoss();
            }

        } catch (IllegalStateException e) {
        	if(Constants.DEBUG_MODE){
				Log.e(Constants.TAG, "IllegalStateException: " + e.toString());
			}
        } catch (Exception e) {
        	if(Constants.DEBUG_MODE){
				Log.e(Constants.TAG, "Exception: " + e.toString());
			}
        }
	}
}

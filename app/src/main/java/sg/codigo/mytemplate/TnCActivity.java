package sg.codigo.mytemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import sg.codigo.mytemplate.callbacks.JsonCallback;
import sg.codigo.mytemplate.callbacks.PopupCallback;
import sg.codigo.mytemplate.dialogs.DialogOK;
import sg.codigo.mytemplate.dialogs.DialogTwoButtons;
import sg.codigo.mytemplate.storages.AppPreferences;
import sg.codigo.mytemplate.utils.Constants;
import sg.codigo.mytemplate.utils.HTTPPostAsyncTask;
import sg.codigo.mytemplate.utils.TypefaceSpan;
import sg.codigo.mytemplate.utils.Utils;
import sg.codigo.mytemplate.views.InteractiveScrollView;
import sg.codigo.mytemplate.views.InteractiveScrollView.OnBottomReachedListener;
import sg.codigo.mytemplate.views.InteractiveScrollView.OnScrollDownListener;
import sg.codigo.mytemplate.views.InteractiveScrollView.OnScrollUpListener;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.nineoldandroids.animation.Animator;

import android.net.Uri;
import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.ColorDrawable;

public class TnCActivity extends Activity implements OnClickListener, PopupCallback{
	// data
	private AppPreferences appPrefs;
	private int animationIndex = 1;
	private boolean lockSlideUp = false;
	private boolean lockSlideDown = false;
	private DialogOK dialogForceUpdate;
	private DialogTwoButtons dialogOptionalUpdate;
	private PopupCallback popupCallback;
	
	
	// layout
	private InteractiveScrollView svLayoutContent;
	private LinearLayout layoutContent;
	private TextView tvSubtitle;
	private TextView tvContent;
	//private WebView wvTNC;
	private RelativeLayout layoutBtnAgree;
	private Button btnAgree;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        if(Constants.DEBUG_MODE){
			Log.e(Constants.TAG, "TnCActivity");
		}
        
        Utils.updateActionBar(TnCActivity.this, getString(R.string.title_tnc), true, false, false, null);
    	
        setContentView(R.layout.activity_tnc);
        appPrefs = new AppPreferences(TnCActivity.this);
        popupCallback = TnCActivity.this;
    	
        try{
	        String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			
	        if(appPrefs.getAppVersionName().compareToIgnoreCase(versionName) > 0){
				if(appPrefs.getIsForceUpdate()){
					// force user to update
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "force user to update");
					}
					if(dialogForceUpdate == null || !dialogForceUpdate.isShowing()){
						dialogForceUpdate = new DialogOK(TnCActivity.this, 
								getString(R.string.dialog_title_app_update), 
								appPrefs.getAppVersionMessage(), //getString(R.string.alert_message_app_update), 
								getString(R.string.btn_go_to_play_store), 
								Constants.POPUP_APP_FORCE_UPDATE, 
								popupCallback);
						dialogForceUpdate.show();
					}
				}else{
					// prompt user to update (optional)
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "prompt user to update (optional)");
					}
					Calendar todayCalendar = Calendar.getInstance();
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);
					if(!appPrefs.getOptionalUpdateLastCheckingDate().equalsIgnoreCase(sdf.format(todayCalendar.getTime())) && (dialogOptionalUpdate == null || !dialogOptionalUpdate.isShowing())){
						dialogOptionalUpdate = new DialogTwoButtons(TnCActivity.this, 
								getString(R.string.dialog_title_app_update), 
								appPrefs.getAppVersionMessage(), //getString(R.string.alert_message_app_update), 
								getString(R.string.btn_maybe_later), 
								getString(R.string.btn_go_to_play_store), 
								Constants.POPUP_APP_OPTIONAL_UPDATE, 
								popupCallback);
						dialogOptionalUpdate.show();
						appPrefs.setOptionalUpdateLastCheckingDate(sdf.format(todayCalendar.getTime()));
					}else{
						if(!appPrefs.getIsAgreedTNC()){
				        	init();
				        }else{
				    		Intent intent = new Intent(this, LandingActivity.class);
				    		startActivity(intent);
				    		this.finish();
				    		appPrefs.setIsAgreedTNC(true);
				        }
					}
				}
	        }
	        else{
	        	if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, "no update");
				}
	        	
		        if(!appPrefs.getIsAgreedTNC()){
		        	init();
		        }else{
		    		Intent intent = new Intent(this, LandingActivity.class);
		    		startActivity(intent);
		    		this.finish();
		    		appPrefs.setIsAgreedTNC(true);
		        }
		    }
        }catch(NameNotFoundException e){
			if(Constants.DEBUG_MODE){
				Log.e(Constants.TAG, "NameNotFoundException: " + e.toString());
			}
		}catch(Exception e){
			if(Constants.DEBUG_MODE){
				Log.e(Constants.TAG, "Exception: " + e.toString());
			}
		}
    }
    
    @Override
    public void onStart() {
      super.onStart();
       
    }
    
    @Override
    protected void onResume(){
		super.onResume();
		Utils.updateGA(this, getString(R.string.ga_screen_tnc));
		if(appPrefs == null){
    		appPrefs = new AppPreferences(this);
    	}
		
		if(appPrefs.getEmergencyStatus()){
        	Intent intent = new Intent(this, EmergencyActivity.class);
    		startActivity(intent);
    		this.finish();
        }
    }
    
    @Override
    public void onPause() {
        super.onPause();
    }
    
    @Override
    protected void onDestroy(){
    	super.onDestroy();
    }
    
    @Override
	protected void onStop() {
    	super.onStop();
    	
	}
    
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        	if(!Utils.isConnectedToInternet(TnCActivity.this)){
        		finish();
        	}else{
        		super.onBackPressed();
        	}
        	return true;
        }else if (keyCode == KeyEvent.KEYCODE_MENU) {
        
        }else if (keyCode == KeyEvent.KEYCODE_HOME) {
        
        }

        return false;
        //return super.onKeyDown(keyCode, event);
    }
	
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
		
    }
	
	
    @Override
	public void onLowMemory(){
    	System.gc();
	}
	
    @Override
	public void onSaveInstanceState (Bundle outState) {
    	super.onSaveInstanceState(outState);
    }
   
    @Override
    protected void onRestoreInstanceState (Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
    
    WebViewClient webViewClient= new WebViewClient(){
	    @Override
	    public boolean shouldOverrideUrlLoading(WebView  view, String  url){
	    	if(Constants.DEBUG_MODE){
	    		Log.d(Constants.TAG, "shouldOverrideUrlLoading url: " + url);
	    	}
	        if(url.contains("mailto")){
	        	String[] emailData = url.split(":");
	        	if(emailData.length>1){
	        	Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(emailData[0],emailData[1], null));
	        		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
	        		startActivity(Intent.createChooser(emailIntent, "Send email"));
	        	}
	        }
	        
	        else{
	        	Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
	        	startActivity(browserIntent);
	        }
	        
	        return true;
	    }
	    
	    @Override
	    public void onPageFinished(WebView view, String url){
	    	if(Utils.isShowingLoadingDialog(TnCActivity.this)){
	    		Utils.hideLoadingDialog(TnCActivity.this);
	    	}
	    	layoutBtnAgree.setVisibility(View.VISIBLE);
	    }
	    
	    /*
	    @Override
	    public void onLoadResource(WebView view, String url){
	    	
	    	if(Constants.DEBUG_MODE){
	    		Log.d(Constants.TAG, "onLoadResource url: " + url);
	    	}
	        if(url.contains("mailto")){
	        	String[] emailData = url.split(":");
	        	if(emailData.length>1){
	        	Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(emailData[0],emailData[1], null));
	        		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "FastFast Delivery(Driver)");
	        		context.startActivity(Intent.createChooser(emailIntent, "Send email"));
	        	}
	        }
	        
	    }
	    */
	};
	
    private void init(){
    	
    	//hide keybaord
    	getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    	

    	svLayoutContent = (InteractiveScrollView)findViewById(R.id.sv_layout_content);
    	layoutContent = (LinearLayout)findViewById(R.id.layout_content);
    	tvSubtitle = (TextView)findViewById(R.id.tv_subtitle);
    	tvSubtitle.setTypeface(Utils.getFontAvenirHeavy(TnCActivity.this));
    	tvContent = (TextView)findViewById(R.id.tv_content);
    	tvContent.setTypeface(Utils.getFontAvenirLight(TnCActivity.this));

    	//wvTNC = (WebView)findViewById(R.id.wv_tnc);
    	layoutBtnAgree = (RelativeLayout)findViewById(R.id.layout_btn_agree);
    	btnAgree = (Button)findViewById(R.id.btn_agree);
    	btnAgree.setTypeface(Utils.getFontAvenirBook(TnCActivity.this));
    	btnAgree.setOnClickListener(this);
    	//svLayoutContent.setVisibility(View.VISIBLE);
    	layoutBtnAgree.setVisibility(View.VISIBLE);
    	//wvTNC.setVisibility(View.VISIBLE);
    	/*
    	if(!Utils.isShowingLoadingDialog(TnCActivity.this)){
    		Utils.showLoadingDialog(TnCActivity.this);
    	}
    	
    	wvTNC.loadUrl(Constants.getURLTNC());
    	wvTNC.setWebViewClient(webViewClient);
		wvTNC.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
			    return true;
			}
		});
		wvTNC.setLongClickable(false);
		*/
    	
    	/*
    	svLayoutContent.setScrollViewCallbacks(new ObservableScrollViewCallbacks(){

			@Override
			public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onDownMotionEvent() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onUpOrCancelMotionEvent(ScrollState scrollState) {
				if (scrollState == ScrollState.UP) {
					if(layoutBtnAgree.getVisibility() == View.GONE){
						layoutBtnAgree.setVisibility(View.VISIBLE);
				    	YoYo.with(Techniques.SlideInUp)
			    		.duration(Constants.animationTime * (animationIndex+1))
			    		.playOn(layoutBtnAgree);
					}
		        } else if (scrollState == ScrollState.DOWN) {
		        	if(layoutBtnAgree.getVisibility() == View.VISIBLE){
						YoYo.with(Techniques.SlideOutDown)
			    		.duration(Constants.animationTime * (animationIndex+1))
			    		.withListener(new Animator.AnimatorListener() {
	                        @Override
	                        public void onAnimationStart(Animator animation) {
	                        	
	                        }

	                        @Override
	                        public void onAnimationEnd(Animator animation) {
	                        	layoutBtnAgree.setVisibility(View.GONE);
	                        }

	                        @Override
	                        public void onAnimationCancel(Animator animation) {
	                            
	                        }

	                        @Override
	                        public void onAnimationRepeat(Animator animation) {

	                        }
	                    })
			    		.playOn(layoutBtnAgree)
			    		;
					}
		        }
			}
    		
    	});
    	*/
    	
    	/*
    	svLayoutContent.setOnBottomReachedListener(new OnBottomReachedListener(){

			@Override
			public void onBottomReached() {
				if(layoutBtnAgree.getVisibility() == View.GONE && !lockSlideUp){
					lockSlideUp = true;
					layoutBtnAgree.setVisibility(View.VISIBLE);
			    	YoYo.with(Techniques.SlideInUp)
			    	.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	lockSlideUp = false;
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.duration(Constants.animationTime * animationIndex)
		    		.playOn(layoutBtnAgree);
				}
			}
    		
    	});
    	*/
    	
    	/*
    	svLayoutContent.setOnScrollUpListener(new OnScrollUpListener(){

			@Override
			public void onScrollUp() {
				if(layoutBtnAgree.getVisibility() == View.VISIBLE && !lockSlideDown){
					lockSlideDown = true;
					YoYo.with(Techniques.SlideOutDown)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	layoutBtnAgree.setVisibility(View.GONE);
                        	lockSlideDown = false;
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.duration(Constants.animationTime * animationIndex)
		    		.playOn(layoutBtnAgree)
		    		;
				}
			}
    		
    	});
    	
    	svLayoutContent.setOnScrollDownListener(new OnScrollDownListener(){

			@Override
			public void onScrollDown() {
				if(layoutBtnAgree.getVisibility() == View.GONE && !lockSlideUp){
					lockSlideUp = true;
					layoutBtnAgree.setVisibility(View.VISIBLE);
			    	YoYo.with(Techniques.SlideInUp)
			    	.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	lockSlideUp = false;
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.duration(Constants.animationTime * animationIndex)
		    		.playOn(layoutBtnAgree);
				}
			}
    		
    	});
    	
    	*/
    	
    	

    	YoYo.with(Techniques.FadeIn)
        	.duration(Constants.animationTime * animationIndex)
        	.playOn(layoutContent);
    	/*
    	YoYo.with(Techniques.FadeIn)
    	.duration(Constants.animationTime * animationIndex)
    	.playOn(wvTNC);
		*/
    	
    	
    	
		/*
    	if(Constants.IS_GA_ON){
			
    		if(MainActivity.trackerCDG!=null){
				// Set screen name.
				MainActivity.trackerCDG.setScreenName(getString(R.string.ga_screen_terms));
	
		        // Send a screen view.
				MainActivity.trackerCDG.send(new HitBuilders.AppViewBuilder().build());
    		}
    		
    		if(MainActivity.trackerSP!=null){
				// Set screen name.
				MainActivity.trackerSP.setScreenName(getString(R.string.ga_screen_terms));
	
		        // Send a screen view.
				MainActivity.trackerSP.send(new HitBuilders.AppViewBuilder().build());
    		}
    	}
    	*/
    	
    	
    }
    
	
    public void onClick(View v) {
    	Utils.hideSoftKeyboard(TnCActivity.this);
    	if(v == btnAgree){
    		//appPrefs.setIsFirstLaunch(false);
    		//appPrefs.setIsLoggedIn(false);
    		Intent intent = new Intent(this, LandingActivity.class);
            startActivity(intent);
            this.finish();
            appPrefs.setIsAgreedTNC(true);
    	}
    }

    @Override
	public void callBackPopup(Object data, int processID, int index) {
		if(processID == Constants.POPUP_APP_FORCE_UPDATE){
			
			if(Constants.DEBUG_MODE){
				Log.d(Constants.TAG, "getPackageName(): " + getPackageName());
			}
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
			this.finish();
		}
		else if(processID == Constants.POPUP_APP_OPTIONAL_UPDATE){
			if(index == Constants.DIALOG_BUTTON_RIGHT){
				if(Constants.DEBUG_MODE){
					Log.d(Constants.TAG, "getPackageName(): " + getPackageName());
				}
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
				this.finish();
			}else{
				
				if(!appPrefs.getIsAgreedTNC()){
		        	init();
		        }else{
		    		Intent intent = new Intent(this, LandingActivity.class);
		    		startActivity(intent);
		    		this.finish();
		    		appPrefs.setIsAgreedTNC(true);
		        }
			}
		}
	}
	
}

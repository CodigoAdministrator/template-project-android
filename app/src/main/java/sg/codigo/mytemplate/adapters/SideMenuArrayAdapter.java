package sg.codigo.mytemplate.adapters;

import java.util.ArrayList;
import java.util.List;

import sg.codigo.mytemplate.R;
import sg.codigo.mytemplate.models.SideMenuItem;
import sg.codigo.mytemplate.utils.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SideMenuArrayAdapter extends ArrayAdapter<SideMenuItem>{
	private final Context context;
	private final List<SideMenuItem> sideMenuList;
	private View rowView;
	//private ViewHolder holder;
	private RelativeLayout layoutView;
	private LinearLayout layoutPadding;
	//private ImageButton btnCloseLeftMenu;
	private ImageView icLeftMenu;
	private TextView title;
	private TextView notificationNo;
	//private LinearLayout layoutSubMenu;
	
	public SideMenuArrayAdapter(Context context, List<SideMenuItem> sideMenuList) {
		super(context, android.R.layout.simple_list_item_1, sideMenuList);
		this.context = context;
		this.sideMenuList = sideMenuList;
	}
 
	@Override
	public int getCount() {
		return this.sideMenuList.size(); 
	}

	@Override
    public SideMenuItem getItem(int position) {
        return this.sideMenuList.get(position);
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = convertView;
		
		final SideMenuItem smi = getItem(position);
		
		
		rowView = inflater.inflate(R.layout.row_left_menu, null);
		layoutView = (RelativeLayout)rowView.findViewById(R.id.row_left_menu_layout);
		layoutPadding = (LinearLayout)rowView.findViewById(R.id.layout_padding);
		//btnCloseLeftMenu = (ImageButton)rowView.findViewById(R.id.btn_close_left_menu);
		icLeftMenu = (ImageView)rowView.findViewById(R.id.ic_left_menu);
		title = (TextView)rowView.findViewById(R.id.row_left_menu_title);
		title.setTypeface(Utils.getFontAvenirBook(context));
		
		notificationNo = (TextView)rowView.findViewById(R.id.row_left_menu_notification_no);
		notificationNo.setTypeface(Utils.getFontAvenirHeavy(context));
		
		//layoutSubMenu = (LinearLayout)rowView.findViewById(R.id.row_left_menu_layout_submenu);
		//layoutSubMenu.removeAllViewsInLayout();
		
		if(position == 0){
			layoutPadding.setVisibility(View.VISIBLE);
		}else{
			layoutPadding.setVisibility(View.GONE);
		}
		
		/*
		if(smi.getType() == 0){
			btnCloseLeftMenu.setVisibility(View.VISIBLE);
			icLeftMenu.setVisibility(View.GONE);
			title.setVisibility(View.GONE);
			Animation rotation = AnimationUtils.loadAnimation(context, R.anim.spin);
			rotation.setRepeatCount(0);
			btnCloseLeftMenu.startAnimation(rotation);
			
			btnCloseLeftMenu.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					MainActivity.toggleDrawer(false);
				}
				
			});
		}
		else 
		*/
		//if(smi.getType() == 1){
		
			//btnCloseLeftMenu.setVisibility(View.GONE);
			icLeftMenu.setImageResource(smi.getIconRes());
			icLeftMenu.setVisibility(View.VISIBLE);
			title.setVisibility(View.VISIBLE);
			
		//}
		//else{
			
		//}
		
		title.setText(smi.getLabel());
		
		if(smi.getNotificationNo() <= 0 || !smi.getIsEnabled()){
			notificationNo.setVisibility(View.GONE);
		}else{
			notificationNo.setVisibility(View.VISIBLE);
			if(smi.getNotificationNo() > 99){
				notificationNo.setText("99+");
			}else{
				notificationNo.setText(String.valueOf(smi.getNotificationNo()));
			}
		}
		
		return rowView;
	}

}

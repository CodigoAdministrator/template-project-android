package sg.codigo.mytemplate.adapters;

import java.util.ArrayList;
import java.util.List;

import sg.codigo.mytemplate.R;
import sg.codigo.mytemplate.models.ActionItem;
import sg.codigo.mytemplate.models.SideMenuItem;
import sg.codigo.mytemplate.utils.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ActionArrayAdapter extends ArrayAdapter<ActionItem>{
	private final Context context;
	private final List<ActionItem> actionList;
	private View rowView;
	private ImageView icChoosePhoto;
	private TextView title;
	
	public ActionArrayAdapter(Context context, List<ActionItem> actionList) {
		super(context, android.R.layout.simple_list_item_1, actionList);
		this.context = context;
		this.actionList = actionList;
	}
 
	@Override
	public int getCount() {
		return this.actionList.size(); 
	}

	@Override
    public ActionItem getItem(int position) {
        return this.actionList.get(position);
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = convertView;
		
		final ActionItem ai = getItem(position);
		
		
		rowView = inflater.inflate(R.layout.row_choose_photo, null);
		icChoosePhoto = (ImageView)rowView.findViewById(R.id.ic_choose_photo);
		title = (TextView)rowView.findViewById(R.id.row_choose_photo_title);
		title.setTypeface(Utils.getFontAvenirBook(context));
		
		icChoosePhoto.setImageResource(ai.getIconRes());
		icChoosePhoto.setVisibility(View.VISIBLE);
		title.setVisibility(View.VISIBLE);
		
		title.setText(ai.getLabel());
		
		
		return rowView;
	}

}

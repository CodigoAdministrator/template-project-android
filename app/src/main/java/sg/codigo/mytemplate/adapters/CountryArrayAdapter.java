package sg.codigo.mytemplate.adapters;

import java.util.ArrayList;
import java.util.List;

import sg.codigo.mytemplate.R;
import sg.codigo.mytemplate.models.Country;
import sg.codigo.mytemplate.utils.Utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CountryArrayAdapter extends ArrayAdapter<Country>{
	private Context context;
	//private LayoutInflater mLayoutInflater;
	private final List<Country> countryList;
	private View rowView;
	
	public CountryArrayAdapter(Context context, List<Country> countryList) {
		super(context, android.R.layout.simple_list_item_1, countryList);
		this.context = context;
		this.countryList = countryList;
	}
 
	@Override
	public int getCount() {
		return this.countryList.size(); 
	}

	@Override
    public Country getItem(int position) {
        return this.countryList.get(position);
    }

	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = convertView;
		final Country country = getItem(position);
		
		if(rowView == null){
			rowView = inflater.inflate(R.layout.row_spinner_selected_item, parent, false);
		}
		
		TextView tvName = (TextView)rowView.findViewById(R.id.row_spinner_selected_item);
		tvName.setTypeface(Utils.getFontAvenirLight(context));
		tvName.setText(country.getName());

        return rowView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
    	LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rowView = convertView;
		final Country country = getItem(position);
		
		if(rowView == null){
			rowView = inflater.inflate(R.layout.row_spinner_list_item, parent, false);
		}
		
		TextView tvName = (TextView)rowView.findViewById(R.id.row_spinner_list_item);
		tvName.setTypeface(Utils.getFontAvenirLight(context));
		tvName.setText(country.getName());

        return rowView;
    }
}

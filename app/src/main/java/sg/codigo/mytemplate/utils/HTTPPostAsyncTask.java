package sg.codigo.mytemplate.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.params.ConnManagerPNames;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import sg.codigo.mytemplate.callbacks.JsonCallback;
import sg.codigo.mytemplate.R;
import sg.codigo.mytemplate.dialogs.DialogProgressBar;
import sg.codigo.mytemplate.security_ssl.EasySSLSocketFactory;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;



public class HTTPPostAsyncTask extends AsyncTask<String, String, String> {
	private Context context;
	private int processID;
	private JsonCallback callback;
	public String actionURL;
	private String errorString = "";
	private boolean showProgressBar;
	//private DialogProgressBar progressDialog;
	private ArrayList<NameValuePair> nameValuePairs;
	private boolean isDialogCancelable=true;
	
	/*
	public HTTPPostAsyncTask(Context context, ArrayList<NameValuePair> nameValuePairs, String actionURL, JsonCallback callback, int processID, boolean showProgressBar) {
		this.actionURL = actionURL;
		this.callback = callback;
		this.nameValuePairs=nameValuePairs;
		this.processID = processID;
		this.showProgressBar = showProgressBar;
		this.context=context;

		if (Utils.isConnectionAvailable(context)) {
			execute();
		} else {
			if (progressDialog != null && showProgressBar) {
				progressDialog.dismiss();
			}
			if (callback != null) {
				callback.jsonError(context.getString(R.string.error_message_no_internet), processID);
			}
		}
	}
	*/
	
	public HTTPPostAsyncTask(Context context, ArrayList<NameValuePair> nameValuePairs,String actionURL, JsonCallback callback, int processID, boolean showProgressBar ,boolean isDialogCancelable) {
		this.actionURL = actionURL;
		this.callback = callback;
		this.nameValuePairs=nameValuePairs;
		this.processID = processID;
		this.showProgressBar = showProgressBar;
		this.context=context;
		this.isDialogCancelable=isDialogCancelable;

		if (Utils.isConnectionAvailable(context)) {
			execute();
		} else {
			/*
			if (progressDialog != null && showProgressBar) {
				
				progressDialog.dismiss();
			}
			*/
			if(Utils.isShowingLoadingDialog(context) && showProgressBar){
				Utils.hideLoadingDialog(context);
			}
			
			if (callback != null) {
				callback.jsonError(context.getString(R.string.error_message_no_internet), processID);
			}
		}

	}

	@Override
	protected void onPreExecute() {
		if (showProgressBar) {
			/*
			if (progressDialog == null) {
				progressDialog = new DialogProgressBar(context);
				progressDialog.setCancelable(isDialogCancelable);
			}
			
			progressDialog.show();
			*/
			if(!Utils.isShowingLoadingDialog(context)){
				Utils.showLoadingDialog(context, isDialogCancelable);
			}
		}
	}

	@Override
	protected void onPostExecute(String result) {
		/*
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
		*/
		if(Utils.isShowingLoadingDialog(context)){
			Utils.hideLoadingDialog(context);
		}
		
		if (errorString.equals("")) {
			callback.callbackJson(result, processID, 0);
		} else {
			/*
			if(processID==APIConstants.ID_TRACK_DRIVER_FOR_BOOKING ||
					processID==APIConstants.ID_NEAR_BY_TAXI  ||
					processID==APIConstants.ID_TRACK_ADVERTISMENT ||
					processID==APIConstants.ID_BOOKING_STATUS_COUNT ||
					processID==APIConstants.ID_HISTORY_TRIP_COUNT){
				
			}else{
			*/
				callback.jsonError(errorString, processID);
			/*	
			}
			*/	
		}
	}

	
	@Override
	protected String doInBackground(String... obj) {
		HttpClient httpsClient;
		HttpPost httppost;
		
		HttpResponse response  = null;
		String result = "";				
		
//		httpsClient = new DefaultHttpClient();
		httpsClient = Utils.getHTTPSClient();

		String request = actionURL;
		
		try {
			//httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY,CookiePolicy.RFC_2109);
			httppost = new HttpPost(request);
			
			String base64 = "";
	        
			String authorization = Constants.UN + ":" + Constants.PW;
	        base64 = Base64.encodeToString(authorization.getBytes(), Base64.NO_WRAP);
	        httppost.setHeader("Authorization", "Basic " + base64);
			httppost.setHeader("Content-Type", Constants.contentType);
			if(nameValuePairs!=null){
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
			}
			
			if(Constants.DEBUG_MODE){
	        	Log.d(Constants.TAG, "API URL: " + actionURL);
	        	Log.d(Constants.TAG, "Authorization: Basic " + base64);
	        	Log.d(Constants.TAG, "Content-Type: " + Constants.contentType);
		        Log.d(Constants.TAG, "httppost.getEntity().getContent().toString(): " + convertStreamToString(httppost.getEntity().getContent()));
			}
			
			response = httpsClient.execute(httppost);

			if (response != null) {
				result = EntityUtils.toString(response.getEntity(), "UTF-8");
			}
			
			if(Constants.DEBUG_MODE){
				Log.d(Constants.TAG, Utils.class.getSimpleName() + "-result: " + result);
			}
			
		}catch(SocketTimeoutException e){
			if(Constants.DEBUG_MODE){
				Log.d(Constants.TAG, "SocketTimeoutException: " + e.toString());
			}
		}catch(Exception e){
			if(Constants.DEBUG_MODE){
				Log.d(Constants.TAG, "Exception: " + e.toString());
			}
		}
		
		return result;
		
		/*
		HttpResponse response  = null;
		
		String parseString = "";
		try {
		
			//byPassSSL();
			if(Constants.DEBUG_MODE){
	        	
	        }
			HttpPost httppost = new HttpPost(actionURL);
			String base64 = "";
	        
			String authorization = Constants.UN + ":" + Constants.PW;
	        base64 = Base64.encodeToString(authorization.getBytes(), Base64.DEFAULT);//.NO_WRAP);
	        httppost.setHeader("Authorization", "Basic " + base64);
	        httppost.setHeader("Content-Type", Constants.contentType);
	        
	        HttpClient httpsClient = getHTTPSClient(); //sslClient(new DefaultHttpClient());
			
			if(nameValuePairs!=null){
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
			}
			if(Constants.DEBUG_MODE){
	        	Log.d(Constants.TAG, "API URL: " + actionURL);
	        	Log.d(Constants.TAG, "Authorization: Basic " + base64);
	        	Log.d(Constants.TAG, "Content-Type: " + Constants.contentType);
		        Log.d(Constants.TAG, "httppost.getEntity().getContent().toString(): " + convertStreamToString(httppost.getEntity().getContent()));
				
			}
			
			response = httpsClient.execute(httppost);
			
			if (response != null) {
				parseString = EntityUtils.toString(response.getEntity(), "UTF-8");
			}
			/
			HttpResponse httpResponse = httpClient.execute(httppost);
			HttpEntity resEntity = httpResponse.getEntity();
			parseString = EntityUtils.toString(resEntity);	
			/
			
			if(Constants.DEBUG_MODE){
				Log.d(Constants.TAG, Utils.class.getSimpleName() + "-parseString: " + parseString);
				
			}
			
 			ParserHelper parser = new ParserHelper();
			Object object = parser.parseData(context, parseString, processID);
			return object;
			
			
		
		} catch (SocketTimeoutException e) {
			errorString = context.getString(R.string.error_message_connection_timeout);
			if(Constants.DEBUG_MODE){
				Log.d(Constants.TAG, Utils.class.getSimpleName() + "-SocketTimeoutException: " + e.toString());
				
			}
		} catch (ConnectTimeoutException e) {
			errorString = context.getString(R.string.error_message_connection_timeout);
			if(Constants.DEBUG_MODE){
				Log.d(Constants.TAG, Utils.class.getSimpleName() + "-ConnectTimeoutException: " + e.toString());
				
			}
		} catch (IOException e) {
			errorString = context.getString(R.string.error_message_server_error);//+ processID;
			if(Constants.DEBUG_MODE){
				Log.d(Constants.TAG, Utils.class.getSimpleName() + "-IOException: " + e.toString());
				
			}
		} catch (Exception e) {
			errorString = context.getString(R.string.error_message_no_internet);
			if(Constants.DEBUG_MODE){
				Log.d(Constants.TAG, Utils.class.getSimpleName() + "-Exception: " + e.toString());
				
			}
		}
		
		return null;
		*/
	}
	
	private String convertStreamToString(InputStream is) throws Exception {
	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb = new StringBuilder();
	    String line = null;
	    while ((line = reader.readLine()) != null) {
	        sb.append(line);
	    }
	    is.close();
	    return sb.toString();
	}
	
	
	/*
	public static void byPassSSL() {
		System.setProperty("jsse.enableSNIExtension", "false");
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			@Override
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			@Override
			public void checkClientTrusted(X509Certificate[] certs,
					String authType) {
			}

			@Override
			public void checkServerTrusted(X509Certificate[] certs,
					String authType) {
			}

		} };

		SSLContext sc;
		try {
			sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection
					.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
		}

		HostnameVerifier allHostsValid = new HostnameVerifier() {
			@Override
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};

		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

	}

	private HttpClient sslClient(HttpClient client) {
		try {
			X509TrustManager tm = new X509TrustManager() {
				public void checkClientTrusted(X509Certificate[] xcs,
						String string) throws CertificateException {
				}

				public void checkServerTrusted(X509Certificate[] xcs,
						String string) throws CertificateException {
				}

				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}
			};
			SSLContext ctx = SSLContext.getInstance("TLS");
			ctx.init(null, new TrustManager[] { tm }, null);
			SSLSocketFactory ssf = new MySSLSocketFactory(ctx);
			ssf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			ClientConnectionManager ccm = client.getConnectionManager();
			SchemeRegistry sr = ccm.getSchemeRegistry();
			sr.register(new Scheme("https", ssf, 443));
			return new DefaultHttpClient(ccm, client.getParams());
		} catch (Exception ex) {
			return null;
		}
	}

	public class MySSLSocketFactory extends SSLSocketFactory {
		SSLContext sslContext = SSLContext.getInstance("TLS");

		public MySSLSocketFactory(KeyStore truststore)
				throws NoSuchAlgorithmException, KeyManagementException,
				KeyStoreException, UnrecoverableKeyException {
			super(truststore);

			TrustManager tm = new X509TrustManager() {
				public void checkClientTrusted(X509Certificate[] chain,
						String authType) throws CertificateException {
				}

				public void checkServerTrusted(X509Certificate[] chain,
						String authType) throws CertificateException {
				}

				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}
			};

			sslContext.init(null, new TrustManager[] { tm }, null);
		}

		public MySSLSocketFactory(SSLContext context)
				throws KeyManagementException, NoSuchAlgorithmException,
				KeyStoreException, UnrecoverableKeyException {
			super(null);
			sslContext = context;
		}

		public Socket createSocket(Socket socket, String host, int port,
				boolean autoClose) throws IOException, UnknownHostException {
			return sslContext.getSocketFactory().createSocket(socket, host,
					port, autoClose);
		}

		public Socket createSocket() throws IOException {
			return sslContext.getSocketFactory().createSocket();
		}
	}
	*/
}
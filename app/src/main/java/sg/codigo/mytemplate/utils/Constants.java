package sg.codigo.mytemplate.utils;

import sg.codigo.mytemplate.R;
import com.google.android.gms.maps.model.LatLng;

public class Constants {

	
	public final static String TAG = "myTemplate";
	
	public static boolean DEBUG_MODE = true;
	public static boolean IS_GA_ON = true;
	//public static boolean IS_STAG = false;		
	public static boolean IS_PRODUCTION = false;
	
	
	public static final int SPLASH_DISPLAY_TIME_MILLISECONDS = 3 * 1000;
	public static final int DEFAULT_CONNECTION_TIMEOUT = 15 * 1000;
	public static final int DEFAULT_SOCKET_TIMEOUT = 30 * 1000;
	public static final int TIMEOUT_IN_MILISECONDS = 5 * 1000;
	public static final int DELAY_1_SECS = 1 * 1000;
	public static final int DELAY_2_SECS = 2 * 1000;
	public static final int DELAY_3_SECS = 3 * 1000;
	public static final int MIN_TIME_TO_REFRESH_LOCATION = 1 * 60 * 60 * 1000;
	public static final int VIBRATION_DURATION = 2 * 1000;
	public static final int GA_REFRESH_INTERVAL = 15;  // 15 secs
	public static final int IMAGE_SAMPLE_SIZE = 4;
	public static final int IMAGE_SAMPLE_SIZE_DIMENSION = 8;
	
	//Animation delay
	public static final int animationTime = 300;
	public static final int animationTime02 = 5000;
	public static final int animationTime03 = 1000;
	
	private static int sideNavId = 0;
	private static int utilityId = 100;


	public static final int DEFAULT_SCROLL_DOWN_DIFF = 50;
	public static final int DEFAULT_SCROLL_UP_DIFF = 50;


	// FACEBOOK
	public static final String[] PERMISSIONS_READ = {"public_profile", "email", "user_birthday", "user_location", "user_about_me"}; //, "user_birthday", "user_location"};
	

	// GCM
	private static final String SENDER_ID_STAG = "";
	private static final String SENDER_ID_PROD = "";
	public static String getGCMSenderID(){
		if(IS_PRODUCTION){
			return SENDER_ID_PROD;
		}
		else{
			return SENDER_ID_STAG;
		}
	}

	// Google Map
	public static final String DEFAULT_COUNTRY_CODE = "SG";
	public static final int DEFAULT_SEARCH_DISTANCE = 1000; //public static final int DEFAULT_GOGGLE_SENSOR = "YES";
	public static final String DEFAULT_GOGGLE_ADDRESS_TYPE =
			"accounting%7C"
			+ "airport%7C"
			+ "amusement_park%7C"
			+ "aquarium%7C"
			+ "art_gallery%7C"
			+ "atm%7C"
			+ "bakery%7C"
			+ "bank%7C"
			+ "bar%7C"
			+ "beauty_salon%7C"
			+ "bicycle_store%7C"
			+ "book_store%7C"
			+ "bowling_alley%7C"
			+ "bus_station%7C"
			+ "cafe%7C"
			+ "campground%7C"
			+ "car_dealer%7C"
			+ "car_rental%7C"
			+ "car_repair%7C"
			+ "car_wash%7C"
			+ "casino%7C"
			+ "cemetery%7C"
			+ "church%7C"
			+ "city_hall%7C"
			+ "clothing_store%7C"
			+ "convenience_store%7C"
			+ "courthouse%7C"
			+ "dentist%7C"
			+ "department_store%7C"
			+ "doctor%7C"
			+ "electrician%7C"
			+ "electronics_store%7C"
			+ "embassy%7C"
			+ "establishment%7C"
			+ "finance%7C"
			+ "fire_station%7C"
			+ "florist%7C"
			+ "food%7C"
			+ "funeral_home%7C"
			+ "furniture_store%7C"
			+ "gas_station%7C"
			+ "general_contractor%7C"
			+ "grocery_or_supermarket%7C"
			+ "gym%7C"
			+ "hair_care%7C"
			+ "hardware_store%7C"
			+ "health%7C"
			+ "hindu_temple%7C"
			+ "home_goods_store%7C"
			+ "hospital%7C"
			+ "insurance_agency%7C"
			+ "jewelry_store%7C"
			+ "laundry%7C"
			+ "lawyer%7C"
			+ "library%7C"
			+ "liquor_store%7C"
			+ "local_government_office%7C"
			+ "locksmith%7C"
			+ "lodging%7C"
			+ "meal_delivery%7C"
			+ "meal_takeaway%7C"
			+ "mosque%7C"
			+ "movie_rental%7C"
			+ "movie_theater%7C"
			+ "moving_company%7C"
			+ "museum%7C"
			+ "night_club%7C"
			+ "painter%7C"
			+ "park%7C"
			+ "parking%7C"
			+ "pet_store%7C"
			+ "pharmacy%7C"
			+ "physiotherapist%7C"
			+ "place_of_worship%7C"
			+ "plumber%7C"
			+ "police%7C"
			+ "post_office%7C"
			+ "real_estate_agency%7C"
			+ "restaurant%7C"
			+ "roofing_contractor%7C"
			+ "rv_park%7C"
			+ "school%7C"
			+ "shoe_store%7C"
			+ "shopping_mall%7C"
			+ "spa%7C"
			+ "stadium%7C"
			+ "storage%7C"
			+ "store%7C"
			+ "subway_station%7C"
			+ "synagogue%7C"
			+ "taxi_stand%7C"
			+ "train_station%7C"
			+ "travel_agency%7C"
			+ "university%7C"
			+ "veterinary_care%7C"
			+ "zoo";
	
	public static final String API_GOOGLE_MAP_GEOCODE="https://maps.googleapis.com/maps/api/geocode/json?";
	//public static final String API_GOOGLE_MAP_PO="https://maps.googleapis.com/maps/api/place/search/json?";
	public static final String API_GOOGLE_SEARCH_TEXT="https://maps.googleapis.com/maps/api/place/textsearch/json?";
	public static final String API_GOOGLE_NEARBY_SEARCH_TEXT="https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
	public static final String API_GOOGLE_PLACE_DETAILS_TEXT="https://maps.googleapis.com/maps/api/place/details/json?";
	public static final int DEFAULT_ZOOM_LEVEL = 15;
	
	public static final LatLng DEFAULT_CENTER_POINT = new LatLng(1.357714, 103.819084);		// center of singapore map
	public static final LatLng DEFAULT_SOUTH_WEST_POINT = new LatLng(1.325794, 103.722267);		// west of singapore map
	public static final LatLng DEFAULT_NORTH_EAST_POINT = new LatLng(1.381397,103.944397);		// east of singapore map
	
	// Google developer key
	/*
	private static final String DEVELOPER_KEY_STAG = "";
	private static final String DEVELOPER_KEY_PROD = "";
	public static String getDeveloperKey(){
		if(IS_PRODUCTION){
			return DEVELOPER_KEY_PROD;
		}
		else{
			return DEVELOPER_KEY_STAG;
		}
	}
	*/
	
	/**
	 *   API
	 */
	// API
	public static final String UN ="apiuser";
	public static final String PW ="codpass108";

	public static final String contentType = "application/x-www-form-urlencoded;charset=UTF-8";
	public static final String DEVICE_TYPE = "android";
	private static final String API_VER = "1";
	
	private static final String URL_STAG = "";
	private static final String URL_PROD = "";
	private static String getURL(){
		if(IS_PRODUCTION){
			return URL_PROD;
		}else{
			return URL_STAG;
		}
	}
	private static final String URL_TNC_STAG = "";
	private static final String URL_TNC_PROD = "";
	public static String getURLTNC(){
		if(IS_PRODUCTION){
			return URL_TNC_PROD;
		}else{
			return URL_TNC_STAG;
		}
	}
	private static final String URL_PP_STAG = "";
	private static final String URL_PP_PROD = "";
	public static String getURLPP(){
		if(IS_PRODUCTION){
			return URL_PP_PROD;
		}else{
			return URL_PP_STAG;
		}
	}

	private static String appType = "Customer";
	public static final String API_REGISTER_DEVICE = getURL() + "/v" + API_VER + "/" + appType + "/registerDevice";
	public static final String API_POST_SEND_FEEDBACK = getURL() + "/v" + API_VER + "/" + appType + "/sendFeedback";


	// ERROR CODE
	public static final int ERROR_CODE_SUCCESS = 0;
	public static final int ERROR_CODE_FAILED = 400;
	
	// API ID
	public static final int ID_API_REGISTER_DEVICE = ++utilityId;
	public static final int ID_API_POST_SEND_FEEDBACK = ++utilityId;
	public static final int ID_API_GET_PROFILE = ++utilityId;
	public static final int ID_API_GET_MASTER_DATA = ++utilityId;
	public static final int ID_API_POST_UPDATE_DEVICE_ID = ++utilityId;

	// SIDE NAV
	public static final int SIDE_NAV_ARRANGE_DELIVERY = ++sideNavId;
	public static final int SIDE_NAV_DELIVERY_STATUS = ++sideNavId;
	public static final int SIDE_NAV_FAVOURITES = ++sideNavId;
	public static final int SIDE_NAV_HISTORY = ++sideNavId;
	public static final int SIDE_NAV_MY_RPOFILE = ++sideNavId;
	public static final int SIDE_NAV_FEEDBACK = ++sideNavId;
	public static final int SIDE_NAV_HELP = ++sideNavId;
	public static final int SIDE_NAV_ABOUT = ++sideNavId;
	
	// ACTION BAR

	public static boolean isShowActionMenu = false;
    public static boolean isShowActionAdd = false;
    public static boolean isShowActionLogout = false;


	// SCREEN
	public static final int SCREEN_SPLASH = ++utilityId;
	public static final int SCREEN_TNC = ++utilityId;
	public static final int SCREEN_MAIN = ++utilityId;
	public static final int SCREEN_LANDING = ++utilityId;
	public static final int SCREEN_REGISTER = ++utilityId;
	public static final int SCREEN_REGISTER_WITH_EMAIL = ++utilityId;
	public static final int SCREEN_SIGN_IN = ++utilityId;

	// ACTION
	//private static int actionId = 2000;
	public static final int ACTION_FINISH_ACTIVITY = ++utilityId;
	public static final int ACTION_TAKE_PHOTO_01 = ++utilityId;
	public static final int ACTION_ATTACH_PHOTO_01 = ++utilityId;
	public static final int ACTION_TAKE_PHOTO_02 = ++utilityId;
	public static final int ACTION_ATTACH_PHOTO_02 = ++utilityId;
	public static final int ACTION_TAKE_PHOTO_03 = ++utilityId;
	public static final int ACTION_ATTACH_PHOTO_03 = ++utilityId;
	public static final int ACTION_TAKE_PROFILE_PHOTO = ++utilityId;
	public static final int ACTION_ATTACH_PROFILE_PHOTO = ++utilityId;
	
	// POPUP
	//private static int popupId = 3000;
	public static final int POPUP_SWITCH_ON_GPS = ++utilityId;
	public static final int POPUP_ACTIVATION_CODE_RESENT = ++utilityId;
	public static final int POPUP_ACCOUNT_ACTIVATED = ++utilityId;
	public static final int POPUP_API_CALL_SUCCEED = ++utilityId;
	public static final int POPUP_API_CALL_FAILED = ++utilityId;
	public static final int POPUP_ACTION_CHOOSE_PROFILE_PHOTO = ++utilityId;
	public static final int POPUP_ACTION_CHANGE_PASSWORD = ++utilityId;
	public static final int POPUP_REDIRECT_TO_LANDING = ++utilityId;
	public static final int POPUP_APP_FORCE_UPDATE = ++utilityId;
	public static final int POPUP_APP_OPTIONAL_UPDATE = ++utilityId;


	// ERROR
	public static final int ERROR_CONNECTION = ++utilityId;
	
	
	// DIALOG BUTTON 
	//private static int dialogButtonId = 4000;
	public static final int DIALOG_BUTTON_LEFT = ++utilityId;
	public static final int DIALOG_BUTTON_RIGHT = ++utilityId;
	public static final int DIALOG_BUTTON_OK = ++utilityId;
	public static final int DIALOG_BUTTON_TAKE_PHOTO = ++utilityId;
	public static final int DIALOG_BUTTON_ATTACH_PHOTO = ++utilityId;

	// BUNDLE
	public static final String BUNDLE_STRING_EMAIL = "bundle_email";
	public static final String BUNDLE_STRING_PASSWORD = "bundle_password";


	// ACCOUNT TYPE
	public static final String ACCOUNT_TYPE_PERSONAL = "Personal\nAccount";

}

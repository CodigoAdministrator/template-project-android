package sg.codigo.mytemplate.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.params.ConnManagerPNames;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sg.codigo.mytemplate.R;
import sg.codigo.mytemplate.models.Country;
import sg.codigo.mytemplate.security_ssl.EasySSLSocketFactory;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.provider.Settings.Secure;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Base64;
import android.util.Base64InputStream;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Utils {
	public static Dialog pd;
	public static boolean isShowBackBtn = false;
	
	// GA
	private static GoogleAnalytics analytics;
	private static Tracker tracker;

	public static void updateGA(Context context, final String screenName){
		analytics = GoogleAnalytics.getInstance(context);
        analytics.setLocalDispatchPeriod(Constants.GA_REFRESH_INTERVAL);

        tracker = analytics.newTracker(context.getString(R.string.ga_tracking_id)); // Replace with actual tracker/property Id
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(false);
        tracker.enableAutoActivityTracking(false);
		
		tracker.setScreenName(screenName);
		tracker.send(new HitBuilders.ScreenViewBuilder().build());
	}
	
	@SuppressLint("NewApi")
	public static void updateActionBar(Context context, String title, boolean isShowActionBar, boolean isShowBackBtn, boolean isShowMenuBtn, Drawable backgroundDrawable){
		Utils.isShowBackBtn = isShowBackBtn;
		
    	if(isShowActionBar){
	    	if(Constants.DEBUG_MODE){
	    		Log.d(Constants.TAG, "title: " + title);
	    	}
	    	SpannableString s = new SpannableString(title);
	    	s.setSpan(new TypefaceSpan(context, "Avenir_Heavy.ttf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	    	ActionBar actionBar = ((Activity) context).getActionBar();
	    	actionBar.show();
	    	actionBar.setTitle(s);
    		
    		// Update the action bar title with the TypefaceSpan instance
	    	actionBar.setIcon(new ColorDrawable(context.getResources().getColor(android.R.color.transparent))); 
	    	actionBar.setBackgroundDrawable(backgroundDrawable);
	    	
	    	if(isShowBackBtn){
	    		if(getOSVersion() >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2){
	    			if(backgroundDrawable != null){
	    				actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_blue_24dp_with_left_padding);  // API 18 and above
	    				setActionBarTitleColor(context, context.getResources().getColor(R.color.blue_02));
	    				
	    	    	}else{
	    	    		actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp_with_left_padding);  // API 18 and above
	    	    		setActionBarTitleColor(context, context.getResources().getColor(R.color.white_01));
	    				
	    	    	}
	    		}else{
	    			if(backgroundDrawable != null){
	    				//actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_blue_24dp_with_left_padding);  // API 18 and above
	    				setActionBarTitleColor(context, context.getResources().getColor(R.color.blue_02));
	    				
	    	    	}else{
	    	    		//actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp_with_left_padding);  // API 18 and above
	    	    		setActionBarTitleColor(context, context.getResources().getColor(R.color.white_01));
	    				
	    	    	}
	    		}
	    		//actionBar.setIcon(R.drawable.ic_arrow_back_white_24dp_with_left_padding);
		    	actionBar.setDisplayHomeAsUpEnabled(isShowBackBtn);
	    	}
	    	
	    	if(isShowMenuBtn){
	    		if(getOSVersion() >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2){
	    			if(backgroundDrawable != null){
	    				actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp_with_left_padding);  // API 18 and above
	    				setActionBarTitleColor(context, context.getResources().getColor(R.color.blue_02));
	    				
	    	    	}else{
	    	    		actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp_with_left_padding);  // API 18 and above
	    	    		setActionBarTitleColor(context, context.getResources().getColor(R.color.white_01));
	    				
	    	    	}
	    		}else{
	    			if(backgroundDrawable != null){
	    				//actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp_with_left_padding);  // API 18 and above
	    				setActionBarTitleColor(context, context.getResources().getColor(R.color.blue_02));
	    				
	    	    	}else{
	    	    		//actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp_with_left_padding);  // API 18 and above
	    	    		setActionBarTitleColor(context, context.getResources().getColor(R.color.white_01));
	    				
	    	    	}
	    		}
	    		//actionBar.setIcon(R.drawable.ic_menu_white_24dp_with_left_padding);
		    	actionBar.setDisplayHomeAsUpEnabled(isShowMenuBtn);
	    	}
    	}else{
    		ActionBar actionBar = ((Activity) context).getActionBar();
    		actionBar.hide();
	    	//actionBar.setTitle("");
	    	//actionBar.setIcon(new ColorDrawable(context.getResources().getColor(android.R.color.transparent))); 
	    	
    		//((Activity) context).requestWindowFeature(Window.FEATURE_NO_TITLE);
    		//((Activity) context).getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            
    	}
    }
    
	public static void setActionBarTitle(Context context, String title, int color){
		SpannableString s = new SpannableString(title);
    	s.setSpan(new TypefaceSpan(context, "Avenir_Heavy.ttf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    	ActionBar actionBar = ((Activity) context).getActionBar();
    	actionBar.setTitle(s);
		
	}

	public static void setActionBarTitleColor(Context context, int color){
		int titleId;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
		      titleId = context.getResources().getIdentifier("action_bar_title", "id", "android");
		      TextView view = (TextView) ((Activity) context).findViewById(titleId);
			  view.setTextColor(color);
		} else {
		      //titleId = R.id.action_bar_title;
		}
		//TextView view = (TextView) ((Activity) context).findViewById(titleId);
		//view.setTextColor(color);
	}
	
	
	public static void showLoadingDialog(Context context){
		/*
		pd = new Dialog (context);
		pd.requestWindowFeature (Window.FEATURE_NO_TITLE);
		pd.setContentView(R.layout.dialog_progress);
     	pd.getWindow().setBackgroundDrawableResource (android.R.color.transparent);
     	pd.setCancelable(false);
     	pd.show();
     	*/
		pd = new Dialog (context);
		pd.requestWindowFeature (Window.FEATURE_NO_TITLE);
		pd.setContentView(R.layout.dialog_progress_new);
     	pd.getWindow().setBackgroundDrawableResource (android.R.color.transparent);
     	pd.setCancelable(false);
     	
     	ImageView ivLoading = (ImageView) pd.findViewById(R.id.preloader);
     	AnimationDrawable loadingAnimation = (AnimationDrawable) ivLoading.getDrawable();
     	loadingAnimation.start();
     	pd.show();
	}
	
	public static void showLoadingDialog(Context context, boolean cancelable){
		/*
		pd = new Dialog (context);
		pd.requestWindowFeature (Window.FEATURE_NO_TITLE);
		pd.setContentView(R.layout.dialog_progress);
     	pd.getWindow().setBackgroundDrawableResource (android.R.color.transparent);
     	pd.setCancelable(cancelable);
     	pd.show();
     	*/
		
		pd = new Dialog (context);
		pd.requestWindowFeature (Window.FEATURE_NO_TITLE);
		pd.setContentView(R.layout.dialog_progress_new);
     	pd.getWindow().setBackgroundDrawableResource (android.R.color.transparent);
     	pd.setCancelable(cancelable);
     	
     	ImageView ivLoading = (ImageView) pd.findViewById(R.id.preloader);
     	AnimationDrawable loadingAnimation = (AnimationDrawable) ivLoading.getDrawable();
     	loadingAnimation.start();
     	pd.show();
	}
	
	public static void showLoadingDialog(Context context, String title){
		/*
		pd = new Dialog (context);
		pd.setContentView(R.layout.dialog_progress);
		pd.setTitle(title);
     	pd.getWindow().setBackgroundDrawableResource (android.R.color.transparent);
     	pd.setCancelable(false);
     	pd.show();
     	*/
		pd = new Dialog (context);
		pd.requestWindowFeature (Window.FEATURE_NO_TITLE);
		pd.setContentView(R.layout.dialog_progress_new);
		pd.setTitle(title);
     	pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
     	pd.setCancelable(false);
     	
     	ImageView ivLoading = (ImageView) pd.findViewById(R.id.preloader);
     	AnimationDrawable loadingAnimation = (AnimationDrawable) ivLoading.getDrawable();
     	loadingAnimation.start();
     	pd.show();
	}
	
	public static boolean isShowingLoadingDialog(Context context){
		if(pd!=null){
			return pd.isShowing();
		}else{
			return false;
		}
	}
	
	public static void hideLoadingDialog(Context context){
		if(pd!=null && pd.isShowing()){
			pd.dismiss();
		}
	}
	
	private static int getOSVersion(){
		int currentAPIVersion = android.os.Build.VERSION.SDK_INT;
		return currentAPIVersion;
	}
	
	public static String getDeviceUniqueId(Context context){
		String deviceID = "";
		try{
			deviceID = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID); 
		}catch(Exception ex){
			
		}
		return deviceID;
	}
	
	public static String getPayloadHttpGet(String url, ArrayList<NameValuePair> nameValuePairs){
		if(nameValuePairs!=null){
			for(int i = 0; i < nameValuePairs.size(); i++){
				if(i == 0){
					url += "?" + nameValuePairs.get(i).getName() + "=" + nameValuePairs.get(i).getValue();
				}else{
					url += "&" + nameValuePairs.get(i).getName() + "=" + nameValuePairs.get(i).getValue();
				}
			}
			return url;
		}else{
			return url;
		}
		
	}

	/*
	public static String getDestinationListJSON(Address destinationAddr, WeightRange approximateWeight, String attentionTo, String contactNumber, String notes, String returnReason){
		
		String json = "";
		json += "[";
		json += "{";
		json += "\"approximate_id\":" + approximateWeight.getId();
		json += ",\"attention_to\":\"" + attentionTo + "\"";
		json += ",\"receiver_mobile\":\"" + contactNumber + "\"";
		json += ",\"note\":\"" + notes + "\"";
		json += ",\"reason_return\":\"" + returnReason + "\"";
		json += ",\"dest_address_display_name\":\"" + destinationAddr.getTitle() + "\"";
		json += ",\"dest_address\":\"" + destinationAddr.getFullAddress() + "\"";
		json += ",\"dest_unit_number\":\"" + destinationAddr.getUnitNumber() + "\"";
		json += ",\"dest_postal_code\":\"" + destinationAddr.getPostalCode() + "\"";
		json += ",\"dest_address_latitude\":\"" + String.format("%.6f", destinationAddr.getLat()) + "\"";
		json += ",\"dest_address_longitude\":\"" + String.format("%.6f", destinationAddr.getLon()) + "\"";
		json += "}";
		json += "]";
		
		
		return json;
	}
	*/

	public static List<Country> getCountryList(){
		List<Country> countryList = new ArrayList<Country>();
		countryList.add(new Country(1, "Singapore"));
		//countryList.add(new Country(2, "Malaysia"));

		return countryList;
	}

	public static String toTitleCase(String input) {
	    StringBuilder titleCase = new StringBuilder();
	    boolean nextTitleCase = true;

	    for (char c : input.toCharArray()) {
	        if (Character.isSpaceChar(c)) {
	            nextTitleCase = true;
	        } else if (nextTitleCase) {
	            c = Character.toTitleCase(c);
	            nextTitleCase = false;
	        }

	        titleCase.append(c);
	    }

	    return titleCase.toString();
	}
	
	@SuppressLint("NewApi")
	private Bitmap blurRenderScript(Context context, Bitmap smallBitmap, float blurRadius) {

	    Bitmap output = Bitmap.createBitmap(smallBitmap.getWidth(), smallBitmap.getHeight(), smallBitmap.getConfig());

	    RenderScript rs = RenderScript.create(context);
	    ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
	    Allocation inAlloc = Allocation.createFromBitmap(rs, smallBitmap, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_GRAPHICS_TEXTURE);
	    Allocation outAlloc = Allocation.createFromBitmap(rs, output);
	    script.setRadius(blurRadius);
	    script.setInput(inAlloc);
	    script.forEach(outAlloc);
	    outAlloc.copyTo(output);

	    rs.destroy();

	    //MutableBitmap.delete(smallBitmap);

	    return output;
	}
	
	public static boolean isValidString(String string) {
		if(string == null){
			return false;
		}else{
			string = string.trim();
			if(string == "" || string.isEmpty())
				return false;
			
			else
				return true;
		}
	}
	
	public static boolean isValidDigit(int digit) {
		String data = String.valueOf(digit);
		for(int i=0;i<data.length();i++)
		{
			 if (!Character.isDigit(data.charAt(i)))
	                return false;
	        	
		}
		return true;
	}
	
	public static boolean isValidDigit(String data) {
		//String data = String.valueOf(digit);
		for(int i=0;i<data.length();i++)
		{
			 if (!Character.isDigit(data.charAt(i)))
	                return false;
	        	
		}
		return true;
	}
	
	public static boolean isValidPhone(String data) {
		//String data = String.valueOf(digit);
		/*
		for(int i=0;i<data.length();i++)
		{
			if(i==0){
				if(data.charAt(i)!='+' && !Character.isDigit(data.charAt(i))){
					return false;
				}
			}
			 if (!Character.isDigit(data.charAt(i)))
	                return false;
	        	
		}
		*/
		if(data.length()<7){
			return false;
		}else{
			return true;
		}
	}
	
	public static boolean emailValidation(String email){
	   String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	   
	   if(email.matches(regex)){
		   return true;
	   }else{
		   return false;
	   }
	}
	
	public static Typeface getFontAvenirBook(Context context) {
		Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir_Book.ttf"); 
		return tf;
	}
	
	public static Typeface getFontAvenirBlack(Context context) {
		Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir_Black.ttf"); 
		return tf;
	}
	
	public static Typeface getFontAvenirHeavy(Context context) {
		Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir_Heavy.ttf"); 
		return tf;
	}
	
	public static Typeface getFontAvenirLight(Context context) {
		Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir_Light.ttf"); 
		return tf;
	}
	
	public static Typeface getFontAvenirMedium(Context context) {
		Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir_Medium.ttf"); 
		return tf;
	}
	
	/*
	public static Typeface getFontBold(Context context) {
		Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Brandon_bld.otf");
		return tf;
	}
	
	public static Typeface getFontBlack(Context context) {
		Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Brandon_blk.otf");
		return tf;
	}
	
	public static Typeface getFontLight(Context context) {
		Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Brandon_light.otf");
		return tf;
	}
	
	public static Typeface getFontMedium(Context context) {
		Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Brandon_med.otf");
		return tf;
	}

	public static Typeface getFontRegular(Context context) {
		Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Brandon_reg.otf");
		return tf;
	}
	
	public static Typeface getFontThin(Context context) {
		Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Brandon_thin.otf");
		return tf;
	}
	*/
	
	public static final boolean isConnectedToInternet(Context context) {
		NetworkInfo info = getActiveNetwork(context);
		return info != null ? info.isConnected() : false;
	}

	public static NetworkInfo getActiveNetwork(Context context) {
		ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
		return connManager != null ? connManager.getActiveNetworkInfo() : null;
	}
	
	public static void hideSoftKeyboard(Activity activity) {
        if(activity!=null && activity.getCurrentFocus()!=null && activity.getCurrentFocus().getWindowToken()!=null){
        	InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
	        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }
		
	//public static Context mContext = null;
	//public static String logPayload = "";
	
	/*
	public static Runnable rPostSaveLogs = new Runnable()
    {
		
        public void run() 
        {
        	//Looper.prepare();
        	new PostSaveLogsAPICall().execute(mContext, logPayload);
        }
    };
    */
	
	/*
	public static String getMimeTypeOfFile(String pathName) {
	    BitmapFactory.Options opt = new BitmapFactory.Options();
	    opt.inJustDecodeBounds = true;
	    BitmapFactory.decodeFile(pathName, opt);
	    return opt.outMimeType;
	}
	
	public static String getMimeType(Context context, Uri uriImage)
	{
	    String strMimeType = null;

	    Cursor cursor = context.getContentResolver().query(uriImage,
	                        new String[] { MediaStore.MediaColumns.MIME_TYPE },
	                        null, null, null);

	    if (cursor != null && cursor.moveToNext())
	    {
	        strMimeType = cursor.getString(0);
	    }

	    return strMimeType;
	}
	*/
	
	public static File createDir(Activity activity){
		String state = Environment.getExternalStorageState();
		File myDir;
    	if (Environment.MEDIA_MOUNTED.equals(state)) {
			myDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + Constants.TAG);  
			if(!myDir.exists()){
				myDir.mkdirs();
			}
    	
		}else{
    		myDir = new File(activity.getFilesDir() + "/" + Constants.TAG);  
			if(!myDir.exists()){
				myDir.mkdirs();
			}
    	}
		return myDir;
	}
	
	public static File createTempFile(Activity activity){
		String state = Environment.getExternalStorageState();
		File myFile;
    	if (Environment.MEDIA_MOUNTED.equals(state)) {
    		myFile = new File(Environment.getExternalStorageDirectory(), Constants.TAG);  
			
		}else{
			myFile = new File(activity.getFilesDir(), Constants.TAG);  
			
    	}
		return myFile;
	}
	
	public static void deleteDir(Activity activity){
		String state = Environment.getExternalStorageState();
		File myDir;
    	if (Environment.MEDIA_MOUNTED.equals(state)) {
			myDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + Constants.TAG);  
			if(myDir.exists()){
				myDir.delete();
			}
    	
		}else{
    		myDir = new File(activity.getFilesDir() + "/" + Constants.TAG);  
			if(myDir.exists()){
				myDir.delete();
			}
    	}
	}
	
	public static void copyStream(InputStream input, OutputStream output) throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

	
	public static String encodeTobase64(Bitmap image) //String filePath) //Bitmap image)
	{
		/*
		//calculate how many bytes our image consists of.
		int bytes = image.getByteCount();
		//or we can calculate bytes this way. Use a different value than 4 if you don't use 32bit images.
		//int bytes = b.getWidth()*b.getHeight()*4; 

		ByteBuffer buffer = ByteBuffer.allocate(bytes); //Create a new buffer
		image.copyPixelsToBuffer(buffer); //Move the byte data to the buffer

		byte[] array = buffer.array(); //Get the underlying array containing the data.
		*/
		
		
		//	current version
	    Bitmap imagex=image;
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();  
	    imagex.compress(Bitmap.CompressFormat.PNG, 100, baos);
	    byte[] array = baos.toByteArray();
	    
	    String image64 = Base64.encodeToString(array, Base64.DEFAULT);
		
        return image64;
	    
		
		/*
		try {
			InputStream inputStream = new FileInputStream(filePath);//You can get an inputStream using any IO API
			byte[] bytes;
			byte[] buffer = new byte[8192];
			int bytesRead;
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			
			
		    while ((bytesRead = inputStream.read(buffer)) != -1) {
		    	output.write(buffer, 0, bytesRead);
		    }
		    
		    bytes = output.toByteArray();
			String image64 = Base64.encodeToString(bytes, Base64.DEFAULT);
			
			inputStream.close();
			output.close();
			
			return image64;
		} catch (IOException e) {
			if(Constants.DEBUG_MODE){
				e.printStackTrace();
				Log.e(Constants.TAG, "IOException: " + e);
			}
			return "";
		} catch (Exception e) {
			if(Constants.DEBUG_MODE){
				e.printStackTrace();
				Log.e(Constants.TAG, "Exception: " + e);
			}
			return "";
		} 
		*/
	}
	
	public static Bitmap decodeBase64(String input) 
	{
	    byte[] decodedByte = Base64.decode(input, Base64.DEFAULT);
	    return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length); 
	}
	
	
	public static Uri getImageUri(Context inContext, Bitmap inImage) {
	  ByteArrayOutputStream bytes = new ByteArrayOutputStream();
	  inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
	  String path = Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
	  return Uri.parse(path);
	}
	
	
	public static String callWSGet(String url, String module, String method, String payload) throws ClientProtocolException, IOException, ParseException, Exception{
		HttpClient httpsClient;
		HttpGet httpget;
		
		HttpResponse response  = null;
		String ret = "";				
		
//		httpsClient = new DefaultHttpClient();
		httpsClient = getHTTPSClient();

		String request = url;
		if(!module.equalsIgnoreCase("") && !method.equalsIgnoreCase("")){
			request += "/" +  module + "/" + method;
		}
		if(!payload.equalsIgnoreCase("")){
			request += "?" + payload;
		}
		
		if(Constants.DEBUG_MODE){
			Log.d(Constants.TAG, Utils.class.getSimpleName() + "-request: " + request);
		}
		
		//try {
			//httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY,CookiePolicy.RFC_2109);
			httpget = new HttpGet(request);
			httpget.setHeader("Content-Type", Constants.contentType);
			
			response = httpsClient.execute(httpget);
			//System.out.println("??**"+response.toString());

			if (response != null) {
				ret = EntityUtils.toString(response.getEntity(), "UTF-8");
				//System.out.println("Response: "+ret);
				//System.out.println("#############################################");

			}
		/*
		}catch(Exception e){
			//e.printStackTrace();
			if(Constants.DEBUG_MODE){
				Log.d(Constants.TAG, "Exception: " + e.toString());
			}
			
		}
		*/
		//ret = ret.replaceAll("&lt;", "<");
		//ret = ret.replaceAll("&gt;", ">");
		return ret;
	}
	
	public static String callWS(String url, String module, String method, String payload) throws UnsupportedEncodingException, ClientProtocolException, IOException, ParseException, Exception{
		HttpClient httpsClient;
		HttpPost httppost;
		
		HttpResponse response  = null;
		String ret = "";				
		
//		httpsClient = new DefaultHttpClient();
		httpsClient = getHTTPSClient();

		String request = url;
		if(!module.equalsIgnoreCase("") && !method.equalsIgnoreCase("")){
			request += "/" +  module + "/" + method;
		}else if(!module.equalsIgnoreCase("")){
			request += "/" +  module;
		}else if(!method.equalsIgnoreCase("")){
			request += "/" +  method;
		}
		
		if(Constants.DEBUG_MODE){
			Log.d(Constants.TAG, Utils.class.getSimpleName() + "-request: " + request);
		}
		
		//try {
			//httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY,CookiePolicy.RFC_2109);
			httppost = new HttpPost(request);
			httppost.setHeader("Content-Type", Constants.contentType);
			if(payload!=null && !payload.equalsIgnoreCase("")){
				httppost.setEntity(new StringEntity(payload, HTTP.UTF_8));
			}
			response = httpsClient.execute(httppost);
			//System.out.println("??**"+response.toString());

			if (response != null) {
				ret = EntityUtils.toString(response.getEntity(), "UTF-8");
				//System.out.println("Response: "+ret);
				//System.out.println("#############################################");

			}
		/*	
		}catch(SocketTimeoutException e){
			//e.printStackTrace();
			if(Constants.DEBUG_MODE){
				Log.d(Constants.TAG, "SocketTimeoutException: " + e.toString());
			}
		}catch(Exception e){
			//e.printStackTrace();
			if(Constants.DEBUG_MODE){
				Log.d(Constants.TAG, "Exception: " + e.toString());
			}
		}
		*/
			
		//ret = ret.replaceAll("&lt;", "<");
		//ret = ret.replaceAll("&gt;", ">");
		return ret;
	}
	
	public String callPostJson(String url,ArrayList<NameValuePair> nameValuePairs ) throws ClientProtocolException, IOException, ParseException, Exception{
		HttpClient httpsClient;
		HttpPost httppost;
		
		HttpResponse response  = null;
		String ret = "";				
		
//		httpsClient = new DefaultHttpClient();
		httpsClient = getHTTPSClient();
		/*
		try {
			//httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY,CookiePolicy.RFC_2109);
			httppost = new HttpPost(url);
			httppost.setHeader("Content-Type","text/xml");
			if(nameValuePairs!=null){
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			}
			response = httpsClient.execute(httppost);
			//System.out.println("??**"+response.toString());

			if (response != null) {
				ret = EntityUtils.toString(response.getEntity());
				//System.out.println("Response: "+ret);
				//System.out.println("#############################################");

			}
		}catch(Exception e){
			e.printStackTrace();
		}
		ret = ret.replaceAll("&lt;", "<");
		ret = ret.replaceAll("&gt;", ">");
		return ret;
		*/
		String request = url;
		
		if(Constants.DEBUG_MODE){
			Log.d(Constants.TAG, Utils.class.getSimpleName() + "-request: " + request);
			//Log.d(Constants.TAG, Utils.class.getSimpleName() + "-entity: " + new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8).toString());
			
		}
		
		try{
			
			httppost = new HttpPost(request);
			if(nameValuePairs!=null){
				if(Constants.DEBUG_MODE){
					for(int i=0; i < nameValuePairs.size(); i++){
						Log.d(Constants.TAG, nameValuePairs.get(i).getName() + "-" + nameValuePairs.get(i).getValue());
					}
				}
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8)); //new StringEntity(body, HTTP.UTF_8));//);
			}
			
			//HttpParams httpParameters = new BasicHttpParams();
			HttpClient httpclient = sslClient(new DefaultHttpClient());

			HttpResponse httpresponse = httpclient.execute(httppost);
			HttpEntity resEntity = httpresponse.getEntity();

			ret = EntityUtils.toString(resEntity);			
			
		}catch(Exception e){
			e.printStackTrace();
			if(Constants.DEBUG_MODE){
				Log.e(Constants.TAG, "Exception: " + e.toString());
			}
		}
		
		return ret;
		
	}
	
	public static HttpClient sslClient(HttpClient client) {
		try {
			X509TrustManager tm = new X509TrustManager() {
				public void checkClientTrusted(X509Certificate[] xcs,
						String string) throws CertificateException {
				}

				public void checkServerTrusted(X509Certificate[] xcs,
						String string) throws CertificateException {
				}

				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}
			};
			SSLContext ctx = SSLContext.getInstance("TLS");
			ctx.init(null, new TrustManager[] { tm }, null); //new java.security.SecureRandom());
			//HttpsURLConnection.setDefaultSSLSocketFactory(ctx.getSocketFactory());
			
			SSLSocketFactory ssf = new MySSLSocketFactory(ctx);
			ssf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			ClientConnectionManager ccm = client.getConnectionManager();
			SchemeRegistry sr = ccm.getSchemeRegistry();
			sr.register(new Scheme("https", ssf, 443));
			return new DefaultHttpClient(ccm, client.getParams());
		} catch (Exception ex) {
			return null;
		}
	}

	public static class MySSLSocketFactory extends SSLSocketFactory {
		SSLContext sslContext = SSLContext.getInstance("TLS");

		public MySSLSocketFactory(KeyStore truststore)
				throws NoSuchAlgorithmException, KeyManagementException,
				KeyStoreException, UnrecoverableKeyException {
			super(truststore);

			TrustManager tm = new X509TrustManager() {
				public void checkClientTrusted(X509Certificate[] chain,
						String authType) throws CertificateException {
				}

				public void checkServerTrusted(X509Certificate[] chain,
						String authType) throws CertificateException {
				}

				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}
			};

			sslContext.init(null, new TrustManager[] { tm }, null);
		}

		public MySSLSocketFactory(SSLContext context)
				throws KeyManagementException, NoSuchAlgorithmException,
				KeyStoreException, UnrecoverableKeyException {
			super(null);
			sslContext = context;
		}

		public Socket createSocket(Socket socket, String host, int port,
				boolean autoClose) throws IOException, UnknownHostException {
			return sslContext.getSocketFactory().createSocket(socket, host,
					port, autoClose);
		}

		public Socket createSocket() throws IOException {
			return sslContext.getSocketFactory().createSocket();
		}
	}
	
	public static String callWSAdvertisement(Context context, String url, String module, String method, String payload) throws ClientProtocolException, IOException, ParseException, Exception{
		
		HttpClient httpsClient;
		HttpGet httpget;
		
		HttpResponse response  = null;
		String ret = "";				
		
//		httpsClient = new DefaultHttpClient();
		httpsClient = sslClient(new DefaultHttpClient()); //getHTTPSClient();

		String request = url;
		if(!module.equalsIgnoreCase("") && !method.equalsIgnoreCase("")){
			request += "/" +  module + "/" + method;
		}
		if(!payload.equalsIgnoreCase("")){
			request += "/" + payload;
		}
		
		if(Constants.DEBUG_MODE){
			Log.d(Constants.TAG, Utils.class.getSimpleName() + "-request: " + request);
		}
		
		//try {
			//httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY,CookiePolicy.RFC_2109);
			httpget = new HttpGet(request);
			httpget.setHeader("Content-Type", Constants.contentType);
			
			response = httpsClient.execute(httpget);
			//System.out.println("??**"+response.toString());

			if (response != null) {
				ret = EntityUtils.toString(response.getEntity(), "UTF-8");
				//System.out.println("Response: "+ret);
				//System.out.println("#############################################");

			}
		/*
		}catch(Exception e){
			//e.printStackTrace();
			if(Constants.DEBUG_MODE){
				Log.d(Constants.TAG, "Exception: " + e.toString());
			}
			
		}
		*/
		//ret = ret.replaceAll("&lt;", "<");
		//ret = ret.replaceAll("&gt;", ">");
			
		return ret;
		
		
		/*
		String request = url;
		if(!module.equalsIgnoreCase("") && !method.equalsIgnoreCase("")){
			request += "/" +  module + "/" + method;
		}
		if(!payload.equalsIgnoreCase("")){
			request += "/" + payload;
		}
		
		if(Constants.DEBUG_MODE){
			Log.d(Constants.TAG, Utils.class.getSimpleName() + "-request: " + request);
		}
		
		// Load CAs from an InputStream
		// (could be from a resource or ByteArrayInputStream or ...)
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		// From https://www.washington.edu/itconnect/security/ca/load-der.crt
		InputStream caInput = new BufferedInputStream(context.getAssets().open("load-der.crt"));
		Certificate ca;
		try {
		    ca = cf.generateCertificate(caInput);
		    System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
		} finally {
		    caInput.close();
		}

		// Create a KeyStore containing our trusted CAs
		String keyStoreType = KeyStore.getDefaultType();
		KeyStore keyStore = KeyStore.getInstance(keyStoreType);
		keyStore.load(null, null);
		keyStore.setCertificateEntry("ca", ca);

		// Create a TrustManager that trusts the CAs in our KeyStore
		String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
		tmf.init(keyStore);

		// Create an SSLContext that uses our TrustManager
		SSLContext sslContext = SSLContext.getInstance("TLS");
		sslContext.init(null, tmf.getTrustManagers(), null);

		// Tell the URLConnection to use a SocketFactory from our SSLContext
		URL url2 = new URL(request);
		HttpsURLConnection urlConnection =
		    (HttpsURLConnection)url2.openConnection();
		urlConnection.setSSLSocketFactory(sslContext.getSocketFactory());
		
		InputStream in = urlConnection.getInputStream();
		//copyInputStreamToOutputStream(in, System.out);
		
		return inputStreamToString(in);
		*/
	}
	
	public static String postData()  throws UnsupportedEncodingException, ClientProtocolException, IOException, ParseException, Exception{
	    // Create a new HttpClient and Post Header
	    HttpClient httpclient = sslClient(new DefaultHttpClient()); //new DefaultHttpClient();
	    HttpPost httppost = new HttpPost("https://uat.mysam.sg/restful-services/advertisementServices/incrementClickCount/141030105616123");

	    
        // Execute HTTP Post Request
        HttpResponse response = httpclient.execute(httppost);
        int code = response.getStatusLine().getStatusCode();
		
		//System.out.println("??**"+response.toString());
		
        
		return String.valueOf(code);
	    
	} 
	
	public static String callWSAdvertisementClickCount(String url, String module, String method, String payload) throws UnsupportedEncodingException, ClientProtocolException, IOException, ParseException, Exception{
		HttpClient httpsClient;
		HttpPost httppost;
		
		HttpResponse response  = null;
		String ret = "";				
		int code = -1;
		
//		httpsClient = new DefaultHttpClient();
		httpsClient = sslClient(new DefaultHttpClient()); //getHTTPSClient();

		String request = url;
		if(!module.equalsIgnoreCase("") && !method.equalsIgnoreCase("")){
			request += "/" +  module + "/" + method;
		}else if(!module.equalsIgnoreCase("")){
			request += "/" +  module;
		}else if(!method.equalsIgnoreCase("")){
			request += "/" +  method;
		}
		if(!payload.equalsIgnoreCase("")){
			request += "/" + payload;
		}
		
		if(Constants.DEBUG_MODE){
			Log.d(Constants.TAG, Utils.class.getSimpleName() + "-request: " + request);
		}
		
		//try {
			//httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY,CookiePolicy.RFC_2109);
			httppost = new HttpPost(request);
			
			//httppost.setHeader("Content-Type", Constants.contentType);
			/*
			if(payload!=null && !payload.equalsIgnoreCase("")){
				httppost.setEntity(new StringEntity(payload, HTTP.UTF_8));
			}
			*/
			
			response = httpsClient.execute(httppost);
			code = response.getStatusLine().getStatusCode();
			
			//System.out.println("??**"+response.toString());
			/*
			if(code == 204){
				ret = "204";
			}
			if (response != null) {
				ret = EntityUtils.toString(response.getEntity(), "UTF-8");
				//System.out.println("Response: "+ret);
				//System.out.println("#############################################");

			}
			*/
			ret = String.valueOf(code);
		/*	
		}catch(SocketTimeoutException e){
			//e.printStackTrace();
			if(Constants.DEBUG_MODE){
				Log.d(Constants.TAG, "SocketTimeoutException: " + e.toString());
			}
		}catch(Exception e){
			//e.printStackTrace();
			if(Constants.DEBUG_MODE){
				Log.d(Constants.TAG, "Exception: " + e.toString());
			}
		}
		*/
			
		//ret = ret.replaceAll("&lt;", "<");
		//ret = ret.replaceAll("&gt;", ">");
		return ret;
	}
	
	public static boolean isConnectionAvailable(Context context) {
		try {
			final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			final NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			final NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			if (wifi.isAvailable() && wifi.isConnected()) {
				return true;
			} else if (mobile.isAvailable() && mobile.isConnected()) {
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			//e.printStackTrace();
			if(Constants.DEBUG_MODE){
				Log.d(Constants.TAG, "Exception: " + e.toString());
			}
		}
		return false;
	}
	
	
	public static DefaultHttpClient getHTTPSClient() {
		SchemeRegistry schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
		schemeRegistry.register(new Scheme("https", new EasySSLSocketFactory(), 443));

		HttpParams params = new BasicHttpParams();
		params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
		params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE, new ConnPerRouteBean(30));
		params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
		
		HttpConnectionParams.setConnectionTimeout(params, Constants.DEFAULT_CONNECTION_TIMEOUT);
	    HttpConnectionParams.setSoTimeout(params, Constants.DEFAULT_SOCKET_TIMEOUT);
	    ConnManagerParams.setTimeout(params, Constants.DEFAULT_SOCKET_TIMEOUT);
	    HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);

		ClientConnectionManager cm = new SingleClientConnManager(params, schemeRegistry);
		DefaultHttpClient httpClient= new DefaultHttpClient(cm, params);
		return httpClient;
	}
    
	public static String getResultHttpGet(Context context, String url, String moduleName, String methodName) throws SocketException, SocketTimeoutException, ConnectTimeoutException, UnsupportedEncodingException, URISyntaxException, ClientProtocolException, IOException, Exception{
		
		String request = ""; //Utils.URL + "/" + moduleName + "/" + methodName ;
		if(!methodName.equalsIgnoreCase("")){
			request = url + "/" + moduleName + "/" + methodName ;
		}else{
			request = url + "/" + moduleName;
		}  
		
		if(Constants.DEBUG_MODE){
			Log.d(Constants.TAG, Utils.class.getSimpleName() + "-request: " + request);
		}
		String ret = Utils.httpGet(context, request);
		
        if(ret != null)
        {
        	return ret;
        }
        else{
        	if(Constants.DEBUG_MODE){
        		Log.d(Constants.TAG, Utils.class.getSimpleName() + "-no return");
        	}
        }
        return null;
	}
	
	public static String httpGet(Context context, String request) throws SocketException, SocketTimeoutException, ConnectTimeoutException, UnsupportedEncodingException, URISyntaxException, ClientProtocolException, IOException, Exception{
		String valuePairs = "";
        String key = "";
		//BufferedReader in = null;
        //try {
        	
            HttpParams httpParameters = new BasicHttpParams();
            HttpProtocolParams.setVersion(httpParameters, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(httpParameters, HTTP.UTF_8);
            HttpProtocolParams.setHttpElementCharset(httpParameters, HTTP.UTF_8);

            HttpConnectionParams.setConnectionTimeout(httpParameters, Constants.DEFAULT_CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpParameters, Constants.DEFAULT_SOCKET_TIMEOUT);
            ConnManagerParams.setTimeout(httpParameters, Constants.DEFAULT_SOCKET_TIMEOUT);
            
            //DefaultHttpClient httpclient = new DefaultHttpClient(cm, httpParameters);
            HttpClient client = new DefaultHttpClient(httpParameters);
            
            HttpGet httpGet = new HttpGet();
            httpGet.getParams().setParameter(HttpConnectionParams.SO_TIMEOUT, Constants.DEFAULT_SOCKET_TIMEOUT);
			
            //String value = replaceAllIllegalChars(valuePairs);
            key = request+""+valuePairs;
	        
            httpGet.setURI(new URI(request+""+valuePairs));
	        
	        if(Constants.DEBUG_MODE){
	        	Log.d(Constants.TAG, Utils.class.getSimpleName() + "-" + httpGet.getRequestLine().toString());
	        	
	        }
	        
	        
            BasicHttpResponse httpResponse = (BasicHttpResponse) client.execute(httpGet);
            if(httpResponse!=null){
            	if(Constants.DEBUG_MODE){
    	        	Log.d(Constants.TAG, Utils.class.getSimpleName() + "-key: " + key);
    	        	//Log.d(Utils.TAG, Utils.class.getSimpleName() + "-httpResponse.getEntity().toString(): " + httpResponse.getEntity().toString());
    	        }
            	//InputStream in = httpResponse.getEntity().getContent(); //Get the data in the entity
                String result = EntityUtils.toString(httpResponse.getEntity(), "UTF-8"); //inputStreamToString(in);
                
                if(Constants.DEBUG_MODE){
    	        	Log.d(Constants.TAG, Utils.class.getSimpleName() + "-result: " + result);
    	        }
    	        
                return result;
	        }
            //return db.getDataByKey(key);
        /*    
        } catch (SocketException e) {
			//e.printStackTrace();
			if(e!=null){
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, Utils.class.getSimpleName() + "-SocketException: " + e.toString());
				}
			}
		}
        catch (SocketTimeoutException e) 
        {
        	if(e!=null){
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, Utils.class.getSimpleName() + "-SocketTimeoutException: " + e.toString());
				}
			}
        }
        catch (ConnectTimeoutException e)
        {
        	if(e!=null){
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, Utils.class.getSimpleName() + "-ConnectTimeoutException: " + e.toString());
				}
			}
        }
        catch (UnsupportedEncodingException e) {
			//e.printStackTrace();
			if(e!=null){
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, Utils.class.getSimpleName() + "-UnsupportedEncodingException: " + e.toString());
				}
			}
		} catch (URISyntaxException e) {
			//e.printStackTrace();
			if(e!=null){
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, Utils.class.getSimpleName() + "-URISyntaxException: " + e.toString());
				}
			}
		} catch (ClientProtocolException e) {
			//e.printStackTrace();
			if(e!=null){
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, Utils.class.getSimpleName() + "-ClientProtocolException: " + e.toString());
				}
			}
		} catch (IOException e) {
			//e.printStackTrace();
			if(e!=null){
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, Utils.class.getSimpleName() + "-IOException: " + e.toString());
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
			if(e!=null){
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, Utils.class.getSimpleName() + "-Exception: " + e.toString());
				}
			}
		} 
        */
        return null;
	}
	
	
	private static String inputStreamToString(InputStream is) {
	    String line = "";
	    StringBuilder total = new StringBuilder();
	    
	    // Wrap a BufferedReader around the InputStream
	    BufferedReader rd = new BufferedReader(new InputStreamReader(is), 65728);

	    // Read response until the end
	    try {
			while ((line = rd.readLine()) != null) { 
			    total.append(line); 
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e!=null){
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, Utils.class.getSimpleName() + "-IOException:" + e.toString());
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if(e!=null){
				if(Constants.DEBUG_MODE){
					Log.e(Constants.TAG, Utils.class.getSimpleName() + "-Exception:" + e.toString());
				}
			}
		}
	    
	    // Return full string
	    return total.toString();
	}
	
	
	/**
     * Trust every server - dont check for any certificate
     */
    public static void trustAllHosts() {
	      // Create a trust manager that does not validate certificate chains
	      TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
	              public java.security.cert.X509Certificate[] getAcceptedIssuers() {
	                      return new java.security.cert.X509Certificate[] {};
	              }
	
	              public void checkClientTrusted(X509Certificate[] chain,
	                              String authType) throws CertificateException {
	              }
	
	              public void checkServerTrusted(X509Certificate[] chain,
	                              String authType) throws CertificateException {
	              }
	      } };
	
	      // Install the all-trusting trust manager
	      try {
	              SSLContext sc = SSLContext.getInstance("TLS");
	              sc.init(null, trustAllCerts, new java.security.SecureRandom());
	              HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	      } catch (Exception e) {
	              e.printStackTrace();
	      }
    }
    
	public static int convertDPToPixel(Context context, float f){
		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		return (int)((f * displayMetrics.density) + 0.5);
	}
	
	public static float pixelsToSp(Context context, Float px) {
	    float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
	    return px/scaledDensity;
	}
	
	private static String getDeviceModel() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			return model.toUpperCase();
		} else {
		    return manufacturer.toUpperCase() + " " + model.toUpperCase();
		}
	}
	
	/*
	public static void showAlertDialog(Context context, String title, final String message, String btnPositive, String btnNegative, String btnNeutral){
		
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_alert_02);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setCancelable(true);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		
		TextView dialogTitle = (TextView) dialog.findViewById(R.id.dialog_title);
		dialogTitle.setTypeface(Utils.getBoldFont(context));
		if(title!=null && !title.equalsIgnoreCase("")){
			dialogTitle.setText(title);
			dialogTitle.setVisibility(View.VISIBLE);
		}else{
			dialogTitle.setVisibility(View.GONE);
		}
		
		TextView dialogMessage = (TextView) dialog.findViewById(R.id.dialog_message);
		dialogMessage.setTypeface(Utils.getRegularFont(context));
		dialogMessage.setText(message);
		
		Button dialogPositiveButton = (Button) dialog.findViewById(R.id.dialog_btn_positive);
		dialogPositiveButton.setTypeface(Utils.getRegularFont(context));
		if(btnPositive.equalsIgnoreCase("")){
			dialogPositiveButton.setVisibility(View.GONE);
		}else{
			dialogPositiveButton.setVisibility(View.VISIBLE);
			dialogPositiveButton.setText(btnPositive);
		}
		dialogPositiveButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		
		Button dialogNegativeButton = (Button) dialog.findViewById(R.id.dialog_btn_negative);
		dialogNegativeButton.setTypeface(Utils.getBoldFont(context));
		if(btnNegative.equalsIgnoreCase("")){
			dialogNegativeButton.setVisibility(View.GONE);
		}else{
			dialogNegativeButton.setVisibility(View.VISIBLE);
			dialogNegativeButton.setText(btnNegative);
		}
		dialogNegativeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				
			}
		});
		
		Button dialogNeutralButton = (Button) dialog.findViewById(R.id.dialog_btn_neutral);
		dialogNeutralButton.setTypeface(Utils.getRegularFont(context));
		if(btnNeutral.equalsIgnoreCase("")){
			dialogNeutralButton.setVisibility(View.GONE);
		}else{
			dialogNeutralButton.setVisibility(View.VISIBLE);
			dialogNeutralButton.setText(btnNeutral);
		}
		dialogNeutralButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		
		if(isAppOnForeground(context)){
			dialog.show();
		}
    }
	
	public static boolean isAppOnForeground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
          return false;
        }
        final String packageName = context.getPackageName();
        for (RunningAppProcessInfo appProcess : appProcesses) {
          if (appProcess.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
            return true;
          }
        }
        return false;
    }
    */
}

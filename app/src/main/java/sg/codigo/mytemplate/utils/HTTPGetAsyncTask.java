package sg.codigo.mytemplate.utils;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.URI;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import sg.codigo.mytemplate.callbacks.JsonCallback;
import sg.codigo.mytemplate.R;
import sg.codigo.mytemplate.dialogs.DialogProgressBar;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

public class HTTPGetAsyncTask extends AsyncTask<String, String, String> {
	private Context context;
	//private DialogProgressBar progressDialog;
	private int processID;
	private JsonCallback callback;
	private String url;
	//private String errorString="";
	private boolean showProgressBar;
	private boolean isCancelable;
	
	public HTTPGetAsyncTask(Context context, String url, JsonCallback callback, int processID, boolean showProgressBar , boolean isCancelable) {
		this.context=context;
		this.processID=processID;
		this.callback =callback;
		this.url=url;
		this.showProgressBar=showProgressBar;
		this.isCancelable = isCancelable;
		
		if(Utils.isConnectionAvailable(context)){
			execute();
		}else{	
			/*
			if(progressDialog!=null && showProgressBar){
				progressDialog.dismiss();
				
			}
			*/
			
			if(Utils.isShowingLoadingDialog(context) && showProgressBar){
				Utils.hideLoadingDialog(context);
			}
			
			if(callback!=null && context!=null){				
				callback.jsonError(context.getString(R.string.error_message_no_internet), processID);
			}			
		}		
		
		
	}
	
	@Override
	protected void onPreExecute() 
	{
		if(showProgressBar){
			/*
			if(progressDialog==null){
				progressDialog = new DialogProgressBar(context);
				progressDialog.setCancelable(isCancelable);
			}
			
	 		progressDialog.show();
	 		*/
			
			if(!Utils.isShowingLoadingDialog(this.context)){
				Utils.showLoadingDialog(this.context, isCancelable);
			}
		}
	   
	}

	@Override
    protected void onPostExecute(String result) {
		
		//if(errorString.equals("")){
			callback.callbackJson(result, processID, 0);
		//}else{
		//	callback.jsonError(errorString, processID);
		//}
		/*
		if(progressDialog!=null && progressDialog.isShowing()){
			progressDialog.dismiss();
		}
		*/
		
		
		if(Utils.isShowingLoadingDialog(this.context)){
			Utils.hideLoadingDialog(this.context);
		}
		
      /*Toast.makeText(AsyncDemo.this, "Done!", Toast.LENGTH_SHORT).show();*/
    }
	 
    @Override
    protected String doInBackground(String... obj) {
    	HttpClient httpsClient;
		HttpGet httpget;
		
		HttpResponse response  = null;
		String ret = "";				
		byPassSSL();
    	httpsClient = sslClient(new DefaultHttpClient());
        
//		httpsClient = new DefaultHttpClient();
//		httpsClient = Utils.getHTTPSClient();

		String request = url;
		
		if(Constants.DEBUG_MODE){
			Log.d(Constants.TAG, Utils.class.getSimpleName() + "-request: " + request);
		}
		
		try {
			httpget = new HttpGet(request);
			String base64 = "";
	        
			String authorization = Constants.UN + ":" + Constants.PW;
	        base64 = Base64.encodeToString(authorization.getBytes(), Base64.NO_WRAP);
	        httpget.setHeader("Authorization", "Basic " + base64);
	        httpget.setHeader("Content-Type", Constants.contentType);
			
			
			response = httpsClient.execute(httpget);

			if (response != null) {
				ret = EntityUtils.toString(response.getEntity(), "UTF-8");

			}
		
		}catch(Exception e){
			//e.printStackTrace();
			if(Constants.DEBUG_MODE){
				Log.d(Constants.TAG, "Exception: " + e.toString());
			}
			
		}
		if(Constants.DEBUG_MODE){
			Log.d(Constants.TAG, Utils.class.getSimpleName() + "-ret: " + ret);
		}
		
		return ret;
    }
    
    
    private HttpClient sslClient(HttpClient client) {
        try {
            X509TrustManager tm = new X509TrustManager() { 
                public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };
            SSLContext ctx = SSLContext.getInstance("TLSv1");
            ctx.init(null, new TrustManager[]{tm}, null);
            SSLSocketFactory ssf = new MySSLSocketFactory(ctx);
            ssf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            ClientConnectionManager ccm = client.getConnectionManager();
            SchemeRegistry sr = ccm.getSchemeRegistry();
            sr.register(new Scheme("https", ssf, 443));
            return new DefaultHttpClient(ccm, client.getParams());
        } catch (Exception ex) {
            return null;
        }
    }
    public class MySSLSocketFactory extends SSLSocketFactory {
        SSLContext sslContext = SSLContext.getInstance("TLSv1");

        public MySSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
            super(truststore);

            TrustManager tm = new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };

            sslContext.init(null, new TrustManager[] { tm }, null);
        }

        public MySSLSocketFactory(SSLContext context) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException {
           super(null);
           sslContext = context;
        }

        public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
            return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
        }

        public Socket createSocket() throws IOException {
            return sslContext.getSocketFactory().createSocket();
        }
   }
    
    /*
    @SuppressWarnings("unchecked")
    @Override
    protected void onProgressUpdate(String... item) {
    	/
      	((ArrayAdapter<String>)getListAdapter()).add(item[0]);
    	 /
    }
    */
    
    public static void byPassSSL() {
		System.setProperty("jsse.enableSNIExtension", "false");
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			@Override
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			@Override
			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}

			@Override
			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}

		} 
		
		};

		SSLContext sc;
		try {
			sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
		}

		HostnameVerifier allHostsValid = new HostnameVerifier() {
			@Override
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};

		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

    }
	

   
}
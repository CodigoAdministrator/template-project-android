package sg.codigo.mytemplate;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sg.codigo.mytemplate.callbacks.JsonCallback;
import sg.codigo.mytemplate.callbacks.PopupCallback;
import sg.codigo.mytemplate.views.InteractiveScrollView;
import sg.codigo.mytemplate.dialogs.DialogOK;
import sg.codigo.mytemplate.dialogs.DialogTNC;
import sg.codigo.mytemplate.dialogs.DialogTNCWebView;
import sg.codigo.mytemplate.dialogs.DialogTwoButtons;
import sg.codigo.mytemplate.models.APIResult;
import sg.codigo.mytemplate.storages.AppPreferences;
import sg.codigo.mytemplate.utils.Constants;
import sg.codigo.mytemplate.utils.HTTPPostAsyncTask;
import sg.codigo.mytemplate.utils.TypefaceSpan;
import sg.codigo.mytemplate.utils.Utils;
import sg.codigo.mytemplate.views.ColorClickableSpan;
import sg.codigo.mytemplate.views.InteractiveScrollView.OnBottomReachedListener;
import sg.codigo.mytemplate.views.InteractiveScrollView.OnScrollDownListener;
import sg.codigo.mytemplate.views.InteractiveScrollView.OnScrollUpListener;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import android.net.Uri;
import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;

public class RegisterWithEmailActivity extends Activity implements OnClickListener, PopupCallback, JsonCallback{
	// data
	private AppPreferences appPrefs;
	private PopupCallback popupCallback;
	private int animationIndex = 1;
	private String email;
	private ArrayList<NameValuePair> nameValuePairs;
	private JsonCallback callback;
	private DialogOK dialogOK;
	private DialogOK dialogError;
	private DialogTwoButtons dialogSignIn;
	private DialogTwoButtons dialogAccountActivation;
	private boolean lockSlideUpBtnRegisterNow = false;
	private boolean lockSlideDownBtnRegisterNow = false;
	
	// layout
	//private InteractiveScrollView svLayout;
	private TextView tvIntroduction;
	private RelativeLayout layoutEmail;
	private ImageView icEmail;
	private EditText etEmail;
	private TextView tvLabelEmail;
	private RelativeLayout layoutName;
	private ImageView icName;
	private EditText etName;
	private TextView tvLabelName;
	private RelativeLayout layoutPassword;
	private ImageView icPassword;
	private EditText etPassword;
	private TextView tvLabelPassword;
	private RelativeLayout layoutRetypePassword;
	private ImageView icRetypePassword;
	private EditText etRetypePassword;
	private TextView tvLabelRetypePassword;
	private RelativeLayout layoutMobile;
	private ImageView icMobile;
	private EditText etMobile;
	private TextView tvLabelMobile;
	private RelativeLayout layoutCompanyName;
	private ImageView icCompanyName;
	private EditText etCompanyName;
	private TextView tvLabelCompanyName;
	private TextView tvNotice;
	private TextView tvRegistrationContent;
	private RelativeLayout layoutBtnRegisterNow;
	private Button btnRegisterNow;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Utils.updateActionBar(RegisterWithEmailActivity.this, getString(R.string.title_register_with_email), true, true, false, null);
        setContentView(R.layout.activity_register_with_email);
        
        
        
        email = getIntent().getStringExtra(Constants.BUNDLE_STRING_EMAIL);
        init();
    }
    
    @Override
    public void onStart() {
      super.onStart();
    }
    
    @Override
    protected void onResume(){
		super.onResume();
		Utils.updateGA(this, getString(R.string.ga_screen_register_with_email));
		if(appPrefs == null){
    		appPrefs = new AppPreferences(this);
    	}
		
		if(appPrefs.getEmergencyStatus()){
        	Intent intent = new Intent(this, EmergencyActivity.class);
    		startActivity(intent);
    		this.finish();
        }
    }
    
    @Override
    public void onPause() {
        super.onPause();
    }
    
    @Override
    protected void onDestroy(){
    	this.setResult(Constants.ACTION_FINISH_ACTIVITY);
    	super.onDestroy();
    	
    }
    
    @Override
	protected void onStop() {
    	super.onStop();
    	
	}
    
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        	finish();
        }else if (keyCode == KeyEvent.KEYCODE_MENU) {
        
        }else if (keyCode == KeyEvent.KEYCODE_HOME) {
        
        }

        return false;
        //return super.onKeyDown(keyCode, event);
    }
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
	     if(Constants.DEBUG_MODE){
	    	 Log.d(Constants.TAG, "Constants.SCREEN_REGISTER_WITH_EMAIL: " + Constants.SCREEN_REGISTER_WITH_EMAIL);
	    	 Log.d(Constants.TAG, "requestCode: " + requestCode);
	    	 Log.d(Constants.TAG, "Constants.ACTION_FINISH_ACTIVITY: " + Constants.ACTION_FINISH_ACTIVITY);
	    	 Log.d(Constants.TAG, "resultCode: " + resultCode);
	     }
	     if(requestCode == Constants.SCREEN_REGISTER_WITH_EMAIL && resultCode == Constants.ACTION_FINISH_ACTIVITY) 
	     {
	    	 this.setResult(Constants.ACTION_FINISH_ACTIVITY);
	    	 this.finish();
	     }else{
	    	 super.onActivityResult(requestCode, resultCode, data);
		     
	     }
	}
	
	
    @Override
	public void onLowMemory(){
    	System.gc();
	}
	
    @Override
	public void onSaveInstanceState (Bundle outState) {
    	super.onSaveInstanceState(outState);
    }
   
    @Override
    protected void onRestoreInstanceState (Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
        	this.finish();
        	
        }
        return true;
    }
    
    private void init(){
    	
    	//hide keybaord
    	getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    	
    	appPrefs = new AppPreferences(RegisterWithEmailActivity.this);
    	popupCallback = this;
    	callback = this;
        nameValuePairs = new ArrayList<NameValuePair>();
        
    	
        //svLayout = (InteractiveScrollView)findViewById(R.id.sv_layout);
        tvIntroduction = (TextView)findViewById(R.id.tv_introduction);
        tvIntroduction.setTypeface(Utils.getFontAvenirLight(RegisterWithEmailActivity.this));
    	layoutEmail = (RelativeLayout)findViewById(R.id.layout_email);
    	icEmail = (ImageView)findViewById(R.id.ic_email);
    	etEmail = (EditText)findViewById(R.id.et_email);
    	etEmail.setTypeface(Utils.getFontAvenirLight(RegisterWithEmailActivity.this));
    	tvLabelEmail = (TextView)findViewById(R.id.tv_label_email);
    	tvLabelEmail.setTypeface(Utils.getFontAvenirBlack(RegisterWithEmailActivity.this));
    	
    	layoutName = (RelativeLayout)findViewById(R.id.layout_name);
    	icName = (ImageView)findViewById(R.id.ic_name);
    	etName = (EditText)findViewById(R.id.et_name);
    	etName.setTypeface(Utils.getFontAvenirLight(RegisterWithEmailActivity.this));
    	tvLabelName = (TextView)findViewById(R.id.tv_label_name);
    	tvLabelName.setTypeface(Utils.getFontAvenirBlack(RegisterWithEmailActivity.this));
    	
    	layoutPassword = (RelativeLayout)findViewById(R.id.layout_password);
    	icPassword = (ImageView)findViewById(R.id.ic_password);
    	etPassword = (EditText)findViewById(R.id.et_password);
    	etPassword.setTypeface(Utils.getFontAvenirLight(RegisterWithEmailActivity.this));
    	tvLabelPassword = (TextView)findViewById(R.id.tv_label_password);
    	tvLabelPassword.setTypeface(Utils.getFontAvenirBlack(RegisterWithEmailActivity.this));
    	
    	layoutRetypePassword = (RelativeLayout)findViewById(R.id.layout_retype_password);
    	icRetypePassword = (ImageView)findViewById(R.id.ic_retype_password);
    	etRetypePassword = (EditText)findViewById(R.id.et_retype_password);
    	etRetypePassword.setTypeface(Utils.getFontAvenirLight(RegisterWithEmailActivity.this));
    	tvLabelRetypePassword = (TextView)findViewById(R.id.tv_label_retype_password);
    	tvLabelRetypePassword.setTypeface(Utils.getFontAvenirBlack(RegisterWithEmailActivity.this));
    	
    	layoutMobile = (RelativeLayout)findViewById(R.id.layout_mobile);
    	icMobile = (ImageView)findViewById(R.id.ic_mobile);
    	etMobile = (EditText)findViewById(R.id.et_mobile);
    	etMobile.setTypeface(Utils.getFontAvenirLight(RegisterWithEmailActivity.this));
    	tvLabelMobile = (TextView)findViewById(R.id.tv_label_mobile);
    	tvLabelMobile.setTypeface(Utils.getFontAvenirBlack(RegisterWithEmailActivity.this));
    	
    	layoutCompanyName = (RelativeLayout)findViewById(R.id.layout_company_name);
    	icCompanyName = (ImageView)findViewById(R.id.ic_company_name);
    	etCompanyName = (EditText)findViewById(R.id.et_company_name);
    	etCompanyName.setTypeface(Utils.getFontAvenirLight(RegisterWithEmailActivity.this));
    	tvLabelCompanyName = (TextView)findViewById(R.id.tv_label_company_name);
    	tvLabelCompanyName.setTypeface(Utils.getFontAvenirBlack(RegisterWithEmailActivity.this));
    	
    	tvNotice = (TextView)findViewById(R.id.tv_notice);
    	tvNotice.setTypeface(Utils.getFontAvenirLight(RegisterWithEmailActivity.this));
    	
    	tvRegistrationContent = (TextView)findViewById(R.id.tv_registration_content);
    	tvRegistrationContent.setTypeface(Utils.getFontAvenirLight(RegisterWithEmailActivity.this));
    	
    	layoutBtnRegisterNow = (RelativeLayout)findViewById(R.id.layout_btn_register_now);
    	btnRegisterNow = (Button)findViewById(R.id.btn_register_now);
    	btnRegisterNow.setTypeface(Utils.getFontAvenirBook(RegisterWithEmailActivity.this));
    	btnRegisterNow.setOnClickListener(this);
    	
    	final String keywordPhoneNumber = getString(R.string.contact_telephone);
    	SpannableString notice = new SpannableString(getString(R.string.text_contact_to_create_corporate_account));
    	notice.setSpan(new UnderlineSpan(), getString(R.string.text_contact_to_create_corporate_account).indexOf(keywordPhoneNumber), getString(R.string.text_contact_to_create_corporate_account).indexOf(keywordPhoneNumber) + keywordPhoneNumber.length(), 0);

    	tvNotice.setText(notice);
    	tvNotice.setMovementMethod(LinkMovementMethod.getInstance());
    	tvNotice.setOnClickListener(this);
    	
    	final String keywordTOS = "Terms of Use";
    	final String keywordPP = "Privacy Policy";
    	
    	ColorClickableSpan spanTOS = new ColorClickableSpan(getResources().getColor(R.color.turquoise_02), getResources().getColor(R.color.turquoise_02), getResources().getColor(R.color.transparent)) {
            @Override
            public void onClick(View textView) {
            	/*
                DialogTNC dialogTNC = new DialogTNC(RegisterWithEmailActivity.this, keywordTOS, getResources().getString(R.string.text_dummy));
                dialogTNC.show();
                */
            	DialogTNCWebView dialogTNC = new DialogTNCWebView(RegisterWithEmailActivity.this, Constants.getURLTNC());
                dialogTNC.show();
                
            }
        };
        

        ColorClickableSpan spanPP = new ColorClickableSpan(getResources().getColor(R.color.turquoise_02), getResources().getColor(R.color.turquoise_02), getResources().getColor(R.color.transparent)) {
            @Override
            public void onClick(View textView) {
            	/*
            	DialogTNC dialogTNC = new DialogTNC(RegisterWithEmailActivity.this, keywordPP, getResources().getString(R.string.text_dummy));
                dialogTNC.show();
                */
            	DialogTNCWebView dialogPP = new DialogTNCWebView(RegisterWithEmailActivity.this, Constants.getURLPP());
            	dialogPP.show();
            }
        };

    	SpannableString content = new SpannableString(getString(R.string.text_registration_content));
        content.setSpan(spanTOS, getString(R.string.text_registration_content).indexOf(keywordTOS), getString(R.string.text_registration_content).indexOf(keywordTOS) + keywordTOS.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        content.setSpan(spanPP, getString(R.string.text_registration_content).indexOf(keywordPP), getString(R.string.text_registration_content).indexOf(keywordPP) + keywordPP.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        
        tvRegistrationContent.setText(content);
        tvRegistrationContent.setMovementMethod(LinkMovementMethod.getInstance());
        
        /*
    	svLayout.setOnTopReachedListener(new OnTopReachedListener(){

			@Override
			public void onTopReached() {
				if(layoutArrangeProgressBar.getVisibility() == View.INVISIBLE && ivArrangeIndicator.getVisibility() == View.INVISIBLE && !lockSlideUpArrangeProgressBar && !lockSlideDownArrangeProgressBar){
					lockSlideDownArrangeProgressBar = true;
					layoutArrangeProgressBar.setVisibility(View.VISIBLE);
					ivArrangeIndicator.setVisibility(View.VISIBLE);
			    	YoYo.with(Techniques.SlideInDown)
			    	.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	lockSlideDownArrangeProgressBar = false;
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.duration(Constants.animationTime * animationIndex)
		    		.playOn(layoutArrangeProgressBar);
			    	
			    	YoYo.with(Techniques.SlideInDown)
			    	.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	lockSlideDownArrangeProgressBar = false;
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.duration(Constants.animationTime * animationIndex)
		    		.playOn(ivArrangeIndicator);
				}
			}
    		
    	});
    	*/
        /*
        svLayout.setOnBottomReachedListener(new OnBottomReachedListener(){

			@Override
			public void onBottomReached() {
				if(Constants.DEBUG_MODE){
					Log.d(Constants.TAG, "lockSlideDownBtnRegisterNow: " + lockSlideDownBtnRegisterNow);
					Log.d(Constants.TAG, "lockSlideUpBtnRegisterNow: " + lockSlideUpBtnRegisterNow);
					Log.d(Constants.TAG, "layoutBtnRegisterNow.getVisibility(): " + layoutBtnRegisterNow.getVisibility());
					Log.d(Constants.TAG, "View.INVISIBLE: " + View.INVISIBLE);
					Log.d(Constants.TAG, "View.VISIBLE: " + View.VISIBLE);
				}
				if(layoutBtnRegisterNow.getVisibility() == View.INVISIBLE && !lockSlideUpBtnRegisterNow && !lockSlideDownBtnRegisterNow){
					lockSlideUpBtnRegisterNow = true;
					layoutBtnRegisterNow.setVisibility(View.VISIBLE);
			    	YoYo.with(Techniques.SlideInUp)
			    	.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	lockSlideUpBtnRegisterNow = false;
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.duration(Constants.animationTime * animationIndex)
		    		.playOn(layoutBtnRegisterNow);
				}
				
			}
    		
    	});
    	*/
        /*
        svLayout.setOnScrollUpListener(new OnScrollUpListener(){

			@Override
			public void onScrollUp() {
				if(layoutBtnRegisterNow.getVisibility() == View.VISIBLE && !lockSlideDownBtnRegisterNow && !lockSlideUpBtnRegisterNow){
					lockSlideDownBtnRegisterNow = true;
					YoYo.with(Techniques.SlideOutDown)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	layoutBtnRegisterNow.setVisibility(View.INVISIBLE);
                        	lockSlideDownBtnRegisterNow = false;
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.duration(Constants.animationTime * animationIndex)
		    		.playOn(layoutBtnRegisterNow)
		    		;
				}
			}
    	});
    	
    	svLayout.setOnScrollDownListener(new OnScrollDownListener(){
			@Override
			public void onScrollDown() {
				
				if(layoutBtnRegisterNow.getVisibility() == View.INVISIBLE && !lockSlideUpBtnRegisterNow && !lockSlideDownBtnRegisterNow){
					lockSlideUpBtnRegisterNow = true;
					layoutBtnRegisterNow.setVisibility(View.VISIBLE);
			    	YoYo.with(Techniques.SlideInUp)
			    	.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	lockSlideUpBtnRegisterNow = false;
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.duration(Constants.animationTime * animationIndex)
		    		.playOn(layoutBtnRegisterNow);
				}
				
				
			}
    	});
    	*/
    	
    	TextWatcher emailTextWatcher = new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if(tvLabelEmail.getVisibility()==View.GONE && s.length() > 0){
					YoYo.with(Techniques.FadeInDown)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	tvLabelEmail.setVisibility(View.VISIBLE);
                        	icEmail.setBackgroundResource(R.drawable.ic_email);
                        	etEmail.setHint(getString(R.string.hint_email));
                			etEmail.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelEmail);
					
					
					
				}else if(tvLabelEmail.getVisibility()==View.VISIBLE && s.length() <= 0){
					YoYo.with(Techniques.FadeOutUp)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	icEmail.setBackgroundResource(R.drawable.ic_email);
                        	etEmail.setHint(getString(R.string.hint_email));
                			etEmail.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	tvLabelEmail.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelEmail);
					
				}
			}
    		
    	};
    	
    	etEmail.addTextChangedListener(emailTextWatcher);
    	
    	TextWatcher nameTextWatcher = new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if(tvLabelName.getVisibility()==View.GONE && s.length() > 0){
					YoYo.with(Techniques.FadeInDown)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	tvLabelName.setVisibility(View.VISIBLE);
                        	icName.setBackgroundResource(R.drawable.ic_name);
                        	etName.setHint(getString(R.string.hint_name));
                        	etName.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelName);
					
					
					
				}else if(tvLabelName.getVisibility()==View.VISIBLE && s.length() <= 0){
					YoYo.with(Techniques.FadeOutUp)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	icName.setBackgroundResource(R.drawable.ic_name);
                        	etName.setHint(getString(R.string.hint_name));
                        	etName.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	tvLabelName.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelName);
					
				}
			}
    		
    	};
    	
    	etName.addTextChangedListener(nameTextWatcher);
    	
    	TextWatcher passwordTextWatcher = new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if(tvLabelPassword.getVisibility()==View.GONE && s.length() > 0){
					YoYo.with(Techniques.FadeInDown)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	tvLabelPassword.setVisibility(View.VISIBLE);
                        	icPassword.setBackgroundResource(R.drawable.ic_password);
                        	etPassword.setHint(getString(R.string.hint_password));
                        	etPassword.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelPassword);
					
					
					
				}else if(tvLabelPassword.getVisibility()==View.VISIBLE && s.length() <= 0){
					YoYo.with(Techniques.FadeOutUp)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	icPassword.setBackgroundResource(R.drawable.ic_password);
                        	etPassword.setHint(getString(R.string.hint_password));
                        	etPassword.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	tvLabelPassword.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelPassword);
					
				}
			}
    		
    	};
    	
    	etPassword.addTextChangedListener(passwordTextWatcher);
    	
    	TextWatcher retypePasswordTextWatcher = new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if(tvLabelRetypePassword.getVisibility()==View.GONE && s.length() > 0){
					YoYo.with(Techniques.FadeInDown)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	tvLabelRetypePassword.setVisibility(View.VISIBLE);
                        	icRetypePassword.setBackgroundResource(R.drawable.ic_password);
                        	etRetypePassword.setHint(getString(R.string.hint_password));
                        	etRetypePassword.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelRetypePassword);
					
					
					
				}else if(tvLabelRetypePassword.getVisibility()==View.VISIBLE && s.length() <= 0){
					YoYo.with(Techniques.FadeOutUp)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	icRetypePassword.setBackgroundResource(R.drawable.ic_password);
                        	etRetypePassword.setHint(getString(R.string.hint_password));
                        	etRetypePassword.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	tvLabelRetypePassword.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelRetypePassword);
					
				}
			}
    		
    	};
    	
    	etRetypePassword.addTextChangedListener(retypePasswordTextWatcher);
    	
    	TextWatcher mobileTextWatcher = new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if(tvLabelMobile.getVisibility()==View.GONE && s.length() > 0){
					YoYo.with(Techniques.FadeInDown)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	tvLabelMobile.setVisibility(View.VISIBLE);
                        	icMobile.setBackgroundResource(R.drawable.ic_mobile);
                        	etMobile.setHint(getString(R.string.hint_mobile));
                        	etMobile.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelMobile);
					
					
					
				}else if(tvLabelMobile.getVisibility()==View.VISIBLE && s.length() <= 0){
					YoYo.with(Techniques.FadeOutUp)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	icMobile.setBackgroundResource(R.drawable.ic_mobile);
                        	etMobile.setHint(getString(R.string.hint_mobile));
                        	etMobile.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	tvLabelMobile.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelMobile);
					
				}
			}
    	};
    	
    	etMobile.addTextChangedListener(mobileTextWatcher);
    	
    	TextWatcher companyNameTextWatcher = new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if(tvLabelCompanyName.getVisibility()==View.GONE && s.length() > 0){
					YoYo.with(Techniques.FadeInDown)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	tvLabelCompanyName.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelCompanyName);
					
					
					
				}else if(tvLabelCompanyName.getVisibility()==View.VISIBLE && s.length() <= 0){
					YoYo.with(Techniques.FadeOutUp)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	tvLabelCompanyName.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelCompanyName);
				}
			}
    	};
    	
    	etCompanyName.addTextChangedListener(companyNameTextWatcher);
    	
    	/*
    	YoYo.with(Techniques.FadeIn)
			.duration(Constants.animationTime * animationIndex)
			.playOn(layoutEmail);

    	YoYo.with(Techniques.FadeIn)
			.duration(Constants.animationTime * animationIndex)
			.playOn(layoutName);

    	YoYo.with(Techniques.FadeIn)
			.duration(Constants.animationTime * animationIndex)
			.playOn(layoutPassword);
    	
    	YoYo.with(Techniques.FadeIn)
			.duration(Constants.animationTime * animationIndex)
			.playOn(layoutRetypePassword);

    	YoYo.with(Techniques.FadeIn)
			.duration(Constants.animationTime * animationIndex)
			.playOn(layoutMobile);

    	YoYo.with(Techniques.FadeIn)
			.duration(Constants.animationTime * animationIndex)
			.playOn(layoutCompanyName);

    	YoYo.with(Techniques.FadeIn)
			.duration(Constants.animationTime * animationIndex)
			.playOn(tvRegistrationContent);
		*/
    	
    	/*
    	YoYo.with(Techniques.FadeIn)
			.duration(Constants.animationTime * animationIndex)
			.playOn(svLayout);
		
    	YoYo.with(Techniques.FadeIn)
			.duration(Constants.animationTime * animationIndex)
			.playOn(layoutBtnRegisterNow);
		*/
    	
    	if(email!=null && !email.equalsIgnoreCase("")){
    		etEmail.setText(email);
    	}
    	
		/*
    	if(Constants.IS_GA_ON){
			
    		if(MainActivity.trackerCDG!=null){
				// Set screen name.
				MainActivity.trackerCDG.setScreenName(getString(R.string.ga_screen_terms));
	
		        // Send a screen view.
				MainActivity.trackerCDG.send(new HitBuilders.AppViewBuilder().build());
    		}
    		
    		if(MainActivity.trackerSP!=null){
				// Set screen name.
				MainActivity.trackerSP.setScreenName(getString(R.string.ga_screen_terms));
	
		        // Send a screen view.
				MainActivity.trackerSP.send(new HitBuilders.AppViewBuilder().build());
    		}
    	}
    	*/
    }
    
	
    public void onClick(View v) {
    	Utils.hideSoftKeyboard(RegisterWithEmailActivity.this);
    	if(v == btnRegisterNow){
    		boolean hasError = false;
    		View viewHighlighted = null;
    		
    		if(etEmail.getText().toString().trim().equalsIgnoreCase("")){
    			hasError = true;
    			etEmail.setText("");
    			etEmail.setHint(getString(R.string.error_message_email_01));
    			etEmail.setHintTextColor(getResources().getColor(R.color.error_red));
    			icEmail.setBackgroundResource(R.drawable.ic_email_error);
    			YoYo.with(Techniques.Shake)
    				.duration(Constants.animationTime)
    				.playOn(icEmail);
    			
    			if(viewHighlighted == null){
    				viewHighlighted = etEmail;
    			}
    			
    			//DialogOK dialogOK = new DialogOK(RegisterWithEmailActivity.this, getString(R.string.dialog_title_alert), getString(R.string.error_message_email_01), getString(R.string.btn_ok), Constants.POPUP_INVALID_EMAIL, popupCallback);
    			//dialogOK.show();
    		}
    		else if(!Utils.emailValidation(etEmail.getText().toString().trim())){
    			hasError = true;
    			etEmail.setText("");
    			etEmail.setHint(getString(R.string.error_message_email_02));
    			etEmail.setHintTextColor(getResources().getColor(R.color.error_red));
    			icEmail.setBackgroundResource(R.drawable.ic_email_error);
    			YoYo.with(Techniques.Shake)
    				.duration(Constants.animationTime)
    				.playOn(icEmail);
    			
    			if(viewHighlighted == null){
    				viewHighlighted = etEmail;
    			}
    			//DialogOK dialogOK = new DialogOK(RegisterWithEmailActivity.this, getString(R.string.dialog_title_alert), getString(R.string.error_message_email_02), getString(R.string.btn_ok), Constants.POPUP_INVALID_EMAIL, popupCallback);
    			//dialogOK.show();
    		}
    		
    		if(etName.getText().toString().trim().equalsIgnoreCase("")){
    			hasError = true;
    			etName.setText("");
    			etName.setHint(getString(R.string.error_message_name));
    			etName.setHintTextColor(getResources().getColor(R.color.error_red));
    			icName.setBackgroundResource(R.drawable.ic_name_error);
    			YoYo.with(Techniques.Shake)
    				.duration(Constants.animationTime)
    				.playOn(icName);
    			
    			if(viewHighlighted == null){
    				viewHighlighted = etName;
    			}
    			//DialogOK dialogOK = new DialogOK(RegisterWithEmailActivity.this, getString(R.string.dialog_title_alert), getString(R.string.error_message_name), getString(R.string.btn_ok), Constants.POPUP_INVALID_NAME, popupCallback);
    			//dialogOK.show();
    		}
    		
    		if(etPassword.getText().toString().trim().equalsIgnoreCase("")){
    			hasError = true;
    			etPassword.setText("");
    			etPassword.setHint(getString(R.string.error_message_password_01));
    			etPassword.setHintTextColor(getResources().getColor(R.color.error_red));
    			icPassword.setBackgroundResource(R.drawable.ic_password_error);
    			YoYo.with(Techniques.Shake)
    				.duration(Constants.animationTime)
    				.playOn(icPassword);
    			
    			if(viewHighlighted == null){
    				viewHighlighted = etPassword;
    			}
    			//DialogOK dialogOK = new DialogOK(RegisterWithEmailActivity.this, getString(R.string.dialog_title_alert), getString(R.string.error_message_password_01), getString(R.string.btn_ok), Constants.POPUP_INVALID_PASSWORD, popupCallback);
    			//dialogOK.show();
    		}
    		else if(etPassword.getText().toString().trim().length() < 6){
    			hasError = true;
    			etPassword.setText("");
    			etPassword.setHint(getString(R.string.error_message_password_03));
    			etPassword.setHintTextColor(getResources().getColor(R.color.error_red));
    			icPassword.setBackgroundResource(R.drawable.ic_password_error);
    			YoYo.with(Techniques.Shake)
    				.duration(Constants.animationTime)
    				.playOn(icPassword);
    			if(viewHighlighted == null){
    				viewHighlighted = etPassword;
    			}
    			//DialogOK dialogOK = new DialogOK(RegisterWithEmailActivity.this, getString(R.string.dialog_title_alert), getString(R.string.error_message_password_03), getString(R.string.btn_ok), Constants.POPUP_INVALID_PASSWORD, popupCallback);
    			//dialogOK.show();
    		}
    		
    		if(etRetypePassword.getText().toString().trim().equalsIgnoreCase("")){
				hasError = true;
    			etRetypePassword.setText("");
    			etRetypePassword.setHint(getString(R.string.error_message_password_05));
    			etRetypePassword.setHintTextColor(getResources().getColor(R.color.error_red));
    			icRetypePassword.setBackgroundResource(R.drawable.ic_password_error);
    			YoYo.with(Techniques.Shake)
    				.duration(Constants.animationTime)
    				.playOn(icRetypePassword);
    			
    			if(viewHighlighted == null){
    				viewHighlighted = etRetypePassword;
    			}
    			//DialogOK dialogOK = new DialogOK(RegisterWithEmailActivity.this, getString(R.string.dialog_title_alert), getString(R.string.error_message_password_01), getString(R.string.btn_ok), Constants.POPUP_INVALID_PASSWORD, popupCallback);
    			//dialogOK.show();
			}
    		else if(!etRetypePassword.getText().toString().trim().equalsIgnoreCase(etPassword.getText().toString().trim())){
    			
    			hasError = true;
    			etRetypePassword.setText("");
    			etRetypePassword.setHint(getString(R.string.error_message_password_04));
    			etRetypePassword.setHintTextColor(getResources().getColor(R.color.error_red));
    			icRetypePassword.setBackgroundResource(R.drawable.ic_password_error);
    			YoYo.with(Techniques.Shake)
    				.duration(Constants.animationTime)
    				.playOn(icRetypePassword);
    			
    			if(viewHighlighted == null){
    				viewHighlighted = etRetypePassword;
    			}
    			//DialogOK dialogOK = new DialogOK(RegisterWithEmailActivity.this, getString(R.string.dialog_title_alert), getString(R.string.error_message_password_04), getString(R.string.btn_ok), Constants.POPUP_INVALID_RETYPE_PASSWORD, popupCallback);
    			//dialogOK.show();
    			
    		}
    		
    		if(etMobile.getText().toString().trim().equalsIgnoreCase("")){
    			hasError = true;
    			etMobile.setText("");
    			etMobile.setHint(getString(R.string.error_message_mobile_01));
    			etMobile.setHintTextColor(getResources().getColor(R.color.error_red));
    			icMobile.setBackgroundResource(R.drawable.ic_mobile_error);
    			YoYo.with(Techniques.Shake)
    				.duration(Constants.animationTime)
    				.playOn(icMobile);
    			
    			if(viewHighlighted == null){
    				viewHighlighted = etMobile;
    			}
    			//DialogOK dialogOK = new DialogOK(RegisterWithEmailActivity.this, getString(R.string.dialog_title_alert), getString(R.string.error_message_mobile_01), getString(R.string.btn_ok), Constants.POPUP_INVALID_MOBILE, popupCallback);
    			//dialogOK.show();
    		}
    		else if(etMobile.getText().toString().trim().length() < 8){
    			hasError = true;
    			etMobile.setText("");
    			etMobile.setHint(getString(R.string.error_message_mobile_02));
    			etMobile.setHintTextColor(getResources().getColor(R.color.error_red));
    			icMobile.setBackgroundResource(R.drawable.ic_mobile_error);
    			YoYo.with(Techniques.Shake)
    				.duration(Constants.animationTime)
    				.playOn(icMobile);
    			
    			if(viewHighlighted == null){
    				viewHighlighted = etMobile;
    			}
    			//DialogOK dialogOK = new DialogOK(RegisterWithEmailActivity.this, getString(R.string.dialog_title_alert), getString(R.string.error_message_mobile_02), getString(R.string.btn_ok), Constants.POPUP_INVALID_MOBILE, popupCallback);
    			//dialogOK.show();
    		}
    		
    		if(!hasError){
				/*
    			nameValuePairs.clear();
    			
    			nameValuePairs.add(new BasicNameValuePair("name", etName.getText().toString().trim()));
    	    	nameValuePairs.add(new BasicNameValuePair("email", etEmail.getText().toString().trim()));
    	    	nameValuePairs.add(new BasicNameValuePair("password", etPassword.getText().toString().trim()));
    	    	nameValuePairs.add(new BasicNameValuePair("moblie", etMobile.getText().toString().trim()));
    	    	nameValuePairs.add(new BasicNameValuePair("company_name", etCompanyName.getText().toString().trim()));
    	    	nameValuePairs.add(new BasicNameValuePair("device_id", appPrefs.getGCMRegistrantId()));
    	    	nameValuePairs.add(new BasicNameValuePair("device_type", Constants.DEVICE_TYPE));
    		    
    			new HTTPPostAsyncTask(RegisterWithEmailActivity.this, nameValuePairs, Constants.API_REGISTER_CUSTOMER_ACCOUNT_BY_EMAIL, callback, Constants.ID_API_REGISTER_CUSTOMER_ACCOUNT_BY_EMAIL, true, false);
    	    	*/
    		}else{
    			if(viewHighlighted != null){
    				viewHighlighted.requestFocus();
    			}
    		}
    	}else if(v == tvNotice){
    		Intent intent = new Intent(Intent.ACTION_DIAL);
    		intent.setData(Uri.parse("tel:" + getString(R.string.contact_telephone)));
    		startActivity(intent); 
    	}
    }

	@Override
	public void callBackPopup(Object data, int processID, int index) {
		/*
		if(processID == Constants.POPUP_API_CALL_INVALID_TOKEN){
			this.finish();
		}
		else if(processID == Constants.POPUP_API_CALL_ACCOUNT_BLOCKED){
			this.finish();
		}
		else
		*/
		if(processID == Constants.POPUP_API_CALL_SUCCEED){
			/*
			appPrefs.setLastLoggedInEmail(appPrefs.getCustomerInfo().getEmail());
			
			Intent intent = new Intent(this, AccountActivationActivity.class);
			intent.putExtra(Constants.BUNDLE_STRING_EMAIL, etEmail.getText().toString().trim());
			intent.putExtra(Constants.BUNDLE_STRING_PASSWORD, etPassword.getText().toString().trim());
			startActivityForResult(intent, Constants.SCREEN_REGISTER_WITH_EMAIL);
			this.setResult(Constants.ACTION_FINISH_ACTIVITY);
	    	this.finish();
	    	*/
		}
		else if(processID == Constants.POPUP_API_CALL_FAILED){
			
		}
		/*
		else if(processID == Constants.POPUP_API_CALL_FAILED_ACCOUNT_EXISTS_SIGN_IN){
			if(index == Constants.DIALOG_BUTTON_RIGHT){
				
				Customer customer = new Customer();
	    		customer.setEmail(etEmail.getText().toString().trim());
				
				appPrefs.setCustomerInfo(customer);
				
				Intent intent = new Intent(this, SignInActivity.class);
				startActivityForResult(intent, Constants.SCREEN_REGISTER_WITH_EMAIL);
				this.setResult(Constants.ACTION_FINISH_ACTIVITY);
		    	this.finish();
			}
		}
		else if(processID == Constants.POPUP_API_CALL_FAILED_ACCOUNT_EXISTS_ACTIVATE_ACCOUNT){
			if(index == Constants.DIALOG_BUTTON_RIGHT){
				
				Customer customer = new Customer();
	    		customer.setEmail(etEmail.getText().toString().trim());
				
				appPrefs.setCustomerInfo(customer);
				
				Intent intent = new Intent(this, AccountActivationActivity.class);
				startActivityForResult(intent, Constants.SCREEN_REGISTER_WITH_EMAIL);
				this.setResult(Constants.ACTION_FINISH_ACTIVITY);
		    	this.finish();
			}
		}
		*/

		/*
		if(processID == Constants.POPUP_INVALID_EMAIL){
			etEmail.requestFocus();
		}
		else if(processID == Constants.POPUP_INVALID_NAME){
			etName.requestFocus();
		}
		else if(processID == Constants.POPUP_INVALID_PASSWORD){
			etPassword.requestFocus();
		}
		else if(processID == Constants.POPUP_INVALID_RETYPE_PASSWORD){
			etRetypePassword.requestFocus();
		}
		else if(processID == Constants.POPUP_INVALID_MOBILE){
			etMobile.requestFocus();
		}
		*/
	}
	
	@Override
	public void jsonError(String msg, int processID) {
		//if(processID == Constants.ID_API_REGISTER_CUSTOMER_ACCOUNT_BY_EMAIL){
			if(Constants.DEBUG_MODE){
				Log.i(Constants.TAG, "jsonError: " + msg);
			}
			if(dialogError==null || !dialogError.isShowing()){
				dialogError = new DialogOK(RegisterWithEmailActivity.this, getString(R.string.dialog_title_error), msg, getString(R.string.btn_ok), Constants.POPUP_API_CALL_FAILED, popupCallback);
				dialogError.show();
			}
		//}
	}

	@Override
	public void callbackJson(String result, int processID, int index) {
		/*
		if(processID == Constants.ID_API_REGISTER_CUSTOMER_ACCOUNT_BY_EMAIL){
			
			if(result!=null && !result.equalsIgnoreCase("")){
				try{
					JSONObject jsonObj = new JSONObject(result);
					
					int errorCode = jsonObj.getInt("errorCode");
					String message = jsonObj.getString("message");
					
					if(errorCode == Constants.ERROR_CODE_SUCCESS){
						if(jsonObj.has("customer_info") && jsonObj.getString("customer_info").startsWith("[")){
							JSONArray customerArray = jsonObj.getJSONArray("customer_info");
							for(int i = 0; i < customerArray.length(); i++){
								JSONObject customerObj = customerArray.getJSONObject(i);
								Customer customer = new Customer();
								customer.setToken((customerObj.has("customer_token") && customerObj.getString("customer_token")!=null)?customerObj.getString("customer_token"):"");
								customer.setName((customerObj.has("customer_name") && customerObj.getString("customer_name")!=null)?customerObj.getString("customer_name"):"");
								customer.setCode((customerObj.has("customer_code") && customerObj.getString("customer_code")!=null)?customerObj.getString("customer_code"):"");
								customer.setMobile((customerObj.has("customer_mobile") && customerObj.getString("customer_mobile")!=null)?customerObj.getString("customer_mobile"):"");
								customer.setEmail((customerObj.has("customer_email") && customerObj.getString("customer_email")!=null)?customerObj.getString("customer_email"):"");
								customer.setCompanyName((customerObj.has("company_name") && customerObj.getString("company_name")!=null)?customerObj.getString("company_name"):"");
								customer.setRegDate((customerObj.has("reg_date") && customerObj.getString("reg_date")!=null)?customerObj.getString("reg_date"):"");
								customer.setPhotoUrl((customerObj.has("customer_photo") && customerObj.getString("customer_photo")!=null)?customerObj.getString("customer_photo"):"");
								customer.setMemberType((customerObj.has("member_type") && customerObj.getString("member_type")!=null)?customerObj.getString("member_type"):"");
								customer.setPaymentType((customerObj.has("payment_type") && customerObj.getString("payment_type")!=null)?customerObj.getString("payment_type"):"");
								
								appPrefs.setCustomerInfo(customer);
								
							}
							if(dialogOK==null || !dialogOK.isShowing()){
								dialogOK = new DialogOK(RegisterWithEmailActivity.this, getString(R.string.dialog_title_activate_your_account), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_SUCCEED, popupCallback);
								dialogOK.show();
							}
						}else if(jsonObj.has("customer_info") && jsonObj.getString("customer_info").startsWith("{")){
							JSONObject customerObj = jsonObj.getJSONObject("customer_info");
							Customer customer = new Customer();
							customer.setToken((customerObj.has("customer_token") && customerObj.getString("customer_token")!=null)?customerObj.getString("customer_token"):"");
							customer.setName((customerObj.has("customer_name") && customerObj.getString("customer_name")!=null)?customerObj.getString("customer_name"):"");
							customer.setCode((customerObj.has("customer_code") && customerObj.getString("customer_code")!=null)?customerObj.getString("customer_code"):"");
							customer.setMobile((customerObj.has("customer_mobile") && customerObj.getString("customer_mobile")!=null)?customerObj.getString("customer_mobile"):"");
							customer.setEmail((customerObj.has("customer_email") && customerObj.getString("customer_email")!=null)?customerObj.getString("customer_email"):"");
							customer.setCompanyName((customerObj.has("company_name") && customerObj.getString("company_name")!=null)?customerObj.getString("company_name"):"");
							customer.setRegDate((customerObj.has("reg_date") && customerObj.getString("reg_date")!=null)?customerObj.getString("reg_date"):"");
							customer.setPhotoUrl((customerObj.has("customer_photo") && customerObj.getString("customer_photo")!=null)?customerObj.getString("customer_photo"):"");
							customer.setMemberType((customerObj.has("member_type") && customerObj.getString("member_type")!=null)?customerObj.getString("member_type"):"");
							customer.setPaymentType((customerObj.has("payment_type") && customerObj.getString("payment_type")!=null)?customerObj.getString("payment_type"):"");
							
							appPrefs.setCustomerInfo(customer);
							if(dialogOK==null || !dialogOK.isShowing()){
								dialogOK = new DialogOK(RegisterWithEmailActivity.this, getString(R.string.dialog_title_activate_your_account), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_SUCCEED, popupCallback);
								dialogOK.show();
							}
						}else{
							if(dialogError==null || !dialogError.isShowing()){
								dialogError = new DialogOK(RegisterWithEmailActivity.this, getString(R.string.dialog_title_error), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_FAILED, popupCallback);
								dialogError.show();
							}
						}
						
					}else if(errorCode == Constants.ERROR_CODE_ACCOUNT_ACTIVATED){
						if(dialogSignIn==null || !dialogSignIn.isShowing()){
							dialogSignIn = new DialogTwoButtons(RegisterWithEmailActivity.this, getString(R.string.dialog_title_error),  message + " " + getString(R.string.alert_message_account_exists_please_sign_in), getString(R.string.btn_cancel), getString(R.string.btn_sign_in), Constants.POPUP_API_CALL_FAILED_ACCOUNT_EXISTS_SIGN_IN, popupCallback);
							dialogSignIn.show();
						}
					}else if(errorCode == Constants.ERROR_CODE_ACCOUNT_NOT_ACTIVATE){
						if(dialogAccountActivation==null || !dialogAccountActivation.isShowing()){
							dialogAccountActivation = new DialogTwoButtons(RegisterWithEmailActivity.this, getString(R.string.dialog_title_error),  message + " " + getString(R.string.alert_message_account_exists_please_activate), getString(R.string.btn_cancel), getString(R.string.btn_activate_now), Constants.POPUP_API_CALL_FAILED_ACCOUNT_EXISTS_ACTIVATE_ACCOUNT, popupCallback);
							dialogAccountActivation.show();
						}
					}else if(errorCode == Constants.ERROR_CODE_INVALID_TOKEN){
						finish();
						/
						if(dialogOK==null || !dialogOK.isShowing()){
							dialogOK = new DialogOK(RegisterWithEmailActivity.this, getString(R.string.dialog_title_error), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_INVALID_TOKEN, popupCallback);
							dialogOK.show();
						}
						/
					}else if(errorCode == Constants.ERROR_CODE_ACCOUNT_BLOCKED){
						if(dialogOK==null || !dialogOK.isShowing()){
							dialogOK = new DialogOK(RegisterWithEmailActivity.this, getString(R.string.dialog_title_error), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_ACCOUNT_BLOCKED, popupCallback);
							dialogOK.show();
						}
					}else{
						if(dialogError==null || !dialogError.isShowing()){
							dialogError = new DialogOK(RegisterWithEmailActivity.this, getString(R.string.dialog_title_error), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_FAILED, popupCallback);
							dialogError.show();
						}
					}
				}catch(JSONException e){
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "JSONException: " + e.toString());
					}
				}catch(Exception e){
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "Exception: " + e.toString());
					}
				}
			}
		}
		*/
	}
}

package sg.codigo.mytemplate.maps;

import java.util.Timer;
import java.util.TimerTask;

import sg.codigo.mytemplate.callbacks.PopupCallback;
import sg.codigo.mytemplate.R;
import sg.codigo.mytemplate.dialogs.DialogOK;
import sg.codigo.mytemplate.dialogs.DialogTwoButtons;
import sg.codigo.mytemplate.utils.Constants;
import sg.codigo.mytemplate.utils.Utils;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;

public class MyLocation implements PopupCallback{
    Timer timer1;
    LocationManager lm;
    LocationResult locationResult;
    boolean gps_enabled=false;
    boolean network_enabled=false;
    PopupCallback popupCallback;
    Context context;
    
    public boolean getLocation(Context context, LocationResult result)
    {
        //I use LocationResult callback class to pass location value from MyLocation to user code.
        locationResult=result;
        if(lm==null)
            lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        //exceptions will be thrown if provider is not permitted.
        try{
        	gps_enabled=lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        	network_enabled=lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }catch(Exception e){
        	if(e!=null){
        		if(Constants.DEBUG_MODE){
        			Log.e(Constants.TAG, MyLocation.class + " Exception: " + e.toString());
        		}
        	}
        }
        
        //don't start listeners if no provider is enabled
        if(!gps_enabled && !network_enabled){
        	if(Constants.DEBUG_MODE){
        		Log.d(Constants.TAG, "no location provider");
        	}
        	this.context = context;
        	this.popupCallback = this;
        	
        	DialogTwoButtons dialogGPS = new DialogTwoButtons(context, context.getString(R.string.dialog_title_alert), context.getString(R.string.alert_message_no_gps_detected), context.getString(R.string.btn_cancel), context.getString(R.string.btn_ok), Constants.POPUP_SWITCH_ON_GPS, this.popupCallback);
        	dialogGPS.show();
        	
            return false;
        }
        if(gps_enabled){
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListenerGps);
        }
        if(network_enabled){
            lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListenerNetwork);
        }
        timer1=new Timer();
        timer1.schedule(new GetLastLocation(), Constants.TIMEOUT_IN_MILISECONDS);
        return true;
    }

    public void stopLocate(Context context)
    {
    	timer1.cancel();
    	lm.removeUpdates(locationListenerGps);
        lm.removeUpdates(locationListenerNetwork);
    }
    
    LocationListener locationListenerGps = new LocationListener() {
        public void onLocationChanged(Location location) {
            timer1.cancel();
            locationResult.gotLocation(location);
            lm.removeUpdates(this);
            lm.removeUpdates(locationListenerNetwork);
        }
        public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

    LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            timer1.cancel();
            locationResult.gotLocation(location);
            lm.removeUpdates(this);
            lm.removeUpdates(locationListenerGps);
        }
        public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

    class GetLastLocation extends TimerTask {
        @Override
        public void run() {
        	 Looper.prepare();
             lm.removeUpdates(locationListenerGps);
             lm.removeUpdates(locationListenerNetwork);

             Location net_loc=null, gps_loc=null;
             if(gps_enabled)
                 gps_loc=lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
             if(network_enabled)
                 net_loc=lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

             //if there are both values use the latest one
             if(gps_loc!=null && net_loc!=null){
                 if(gps_loc.getTime()>net_loc.getTime())
                     locationResult.gotLocation(gps_loc);
                 else
                     locationResult.gotLocation(net_loc);
                 return;
             }

             if(gps_loc!=null){
                 locationResult.gotLocation(gps_loc);
                 return;
             }
             if(net_loc!=null){
                 locationResult.gotLocation(net_loc);
                 return;
             }
             locationResult.gotLocation(null);
        }
    }

    public static abstract class LocationResult{
        public abstract void gotLocation(Location location);
    }

	@Override
	public void callBackPopup(Object data, int processID, int index) {
		if(processID == Constants.POPUP_SWITCH_ON_GPS){
			
			if(index == Constants.DIALOG_BUTTON_RIGHT){
				//Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);  
	        	//context.startActivity(gpsOptionsIntent);
				
	        	context.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
	        	
			}else if(index == Constants.DIALOG_BUTTON_LEFT){
				if(Utils.isShowingLoadingDialog(context)){
					Utils.hideLoadingDialog(context);
				}
			}
			
		}
	}
}
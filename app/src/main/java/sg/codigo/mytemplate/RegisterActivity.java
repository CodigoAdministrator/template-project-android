package sg.codigo.mytemplate;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sg.codigo.mytemplate.callbacks.JsonCallback;
import sg.codigo.mytemplate.callbacks.PopupCallback;
import sg.codigo.mytemplate.dialogs.DialogOK;
import sg.codigo.mytemplate.models.FacebookUserInfo;
import sg.codigo.mytemplate.storages.AppPreferences;
import sg.codigo.mytemplate.utils.Constants;
import sg.codigo.mytemplate.utils.HTTPPostAsyncTask;
import sg.codigo.mytemplate.utils.TypefaceSpan;
import sg.codigo.mytemplate.utils.Utils;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.facebook.AppEventsLogger;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionDefaultAudience;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.internal.SessionTracker;
import com.facebook.model.GraphUser;
import com.nineoldandroids.animation.Animator;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;

public class RegisterActivity extends Activity implements OnClickListener, PopupCallback, JsonCallback{
	// data
	public static Activity activity;
	private AppPreferences appPrefs;
	private PopupCallback popupCallback;
	private int animationIndex = 1;
	private ArrayList<NameValuePair> nameValuePairs;
	private JsonCallback jsonCallback;
	private DialogOK dialogOK;
	private DialogOK dialogError;
	private Boolean isFBAuthorized = false;
	
	// facebook
	private SessionTracker mSessionTracker;
	private Session activeSession;
	private FacebookUserInfo fbUserInfo;
	private UiLifecycleHelper uiHelper;
	
	// layout
	private RelativeLayout layoutBtnFB;
	private Button btnFb;
	private ImageView ivDividerOR;
	private RelativeLayout layoutEmail;
	private ImageView icEmail;
	private EditText etEmail;
	private TextView tvLabelEmail;
	//private TextView tvEmailError;
	private RelativeLayout layoutBtnRegister;
	private Button btnRegister;
	
	// facebook
    private Session.StatusCallback fbCallback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
        	if(Constants.DEBUG_MODE){
 				Log.d(Constants.TAG, "signupViaFacebook 04");
 			}
            onSessionStateChange(session, state, exception);
        }
    };
    
    // facebook
    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
 	   	if(Constants.DEBUG_MODE){
 			Log.d(Constants.TAG, "signupViaFacebook 05");
 	   	}
 	   	if(state.isOpened() && isFBAuthorized) {
 			
 		   	fetchFacebookUserData(session);
 		   	isFBAuthorized = false;
 			if(Constants.DEBUG_MODE){
  				Log.d(Constants.TAG, "signupViaFacebook 06");
  			}
 		}else{
  		
  			if(Constants.DEBUG_MODE){
  				Log.d(Constants.TAG, "signupViaFacebook 07");
  			}
  			
  		}
    }
    
    public void signupViaFacebook() {
    	mSessionTracker = new SessionTracker(RegisterActivity.this, fbCallback);
    	isFBAuthorized = true;
        String applicationId = "";
        //if(Constants.IS_PRODUCTION){
        	applicationId = getString(R.string.fb_app_id); //_prod);//Utility.getMetadataApplicationId(context);
        //}else{
        //	applicationId = getString(R.string.fb_app_id_stag);
        //}
        
        activeSession = mSessionTracker.getSession();

    	
    	if (activeSession == null || activeSession.isClosed()) {
 			//Session.openActiveSession(MainActivity.this, true, callback);
    		mSessionTracker.setSession(null);
            Session session = new Session.Builder(RegisterActivity.this).setApplicationId(applicationId).build();
            
            Session.setActiveSession(session);
            activeSession = session;
    	}else{
    		
    	}

    	if (!activeSession.isOpened()) {
	        Session.OpenRequest openRequest = null;
	        openRequest = new Session.OpenRequest((Activity) RegisterActivity.this);
	
	        if (openRequest != null) {
	            openRequest.setDefaultAudience(SessionDefaultAudience.FRIENDS);
	            openRequest.setPermissions(Arrays.asList(Constants.PERMISSIONS_READ));
	            openRequest.setLoginBehavior(SessionLoginBehavior.SSO_WITH_FALLBACK);
	
	            activeSession.openForRead(openRequest);
	        	
	            //fetchFacebookUserData();
	        }
 		} else {
 			//publishFeedDialog();
 			if(isFBAuthorized){
 				fetchFacebookUserData(activeSession);
 				isFBAuthorized = false;
 			}
 		}
 	}
    
    private void fetchFacebookUserData(final Session session){ //, final int actionType){
    	
    	// make request to the /me API
        Request.newMeRequest(session, new Request.GraphUserCallback() {

        	// callback after Graph API response with user object
        	@Override
        	public void onCompleted(GraphUser user, Response response) {
        		if (user != null) {
        			
        			if(session.getAccessToken()!=null){
        				if(Constants.DEBUG_MODE){
        					Log.d(Constants.TAG, "session.getAccessToken(): " + session.getAccessToken());
        				}
        				appPrefs.setFBToken(session.getAccessToken());
        			}
        			
        			if(Constants.DEBUG_MODE){
        				Log.d(Constants.TAG, "fb id: " + user.getId());
						Log.d(Constants.TAG, "fb name: " + user.getName());
						//Log.d(Constants.TAG, "fb gender: " + user.getProperty("gender").toString());
						//Log.d(Constants.TAG, "fb bday: " + user.getBirthday());
						Log.d(Constants.TAG, "fb email: " + user.getProperty("email"));
						
					}
        			if(user.getId()!=null){	
						fbUserInfo.setId(user.getId());
					}
        			
					if(user.getName()!=null){	
						fbUserInfo.setName(user.getName());
					}
					/*
					if(user.getBirthday()!=null){	
						String bday = user.getBirthday();
						fbUserInfo.setDOB(bday);
					}
					if(user.getProperty("gender")!=null){		
						fbUserInfo.setGender(user.getProperty("gender").toString());
					}
					*/
					if(user.getProperty("email")!=null){					
						fbUserInfo.setEmail(user.getProperty("email").toString());
					}
					
					appPrefs.setFBUserInfo(fbUserInfo);
					appPrefs.setFBLoginStatus(true);

					/*
					nameValuePairs.clear();
					nameValuePairs.add(new BasicNameValuePair("name", appPrefs.getFBUserInfo().getName()));
	    	    	nameValuePairs.add(new BasicNameValuePair("email", appPrefs.getFBUserInfo().getEmail()));
	    	    	nameValuePairs.add(new BasicNameValuePair("facebook_id", appPrefs.getFBUserInfo().getId()));
	    	    	nameValuePairs.add(new BasicNameValuePair("device_id", appPrefs.getGCMRegistrantId()));
	    	    	nameValuePairs.add(new BasicNameValuePair("device_type", Constants.DEVICE_TYPE));
	    		    
	    			new HTTPPostAsyncTask(RegisterActivity.this, nameValuePairs, Constants.API_REGISTER_CUSTOMER_ACCOUNT_BY_FB, jsonCallback, Constants.ID_API_REGISTER_CUSTOMER_ACCOUNT_BY_FB, true, false);
					*/
					
        		}
        		else{
        			appPrefs.setFBLoginStatus(false);
        			appPrefs.clearFBUserInfo();
        		}
        	}

        }).executeAsync();
        
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        
        Utils.updateActionBar(RegisterActivity.this, getString(R.string.title_register), true, true, false, null);
    	setContentView(R.layout.activity_register);
    	
    	if(appPrefs == null){
    		appPrefs = new AppPreferences(this);
    	}
    	appPrefs.setIsFirstLaunch(false);
    	
    	// facebook
    	uiHelper = new UiLifecycleHelper(RegisterActivity.this, fbCallback);
        uiHelper.onCreate(savedInstanceState);
        
    	init();
    }
    
    @Override
    public void onStart() {
    	super.onStart();
    }
    
    @Override
    protected void onResume(){
		super.onResume();
		// facebook
    	uiHelper.onResume();
		// Logs 'install' and 'app activate' App Events.
		AppEventsLogger.activateApp(this);
		
		Utils.updateGA(this, getString(R.string.ga_screen_register));
    	
		if(appPrefs == null){
    		appPrefs = new AppPreferences(this);
    	}
		
		if(appPrefs.getEmergencyStatus()){
        	Intent intent = new Intent(this, EmergencyActivity.class);
    		startActivity(intent);
    		this.finish();
        }
    }
    
    @Override
    public void onPause() {
        super.onPause();
        // facebook
        uiHelper.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }
    
    @Override
    protected void onDestroy(){
    	super.onDestroy();
    	this.setResult(Constants.ACTION_FINISH_ACTIVITY);
    	// facebook
    	uiHelper.onDestroy();
    }
    
    @Override
	protected void onStop() {
    	super.onStop();
    	
	}
    
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        	finish();
        }else if (keyCode == KeyEvent.KEYCODE_MENU) {
        
        }else if (keyCode == KeyEvent.KEYCODE_HOME) {
        
        }

        return false;
        //return super.onKeyDown(keyCode, event);
    }
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
	     if(requestCode == Constants.SCREEN_REGISTER && resultCode == Constants.ACTION_FINISH_ACTIVITY) 
	     {
	    	 this.setResult(Constants.ACTION_FINISH_ACTIVITY);
	    	 this.finish();
	     }else{
	    	 // facebook
			 uiHelper.onActivityResult(requestCode, resultCode, data);
			
			 if(Session.getActiveSession()!=null){
				 Session.getActiveSession().onActivityResult(RegisterActivity.this, requestCode, resultCode, data);
			 }
	    	 
	    	 super.onActivityResult(requestCode, resultCode, data);
	     }
	}
	
	
    @Override
	public void onLowMemory(){
    	System.gc();
	}
	
    @Override
	public void onSaveInstanceState (Bundle outState) {
    	super.onSaveInstanceState(outState);
    	
    	// facebook
	    uiHelper.onSaveInstanceState(outState);
    }
   
    @Override
    protected void onRestoreInstanceState (Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
        	this.finish();
        	
        }
        return true;
    }
    
    private void init(){
    	
    	//hide keybaord
    	getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    	
    	appPrefs = new AppPreferences(RegisterActivity.this);
    	popupCallback = this;
    	jsonCallback = this;
        nameValuePairs = new ArrayList<NameValuePair>();
        
        // facebook
    	fbUserInfo = new FacebookUserInfo();
    	
    	layoutBtnFB = (RelativeLayout)findViewById(R.id.layout_btn_fb);
    	btnFb = (Button)findViewById(R.id.btn_fb);
    	btnFb.setTypeface(Utils.getFontAvenirBook(RegisterActivity.this));
    	btnFb.setOnClickListener(this);
    	ivDividerOR = (ImageView)findViewById(R.id.iv_divider_or);
    	layoutEmail = (RelativeLayout)findViewById(R.id.layout_email);
    	icEmail = (ImageView)findViewById(R.id.ic_email);
    	etEmail = (EditText)findViewById(R.id.et_email);
    	etEmail.setTypeface(Utils.getFontAvenirLight(RegisterActivity.this));
    	tvLabelEmail = (TextView)findViewById(R.id.tv_label_email);
    	tvLabelEmail.setTypeface(Utils.getFontAvenirBlack(RegisterActivity.this));
    	//tvEmailError = (TextView)findViewById(R.id.tv_email_error);
    	//tvEmailError.setTypeface(Utils.getFontAvenirLight(RegisterActivity.this));
    	layoutBtnRegister = (RelativeLayout)findViewById(R.id.layout_btn_register);
    	btnRegister = (Button)findViewById(R.id.btn_register);
    	btnRegister.setTypeface(Utils.getFontAvenirBook(RegisterActivity.this));
    	btnRegister.setOnClickListener(this);
    	
    	TextWatcher emailTextWatcher = new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if(tvLabelEmail.getVisibility()==View.GONE && s.length() > 0){
					YoYo.with(Techniques.FadeInDown)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	tvLabelEmail.setVisibility(View.VISIBLE);
                        	icEmail.setBackgroundResource(R.drawable.ic_email);
                        	etEmail.setHint(getString(R.string.hint_email));
                			etEmail.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelEmail);
					
					
					
				}else if(tvLabelEmail.getVisibility()==View.VISIBLE && s.length() <= 0){
					YoYo.with(Techniques.FadeOutUp)
		    		.duration(Constants.animationTime)
		    		.withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        	icEmail.setBackgroundResource(R.drawable.ic_email);
                        	etEmail.setHint(getString(R.string.hint_email));
                			etEmail.setHintTextColor(getResources().getColor(R.color.hint_light_grey_01));
                			
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        	tvLabelEmail.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
		    		.playOn(tvLabelEmail);
					
					
		    		
				}
			}
    		
    	};
    	
    	etEmail.addTextChangedListener(emailTextWatcher);
    	
    	/*
    	YoYo.with(Techniques.FadeIn)
    		.duration(Constants.animationTime * animationIndex++)
    		.playOn(layoutBtnFB);
    	
    	YoYo.with(Techniques.FadeIn)
    		.duration(Constants.animationTime * animationIndex++)
    		.playOn(ivDividerOR);
    	
    	YoYo.with(Techniques.FadeIn)
			.duration(Constants.animationTime * animationIndex++)
			.playOn(layoutEmail);

    	YoYo.with(Techniques.FadeIn)
			.duration(Constants.animationTime * animationIndex++)
			.playOn(layoutBtnRegister);
		*/

		/*
    	if(Constants.IS_GA_ON){
			
    		if(MainActivity.trackerCDG!=null){
				// Set screen name.
				MainActivity.trackerCDG.setScreenName(getString(R.string.ga_screen_terms));
	
		        // Send a screen view.
				MainActivity.trackerCDG.send(new HitBuilders.AppViewBuilder().build());
    		}
    		
    		if(MainActivity.trackerSP!=null){
				// Set screen name.
				MainActivity.trackerSP.setScreenName(getString(R.string.ga_screen_terms));
	
		        // Send a screen view.
				MainActivity.trackerSP.send(new HitBuilders.AppViewBuilder().build());
    		}
    	}
    	*/
    }
    
	
    public void onClick(View v) {
    	Utils.hideSoftKeyboard(RegisterActivity.this);
    	if(v == btnFb){
    		etEmail.setText("");
    		
    		signupViaFacebook();
    		
    	}else if(v == btnRegister){
    		if(etEmail.getText().toString().trim().equalsIgnoreCase("")){
    			etEmail.setText("");
    			etEmail.setHint(getString(R.string.error_message_email_01));
    			etEmail.setHintTextColor(getResources().getColor(R.color.error_red));
    			icEmail.setBackgroundResource(R.drawable.ic_email_error);
    			YoYo.with(Techniques.Shake)
    				.duration(Constants.animationTime)
    				.playOn(icEmail);
    			//DialogOK dialogOK = new DialogOK(RegisterActivity.this, getString(R.string.dialog_title_alert), getString(R.string.error_message_email_01), getString(R.string.btn_ok), Constants.POPUP_INVALID_EMAIL, popupCallback);
    			//dialogOK.show();
    		}
    		else if(!Utils.emailValidation(etEmail.getText().toString().trim())){
    			etEmail.setText("");
    			etEmail.setHint(getString(R.string.error_message_email_02));
    			etEmail.setHintTextColor(getResources().getColor(R.color.error_red));
    			icEmail.setBackgroundResource(R.drawable.ic_email_error);
    			YoYo.with(Techniques.Shake)
    				.duration(Constants.animationTime)
    				.playOn(icEmail);
    			//DialogOK dialogOK = new DialogOK(RegisterActivity.this, getString(R.string.dialog_title_alert), getString(R.string.error_message_email_02), getString(R.string.btn_ok), Constants.POPUP_INVALID_EMAIL, popupCallback);
    			//dialogOK.show();
    			
    		}else{
    			Intent intent = new Intent(this, RegisterWithEmailActivity.class);
    			intent.putExtra(Constants.BUNDLE_STRING_EMAIL, etEmail.getText().toString());
    			startActivityForResult(intent, Constants.SCREEN_REGISTER);
    		}
    	}
    }

	@Override
	public void callBackPopup(Object data, int processID, int index) {
		/*
		if(processID == Constants.POPUP_API_CALL_INVALID_TOKEN){
			finish();
		}
		else if(processID == Constants.POPUP_API_CALL_ACCOUNT_BLOCKED){
			finish();
		}
		else
		*/
		if(processID == Constants.POPUP_API_CALL_SUCCEED){
			appPrefs.setIsLoginThruFB(true);
			Intent intent = new Intent(this, MainActivity.class);
    		startActivity(intent);
    		this.setResult(Constants.ACTION_FINISH_ACTIVITY);
    		this.finish();
		}
		
		else if(processID == Constants.POPUP_API_CALL_FAILED){
			
		}
		
		/*
		if(processID == Constants.POPUP_INVALID_EMAIL){
			etEmail.requestFocus();
			
		}
		*/
	}

	@Override
	public void callbackJson(String result, int processID, int index) {
		/*
		if(processID == Constants.ID_API_REGISTER_CUSTOMER_ACCOUNT_BY_FB){
			if(result!=null && !result.equalsIgnoreCase("")){
				try{
					JSONObject jsonObj = new JSONObject(result);
					
					int errorCode = jsonObj.getInt("errorCode");
					String message = jsonObj.getString("message");
					
					if(errorCode == Constants.ERROR_CODE_SUCCESS){
						if(jsonObj.has("customer_info") && jsonObj.getString("customer_info").startsWith("[")){
					
							JSONArray customerArray = jsonObj.getJSONArray("customer_info");
							for(int i = 0; i < customerArray.length(); i++){
								JSONObject customerObj = customerArray.getJSONObject(i);
								Customer customer = new Customer();
								customer.setToken((customerObj.has("customer_token") && customerObj.getString("customer_token")!=null)?customerObj.getString("customer_token"):"");
								customer.setName((customerObj.has("customer_name") && customerObj.getString("customer_name")!=null)?customerObj.getString("customer_name"):"");
								customer.setCode((customerObj.has("customer_code") && customerObj.getString("customer_code")!=null)?customerObj.getString("customer_code"):"");
								customer.setMobile((customerObj.has("customer_mobile") && customerObj.getString("customer_mobile")!=null)?customerObj.getString("customer_mobile"):"");
								customer.setEmail((customerObj.has("customer_email") && customerObj.getString("customer_email")!=null)?customerObj.getString("customer_email"):"");
								customer.setCompanyName((customerObj.has("company_name") && customerObj.getString("company_name")!=null)?customerObj.getString("company_name"):"");
								customer.setRegDate((customerObj.has("reg_date") && customerObj.getString("reg_date")!=null)?customerObj.getString("reg_date"):"");
								customer.setPhotoUrl((customerObj.has("customer_photo") && customerObj.getString("customer_photo")!=null)?customerObj.getString("customer_photo"):"");
								customer.setMemberType((customerObj.has("member_type") && customerObj.getString("member_type")!=null)?customerObj.getString("member_type"):"");
								customer.setPaymentType((customerObj.has("payment_type") && customerObj.getString("payment_type")!=null)?customerObj.getString("payment_type"):"");
								
								appPrefs.setCustomerInfo(customer);
								appPrefs.setIsLoggedIn(true);
								
							}
							
							/
							if(dialogOK==null || !dialogOK.isShowing()){
								dialogOK = new DialogOK(RegisterActivity.this, getString(R.string.dialog_title_success), getString(R.string.success_message_fb_registration_success), getString(R.string.btn_ok), Constants.POPUP_API_CALL_SUCCEED, popupCallback);
								dialogOK.show();
							}
							/
							
							goToMainActivity();
				    		
							
						}else if(jsonObj.has("customer_info") && jsonObj.getString("customer_info").startsWith("{")){
					
							JSONObject customerObj = jsonObj.getJSONObject("customer_info");
							Customer customer = new Customer();
							customer.setToken((customerObj.has("customer_token") && customerObj.getString("customer_token")!=null)?customerObj.getString("customer_token"):"");
							customer.setName((customerObj.has("customer_name") && customerObj.getString("customer_name")!=null)?customerObj.getString("customer_name"):"");
							customer.setCode((customerObj.has("customer_code") && customerObj.getString("customer_code")!=null)?customerObj.getString("customer_code"):"");
							customer.setMobile((customerObj.has("customer_mobile") && customerObj.getString("customer_mobile")!=null)?customerObj.getString("customer_mobile"):"");
							customer.setEmail((customerObj.has("customer_email") && customerObj.getString("customer_email")!=null)?customerObj.getString("customer_email"):"");
							customer.setCompanyName((customerObj.has("company_name") && customerObj.getString("company_name")!=null)?customerObj.getString("company_name"):"");
							customer.setRegDate((customerObj.has("reg_date") && customerObj.getString("reg_date")!=null)?customerObj.getString("reg_date"):"");
							customer.setPhotoUrl((customerObj.has("customer_photo") && customerObj.getString("customer_photo")!=null)?customerObj.getString("customer_photo"):"");
							customer.setMemberType((customerObj.has("member_type") && customerObj.getString("member_type")!=null)?customerObj.getString("member_type"):"");
							customer.setPaymentType((customerObj.has("payment_type") && customerObj.getString("payment_type")!=null)?customerObj.getString("payment_type"):"");
							
							appPrefs.setCustomerInfo(customer);
							
							appPrefs.setIsLoggedIn(true);
							
							/
							if(dialogOK==null || !dialogOK.isShowing()){
								dialogOK = new DialogOK(RegisterActivity.this, getString(R.string.dialog_title_success), getString(R.string.success_message_fb_registration_success), getString(R.string.btn_ok), Constants.POPUP_API_CALL_SUCCEED, popupCallback);
								dialogOK.show();
							}
							/
							
							goToMainActivity();
							
							
						}else{
							if(dialogError==null || !dialogError.isShowing()){
								dialogError = new DialogOK(RegisterActivity.this, getString(R.string.dialog_title_error), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_FAILED, popupCallback);
								dialogError.show();
							}
						}
						
						
					}else if(errorCode == Constants.ERROR_CODE_INVALID_TOKEN){
						finish();
						/
						if(dialogOK==null || !dialogOK.isShowing()){
							dialogOK = new DialogOK(RegisterActivity.this, getString(R.string.dialog_title_error), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_INVALID_TOKEN, popupCallback);
							dialogOK.show();
						}
						/
					}else if(errorCode == Constants.ERROR_CODE_ACCOUNT_BLOCKED){
						if(dialogOK==null || !dialogOK.isShowing()){
							dialogOK = new DialogOK(RegisterActivity.this, getString(R.string.dialog_title_error), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_ACCOUNT_BLOCKED, popupCallback);
							dialogOK.show();
						}
					}else{
						if(dialogError==null || !dialogError.isShowing()){
							dialogError = new DialogOK(RegisterActivity.this, getString(R.string.dialog_title_error), message, getString(R.string.btn_ok), Constants.POPUP_API_CALL_FAILED, popupCallback);
							dialogError.show();
						}
					}
				}catch(JSONException e){
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "JSONException: " + e.toString());
					}
					
				}catch(Exception e){
					if(Constants.DEBUG_MODE){
						Log.e(Constants.TAG, "Exception: " + e.toString());
					}
				}
			}
		}
		*/
	}

	@Override
	public void jsonError(String msg, int processID) {
		//if(processID == Constants.ID_API_REGISTER_CUSTOMER_ACCOUNT_BY_FB){
			if(Constants.DEBUG_MODE){
				Log.i(Constants.TAG, "jsonError: " + msg);
			}
			if(dialogError==null || !dialogError.isShowing()){
				dialogError = new DialogOK(RegisterActivity.this, getString(R.string.dialog_title_error), msg, getString(R.string.btn_ok), Constants.POPUP_API_CALL_FAILED, popupCallback);
				dialogError.show();
			}
		//}
	}
	
	private void goToMainActivity(){
		appPrefs.setIsLoginThruFB(true);
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
		this.setResult(Constants.ACTION_FINISH_ACTIVITY);
		this.finish();
	}
}

package sg.codigo.mytemplate;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import sg.codigo.mytemplate.callbacks.JsonCallback;
import sg.codigo.mytemplate.storages.AppPreferences;
import sg.codigo.mytemplate.utils.Constants;
import sg.codigo.mytemplate.utils.HTTPPostAsyncTask;
import sg.codigo.mytemplate.utils.TypefaceSpan;
import sg.codigo.mytemplate.utils.Utils;
import sg.codigo.mytemplate.views.InteractiveScrollView;
import sg.codigo.mytemplate.views.InteractiveScrollView.OnBottomReachedListener;
import sg.codigo.mytemplate.views.InteractiveScrollView.OnScrollDownListener;
import sg.codigo.mytemplate.views.InteractiveScrollView.OnScrollUpListener;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.nineoldandroids.animation.Animator;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;

public class EmergencyActivity extends Activity{
	// data
	private AppPreferences appPrefs;
	private int animationIndex = 1;
	
	
	// layout
	private LinearLayout layoutEmergency;
	
	private ImageView ivEmergency;
	private TextView tvEmergencyTitle;
	private TextView tvEmergencyContent;
	
	private WebView wvEmergency;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        Utils.updateActionBar(EmergencyActivity.this, "", false, false, false, null);
    	
        setContentView(R.layout.activity_emergency);
        
        init();
        
    }
    
    @Override
    public void onStart() {
      super.onStart();
       
    }
    
    @Override
    protected void onResume(){
		super.onResume();
		Utils.updateGA(this, getString(R.string.ga_screen_emergency));
    	
    }
    
    @Override
    public void onPause() {
        super.onPause();
    }
    
    @Override
    protected void onDestroy(){
    	super.onDestroy();
    }
    
    @Override
	protected void onStop() {
    	super.onStop();
    	
	}
    
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        	finish();
        	
        	return true;
        }else if (keyCode == KeyEvent.KEYCODE_MENU) {
        
        }else if (keyCode == KeyEvent.KEYCODE_HOME) {
        
        }

        return false;
        //return super.onKeyDown(keyCode, event);
    }
	
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
		
    }
	
	
    @Override
	public void onLowMemory(){
    	System.gc();
	}
	
    @Override
	public void onSaveInstanceState (Bundle outState) {
    	super.onSaveInstanceState(outState);
    }
   
    @Override
    protected void onRestoreInstanceState (Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
    
    
    private void init(){
    	
    	//hide keybaord
    	getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    	
    	appPrefs = new AppPreferences(EmergencyActivity.this);
        
    	layoutEmergency = (LinearLayout)findViewById(R.id.layout_emergency);
    	ivEmergency = (ImageView)findViewById(R.id.iv_emergency);
    	tvEmergencyTitle = (TextView)findViewById(R.id.tv_emergency_title);
    	tvEmergencyTitle.setTypeface(Utils.getFontAvenirHeavy(EmergencyActivity.this));
    	tvEmergencyContent = (TextView)findViewById(R.id.tv_emergency_content);
    	tvEmergencyContent.setTypeface(Utils.getFontAvenirLight(EmergencyActivity.this));
    	
    	
    	wvEmergency = (WebView)findViewById(R.id.wv_emergency);
    	if(//appPrefs.getEmergencyMessage().equalsIgnoreCase("") && 
    			!appPrefs.getEmergencyRedirectURL().equalsIgnoreCase("") && 
    			appPrefs.getEmergencyRedirectURL().contains("http")){
    		wvEmergency.setVisibility(View.VISIBLE);
    		layoutEmergency.setVisibility(View.GONE);
    		wvEmergency.loadUrl(appPrefs.getEmergencyRedirectURL());
    	}else{
    		wvEmergency.setVisibility(View.GONE);
    		layoutEmergency.setVisibility(View.VISIBLE);
    		tvEmergencyContent.setText(appPrefs.getEmergencyMessage());
        	
    	}
    	
    	YoYo.with(Techniques.FadeIn)
    		.duration(Constants.animationTime * animationIndex)
    		.playOn(ivEmergency);
	
	
    	YoYo.with(Techniques.FadeIn)
        	.duration(Constants.animationTime * animationIndex)
        	.playOn(tvEmergencyTitle);
    	
    	YoYo.with(Techniques.FadeIn)
    		.duration(Constants.animationTime * animationIndex)
    		.playOn(tvEmergencyContent);
	
	
    	
    	
		/*
    	if(Constants.IS_GA_ON){
			
    		if(MainActivity.trackerCDG!=null){
				// Set screen name.
				MainActivity.trackerCDG.setScreenName(getString(R.string.ga_screen_terms));
	
		        // Send a screen view.
				MainActivity.trackerCDG.send(new HitBuilders.AppViewBuilder().build());
    		}
    		
    		if(MainActivity.trackerSP!=null){
				// Set screen name.
				MainActivity.trackerSP.setScreenName(getString(R.string.ga_screen_terms));
	
		        // Send a screen view.
				MainActivity.trackerSP.send(new HitBuilders.AppViewBuilder().build());
    		}
    	}
    	*/
    }
    
	
}
